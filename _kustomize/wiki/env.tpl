WIKI_POSTGRES_HOST={{ .postgres.Host }}
WIKI_POSTGRES_USER={{ .postgres.User }}
WIKI_POSTGRES_PASSWORD={{ .postgres.Password }}
WIKI_POSTGRES_DATABASE={{ .postgres.Database }}
WIKI_POSTGRES_INSECURE={{ .postgres.Insecure }}
WIKI_REDIS_SECRET_STORE_HOST={{ .redisSecretStore.Host }}
