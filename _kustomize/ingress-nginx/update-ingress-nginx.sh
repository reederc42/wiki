#!/bin/bash

set -e

VERSION=controller-v1.8.1
http_port="${NODE_HTTP_PORT:-30080}"
https_port="${NODE_HTTPS_PORT:-30443}"

# get nginx manifests
wget -qO kustomization.yaml "https://raw.githubusercontent.com/kubernetes/ingress-nginx/$VERSION/deploy/static/provider/baremetal/kustomization.yaml"
wget -qO deploy.yaml "https://raw.githubusercontent.com/kubernetes/ingress-nginx/$VERSION/deploy/static/provider/baremetal/deploy.yaml"

# set nodeport
sed -e "s/\${http_port}/${http_port}/g" -e "s/\${https_port}/${https_port}/g" nodeport.yaml.tpl > nodeport.yaml

# add nodeport
kustomize edit add patch --path nodeport.yaml
