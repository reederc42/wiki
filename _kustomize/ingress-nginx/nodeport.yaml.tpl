apiVersion: v1
kind: Service
metadata:
  name: ingress-nginx-controller
  namespace: ingress-nginx
spec:
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: http
      nodePort: ${http_port}
    - name: https
      port: 443
      protocol: TCP
      targetPort: https
      nodePort: ${https_port}
