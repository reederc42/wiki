const path = require('path');

module.exports = {
    lintOnSave: false,
    chainWebpack: (config) => {
        resolveAPIClient(config);
        resolveAuthClient(config);
    },
    transpileDependencies: ['vuetify'],
    css: {
        loaderOptions: {
            scss: {
                additionalData: `$VUE_APP_SERVE_FONTS: ${
                    process.env.SERVE_FONTS || 'false'
                };`,
            },
        },
    },
    outputDir: resolveOutputDir(),
};

function resolveAPIClient(config) {
    config.resolve.alias.set(
        'api',
        path.resolve(__dirname, `src/api/${process.env.API_CLIENT || 'mock'}`)
    );
}

function resolveAuthClient(config) {
    config.resolve.alias.set(
        'auth',
        path.resolve(__dirname, `src/auth/${process.env.AUTH_CLIENT || 'mock'}`)
    );
}

function resolveOutputDir() {
    var distPath = 'dist';
    if (process.env.DIST_SUFFIX) {
        distPath += '-' + process.env.DIST_SUFFIX;
    }
    return path.resolve(__dirname, distPath);
}
