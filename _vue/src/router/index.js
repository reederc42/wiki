import Vue from 'vue';
import VueRouter from 'vue-router';
import WikiSubject from '../views/WikiSubject';
import WikiHome from '../views/WikiHome';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: WikiHome,
    },
    {
        path: '/wiki/:subject?',
        component: WikiSubject,
        props: true,
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
