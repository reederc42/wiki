import axios from 'axios';
import auth from 'auth';

const apiHost = process.env.VUE_APP_API_CLIENT_HOST;
const apiPath = process.env.VUE_APP_API_CLIENT_PATH;

export default {
    subject: {
        get: (name) => {
            return axios
                .get(subjectURL(name))
                .then((response) => response.data)
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
        list: () => {
            return axios
                .get(subjectURL())
                .then((response) => {
                    return response.data
                        .map((s) => {
                            return {
                                subject: s,
                                title: s.replace(/_/g, ' '),
                            };
                        })
                        .sort((a, b) => {
                            var titleA = a.title.toUpperCase();
                            var titleB = b.title.toUpperCase();
                            if (titleA < titleB) {
                                return -1;
                            }
                            if (titleA > titleB) {
                                return 1;
                            }
                            return 0;
                        });
                })
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
        edit: (name, content) => {
            return auth
                .put(apiHost, subjectPath(name), content)
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
        create: (name, content) => {
            return auth
                .post(apiHost, subjectPath(name), content)
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
        listEdits: (name) => {
            return axios
                .get(subjectURL() + 'edits/' + name)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
        featured: () => {
            return axios
                .get(subjectURL('__featured'))
                .then((response) => {
                    return {
                        subject: response.data,
                        title: response.data.replace(/_/g, ' '),
                    };
                })
                .catch((error) => {
                    throw getErrorResponse(error);
                });
        },
    },
};

function subjectPath(subject) {
    var u = apiPath + 'subject/';
    if (subject) {
        u += subject;
    }
    return u;
}

function subjectURL(subject) {
    return apiHost + subjectPath(subject);
}

function getErrorResponse(error) {
    if (error) {
        if (error.response) {
            if (error.response.data) {
                return error.response.data;
            }
            return error.response;
        }
    }
    return error;
}
