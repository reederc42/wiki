import subjects from './subjects.json';

const timeout = 40;

export default {
    subject: {
        get: (name) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    var s = subjects.filter((s) => s.subject == name);
                    if (s.length > 0) {
                        resolve(s[0].content);
                    } else {
                        reject('not found: ' + name);
                    }
                }, timeout);
            });
        },
        list: () => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve(
                        subjects
                            .map((s) => {
                                return {
                                    subject: s.subject,
                                    title: s.subject.replace(/_/g, ' '),
                                };
                            })
                            .sort((a, b) => {
                                var titleA = a.title.toUpperCase();
                                var titleB = b.title.toUpperCase();
                                if (titleA < titleB) {
                                    return -1;
                                }
                                if (titleA > titleB) {
                                    return 1;
                                }
                                return 0;
                            })
                    );
                }, timeout);
            });
        },
        edit: (name, content) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    var s = subjects.filter((s) => s.subject == name);
                    if (s.length > 0) {
                        s[0].content = content;
                        resolve();
                    } else {
                        reject('not found: ' + name);
                    }
                }, timeout);
            });
        },
        create: (name, content) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    var s = subjects.filter((s) => s.subject == name);
                    if (s.length > 0) {
                        reject('conflict: ' + name + ' already exists');
                    } else {
                        subjects.push({ subject: name, content: content });
                        resolve();
                    }
                }, timeout);
            });
        },
        listEdits: (name) => {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    var s = subjects.filter((s) => s.subject == name);
                    if (s.length > 0) {
                        resolve(s[0].edits);
                    } else {
                        reject('not found: ' + name);
                    }
                }, timeout);
            });
        },
        featured: () => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    var i = Math.floor(Math.random() * subjects.length);
                    resolve({
                        subject: subjects[i].subject,
                        title: subjects[i].subject.replace(/_/g, ' '),
                    });
                }, timeout);
            });
        },
    },
};
