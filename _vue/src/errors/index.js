import store from '../store';

export default {
    show: (error) => {
        store.commit('errors/push', error);
    },
    cancel: () => {
        store.commit('errors/shift');
    },
    count: () => {
        return store.getters['errors/count'];
    },
    message: () => {
        return store.getters['errors/message'];
    },
};
