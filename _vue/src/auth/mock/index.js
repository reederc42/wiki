import users from './users.json';
import axios from 'axios';

export default {
    signup: (name, password) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                var u = users.find((u) => u.name === name);
                if (u) {
                    signout();
                    reject('unauthorized');
                } else {
                    users.push({ name: name, password: password });
                    localStorage.setItem('name', name);
                    localStorage.setItem('token', 'bad token');
                    resolve();
                }
            }, 40);
        });
    },
    signin: (name, password) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                var u = users.find(
                    (u) => u.name === name && u.password === password
                );
                if (u) {
                    localStorage.setItem('name', name);
                    localStorage.setItem('token', 'bad token');
                    resolve();
                } else {
                    signout();
                    reject('unauthorized');
                }
            }, 40);
        });
    },
    signout: () => {
        delete localStorage.name;
        delete localStorage.token;
    },
    refresh: () => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                var u = users.find((u) => u.name === localStorage.name);
                if (u) {
                    localStorage.setItem('token', 'bad token');
                    resolve();
                } else {
                    signout();
                    reject('unauthorized');
                }
            }, 40);
        });
    },
    signedIn: () => {
        return !!(localStorage.name && localStorage.token);
    },
    name: () => {
        return localStorage.name;
    },
    axios: () => {
        return axios.create();
    },
};

function signout() {
    delete localStorage.name;
    delete localStorage.token;
}
