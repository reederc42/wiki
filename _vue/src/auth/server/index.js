import axios from 'axios';

const authURL =
    process.env.VUE_APP_AUTH_CLIENT_HOST + process.env.VUE_APP_AUTH_CLIENT_PATH;

export default {
    signup: (name, password) => {
        return axios
            .post(authURL + 'signup', null, {
                headers: {
                    Authorization: 'Basic ' + btoa(name + ':' + password),
                },
            })
            .then((response) => {
                localStorage.setItem('token', response.data);
                localStorage.setItem('name', name);
            })
            .catch((error) => {
                signout();
                throw getErrorResponse(error);
            });
    },
    signin: (name, password) => {
        return axios
            .get(authURL + 'signin', {
                headers: {
                    Authorization: 'Basic ' + btoa(name + ':' + password),
                },
            })
            .then((response) => {
                localStorage.setItem('token', response.data);
                localStorage.setItem('name', name);
            })
            .catch((error) => {
                signout();
                throw getErrorResponse(error);
            });
    },
    signout: () => {
        signout();
    },
    refresh: () => {
        return axios
            .get(authURL + 'signin', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.token,
                },
            })
            .then((response) => {
                localStorage.setItem('token', response.data);
            })
            .catch((error) => {
                signout();
                throw getErrorResponse(error);
            });
    },
    signedIn: () => {
        return !!(localStorage.name && localStorage.token);
    },
    name: () => {
        return localStorage.name;
    },
    post: (host, path, content) => {
        return authorize('POST', path).then((token) => {
            axios.post(host + path, content, {
                headers: {
                    Authorization: 'Bearer ' + token.data,
                },
            });
        });
    },
    put: (host, path, content) => {
        return authorize('PUT', path).then((token) => {
            axios.put(host + path, content, {
                headers: {
                    Authorization: 'Bearer ' + token.data,
                },
            });
        });
    },
};

function authorize(method, path) {
    return axios
        .get(authURL + method + path, {
            headers: {
                Authorization: 'Bearer ' + localStorage.token,
            },
        })
        .catch((error) => {
            throw getErrorResponse(error);
        });
}

function signout() {
    delete localStorage.name;
    delete localStorage.token;
}

function getErrorResponse(error) {
    if (error) {
        if (error.response) {
            if (error.response.data) {
                return error.response.data;
            }
            return error.response;
        }
    }
    return error;
}
