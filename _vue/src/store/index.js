import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            name: '',
            signedIn: false,
        },
        subjects: {
            dirty: true,
            count: 0,
        },
        newSubjects: new Map(),
    },
    mutations: {
        setUser(state, { name, signedIn }) {
            state.user.name = name;
            state.user.signedIn = signedIn;
        },
        setSubjects(state, { dirty, count }) {
            state.subjects.dirty = dirty;
            state.subjects.count = count;
        },
        addNewSubject(state, subject) {
            if (!state.newSubjects.has(subject)) {
                state.newSubjects.set(subject, true);
            }
        },
        removeNewSubject(state, subject) {
            state.newSubjects.delete(subject);
        },
    },
    getters: {
        isNewSubject: (state) => (subject) => {
            return state.newSubjects.has(subject);
        },
    },
    modules: {
        errors: {
            namespaced: true,
            state: {
                errors: [],
            },
            mutations: {
                push(state, error) {
                    state.errors.push(error);
                },
                shift(state) {
                    return state.errors.shift();
                },
            },
            getters: {
                count(state) {
                    return state.errors.length;
                },
                message(state) {
                    return state.errors[0];
                },
            },
        },
    },
});
