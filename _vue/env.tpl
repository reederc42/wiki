NODE_ENV={{ .vue.NodeEnv }}

# suffix of dist directory for build output
DIST_SUFFIX={{ .vue.Dist }}

# api client ("mock", "server")
API_CLIENT={{ .vue.APIClient }}

# api client server, ignored if api client is mock
VUE_APP_API_CLIENT_HOST={{ .vue.APIClientHost }}
VUE_APP_API_CLIENT_PATH={{ .vue.APIClientPath }}

# subject list refresh period in milliseconds
VUE_APP_API_LIST_REFRESH_MS={{ .vue.APIListRefresh.Milliseconds }}

# auth client ("mock", "server")
AUTH_CLIENT={{ .vue.AuthClient }}

# auth client server, ignored if auth client is mock
VUE_APP_AUTH_CLIENT_HOST={{ .vue.AuthClientHost }}
VUE_APP_AUTH_CLIENT_PATH={{ .vue.AuthClientPath }}

# auth token lifespan in milliseconds
VUE_APP_AUTH_LIFESPAN_MS={{ durationSub .auth.Authorization.TokenLifespan .auth.Wiggle | toMilliseconds }}

# if true, fonts are bundled into app
SERVE_FONTS={{ .vue.ServeFonts }}
