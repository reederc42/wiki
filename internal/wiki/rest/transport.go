package rest

import (
	"context"
	"io"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/gorilla/mux"

	"gitlab.com/reederc42/wiki/internal/wiki"
)

func decodeGetSubjectRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	return mux.Vars(r)["name"], nil
}

func makeGetSubjectEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		name := request.(string)
		v, err := s.GetSubject(ctx, name)
		if err != nil {
			return nil, err
		}
		return v.Content, nil
	}
}

func makeListSubjectsEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		v, err := s.ListSubjects(ctx)
		if err != nil {
			return nil, err
		}
		return v, nil
	}
}

func decodeSubjectRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return wiki.Subject{
		Name:    mux.Vars(r)["name"],
		Content: string(b),
	}, nil
}

func makeCreateSubjectEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		subject := request.(wiki.Subject)
		return nil, s.CreateSubject(ctx, &subject)
	}
}

func makeEditSubjectEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		subject := request.(wiki.Subject)
		return nil, s.EditSubject(ctx, &subject)
	}
}

func decodeListSubjectEditsRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	return mux.Vars(r)["name"], nil
}

func makeListSubjectEditsEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		name := request.(string)
		v, err := s.ListSubjectEdits(ctx, name)
		if err != nil {
			return nil, err
		}
		return v, nil
	}
}

func makeFeaturedSubjectEndpoint(s wiki.Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		return s.FeaturedSubject(ctx)
	}
}
