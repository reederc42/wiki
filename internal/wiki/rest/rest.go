package rest

import (
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/internal/wiki"
)

// Handle adds routes to router
func Handle(r *mux.Router, s wiki.Service, l *zap.Logger) *mux.Router {
	r.Path("/api/v1/subject/__featured").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeFeaturedSubjectEndpoint(s),
			kithttp.NopRequestDecoder,
			wiki.EncodePlainResponse,
			serverOptions(l, s)...,
		))
	r.Path("/api/v1/subject/{name}").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeGetSubjectEndpoint(s),
			decodeGetSubjectRequest,
			kithttp.EncodeJSONResponse,
			serverOptions(l, s)...,
		))
	r.Path("/api/v1/subject/").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeListSubjectsEndpoint(s),
			kithttp.NopRequestDecoder,
			kithttp.EncodeJSONResponse,
			serverOptions(l, s)...,
		))
	r.Path("/api/v1/subject/{name}").
		Methods(http.MethodPost).
		Handler(kithttp.NewServer(
			makeCreateSubjectEndpoint(s),
			decodeSubjectRequest,
			wiki.EncodeEmptyResponse,
			serverOptionsWithAuth(l, s)...,
		))
	r.Path("/api/v1/subject/{name}").
		Methods(http.MethodPut).
		Handler(kithttp.NewServer(
			makeEditSubjectEndpoint(s),
			decodeSubjectRequest,
			wiki.EncodeEmptyResponse,
			serverOptionsWithAuth(l, s)...,
		))
	r.Path("/api/v1/subject/edits/{name}").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeListSubjectEditsEndpoint(s),
			decodeListSubjectEditsRequest,
			kithttp.EncodeJSONResponse,
			serverOptions(l, s)...,
		))
	return r
}

func serverOptions(l *zap.Logger, s wiki.Service) []kithttp.ServerOption {
	return []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(wiki.EncodeError),
		kithttp.ServerBefore(wiki.ServerBeforeAddTrace(l, s.ComponentName())),
		kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
	}
}

func serverOptionsWithAuth(l *zap.Logger,
	s wiki.Service,
) []kithttp.ServerOption {
	return []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(wiki.EncodeError),
		kithttp.ServerBefore(
			wiki.ServerBeforeAddTrace(l, s.ComponentName()),
			auth.ServerBefore,
		),
		kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
	}
}
