package wiki

import (
	"context"
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/pkg/trace"
)

func EncodeEmptyResponse(_ context.Context, w http.ResponseWriter,
	_ interface{},
) error {
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func EncodePlainResponse(_ context.Context, w http.ResponseWriter,
	response interface{},
) error {
	res := []byte(response.(string))
	_, err := w.Write(res)
	return err
}

func EncodeError(ctx context.Context, err error, w http.ResponseWriter) {
	trace.L(ctx).Error("error serving request", zap.Error(err))
	if h, ok := err.(errors.HTTPStatuser); ok {
		w.WriteHeader(h.HTTPStatus())
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	_, _ = w.Write([]byte(err.Error()))
}

func ServerBeforeAddTrace(l *zap.Logger,
	component string,
) kithttp.RequestFunc {
	return func(ctx context.Context, _ *http.Request) context.Context {
		return trace.Set(ctx, trace.New(l).With(
			zap.String("component", component),
		))
	}
}

func ServerFinalizerEndTrace(ctx context.Context, code int, r *http.Request) {
	l := trace.L(ctx).With(zap.Int("code", code),
		zap.String("path", r.URL.Path), zap.String("method", r.Method))
	if 200 <= code && code <= 299 {
		l.Info("success")
	} else {
		l.Error("error")
	}
}
