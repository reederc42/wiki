package wiki

import (
	"context"

	"gitlab.com/reederc42/wiki/internal/errors"
)

// UnimplementedService default, example implementation of wiki service
type UnimplementedService struct{}

var _ Service = &UnimplementedService{}

func (us *UnimplementedService) GetSubject(_ context.Context,
	_ string,
) (*Subject, error) {
	return nil, errors.ErrNotImplemented{
		Functionality: "GetSubject",
	}
}

func (us *UnimplementedService) ListSubjects(_ context.Context) ([]string, error) {
	return nil, errors.ErrNotImplemented{
		Functionality: "ListSubjects",
	}
}

func (us *UnimplementedService) ListSubjectEdits(_ context.Context,
	name string,
) ([]SubjectEdit, error) {
	return nil, errors.ErrNotImplemented{
		Functionality: "ListSubjectEdits",
	}
}

func (us *UnimplementedService) CreateSubject(_ context.Context,
	_ *Subject,
) error {
	return errors.ErrNotImplemented{
		Functionality: "CreateSubject",
	}
}

func (us *UnimplementedService) EditSubject(_ context.Context,
	_ *Subject,
) error {
	return errors.ErrNotImplemented{
		Functionality: "EditSubject",
	}
}

func (us *UnimplementedService) FeaturedSubject(
	_ context.Context,
) (string, error) {
	return "", errors.ErrNotImplemented{
		Functionality: "FeaturedSubject",
	}
}

func (us *UnimplementedService) ComponentName() string {
	return "unimplemented"
}
