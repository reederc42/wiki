package wiki

import (
	"context"
	"time"
)

type Subject struct {
	Name    string `json:"name,omitempty"`
	Content string `json:"content,omitempty"`
}

type SubjectEdit struct {
	Diff    string    `json:"diff"`
	Created time.Time `json:"created"`
	User    string    `json:"user"`
}

type Service interface {
	GetSubject(context.Context, string) (*Subject, error)
	ListSubjects(context.Context) ([]string, error)
	CreateSubject(context.Context, *Subject) error
	EditSubject(context.Context, *Subject) error
	ListSubjectEdits(context.Context, string) ([]SubjectEdit, error)
	FeaturedSubject(context.Context) (string, error)
	ComponentName() string
}
