// Package mdlwiki is the model-backed business layer of wiki's pkg
package mdlwiki

import (
	"context"

	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/model"
	"gitlab.com/reederc42/wiki/internal/wiki"
	"gitlab.com/reederc42/wiki/pkg/patch"
	"gitlab.com/reederc42/wiki/pkg/trace"
)

type Service struct {
	Model     model.Model
	GetUserID auth.AuthorizeServiceFunc
}

var _ wiki.Service = &Service{}

func (s *Service) GetSubject(ctx context.Context,
	name string,
) (*wiki.Subject, error) {
	trace.L(ctx).Info("GetSubject")
	subject, err := s.Model.Subject.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	subjectEdit, err := s.Model.SubjectEdit.GetBySubject(ctx, subject.ID)
	if err != nil {
		return nil, err
	}
	if len(subjectEdit) < 1 {
		return nil, errors.ErrNotFound{
			Resource: name,
		}
	}
	var c string
	for i := range subjectEdit {
		p, err := patch.FromString(subjectEdit[i].Diff)
		if err != nil {
			return nil, err
		}
		c = p.Apply(c)
	}
	return &wiki.Subject{
		Name:    name,
		Content: c,
	}, nil
}

func (s *Service) ListSubjects(ctx context.Context) ([]string, error) {
	trace.L(ctx).Info("ListSubject")
	names, err := s.Model.Subject.List(ctx)
	if err != nil {
		return nil, err
	}
	return names, nil
}

func (s *Service) CreateSubject(ctx context.Context,
	subject *wiki.Subject,
) error {
	l := trace.L(ctx)
	l.Info("CreateSubject")
	userID, err := s.GetUserID(ctx)
	if err != nil {
		l.Error("authorization error", zap.Error(err))
		return errors.ErrUnauthorized{}
	}
	if subject.Name == "" || subject.Content == "" {
		return errors.ErrInvalidInput{
			Message: "name and content must not be empty",
		}
	}
	newSubject, err := s.Model.Subject.Create(ctx, subject.Name)
	if err != nil {
		return err
	}
	p := patch.Find("", subject.Content)
	_, err = s.Model.SubjectEdit.Create(ctx, newSubject.ID, userID, p.String())
	return err
}

func (s *Service) EditSubject(ctx context.Context,
	subject *wiki.Subject,
) error {
	l := trace.L(ctx)
	l.Info("EditSubject")
	userID, err := s.GetUserID(ctx)
	if err != nil {
		l.Error("authorization error", zap.Error(err))
		return errors.ErrUnauthorized{}
	}
	if subject.Name == "" || subject.Content == "" {
		return errors.ErrInvalidInput{
			Message: "name and content must not be empty",
		}
	}
	subj, err := s.Model.Subject.GetByName(ctx, subject.Name)
	if err != nil {
		return err
	}
	subjectEdit, err := s.Model.SubjectEdit.GetBySubject(ctx, subj.ID)
	if err != nil {
		return err
	}
	var c string
	for i := range subjectEdit {
		p, err := patch.FromString(subjectEdit[i].Diff)
		if err != nil {
			return err
		}
		c = p.Apply(c)
	}
	p := patch.Find(c, subject.Content)
	_, err = s.Model.SubjectEdit.Create(ctx, subj.ID, userID, p.String())
	return err
}

func (s *Service) ListSubjectEdits(ctx context.Context,
	name string,
) ([]wiki.SubjectEdit, error) {
	trace.L(ctx).Info("ListSubjectEdits")
	subject, err := s.Model.Subject.GetByName(ctx, name)
	if err != nil {
		return nil, err
	}
	subjectEdit, err := s.Model.SubjectEdit.GetBySubject(ctx, subject.ID)
	if err != nil {
		return nil, err
	}
	se := make([]wiki.SubjectEdit, len(subjectEdit))
	for k, v := range subjectEdit {
		user, err := s.Model.User.Get(ctx, v.UserID)
		if err != nil {
			return nil, err
		}
		se[len(subjectEdit)-k-1] = wiki.SubjectEdit{
			Diff:    v.Diff,
			Created: v.Created,
			User:    user.Name,
		}
	}
	return se, nil
}

func (s *Service) FeaturedSubject(ctx context.Context) (string, error) {
	l := trace.L(ctx)
	l.Info("FeaturedSubject")
	subj, err := s.Model.Subject.Random(ctx)
	if err != nil {
		return "", err
	}
	return subj.Name, nil
}

func (s *Service) ComponentName() string {
	return "pgwiki"
}
