package tools

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	defaultHost = "http://localhost/"
	apiPath     = "/api/v1/subject"
	signinPath  = "/auth/v1/signin"
	authPath    = "/auth/v1"
)

var postDataCmd = &cobra.Command{
	Use: "post-data",
	PreRun: func(cmd *cobra.Command, _ []string) {
		_ = viper.BindPFlags(cmd.Flags())
	},
	RunE: func(_ *cobra.Command, _ []string) error {
		var input []mock
		if err := json.NewDecoder(os.Stdin).Decode(&input); err != nil {
			return err
		}
		u, err := url.Parse(viper.GetString("host"))
		if err != nil {
			return err
		}
		authURL, err := url.Parse(viper.GetString("host"))
		if err != nil {
			return err
		}
		refreshToken, err := getAuthToken(u, viper.GetString("username"),
			viper.GetString("password"), signinPath)
		if err != nil {
			return err
		}
		for i := range input {
			u.Path = path.Join(apiPath, input[i].Subject)
			authURL.Path = path.Join(authPath, http.MethodPost, u.Path)
			r, err := http.NewRequest(http.MethodGet, authURL.String(), nil)
			if err != nil {
				return err
			}
			r.Header.Add("Authorization", "Bearer "+refreshToken)
			res, err := http.DefaultClient.Do(r)
			if err != nil || res.StatusCode != http.StatusOK {
				if err == nil {
					err = fmt.Errorf("status: %s path: %s", res.Status,
						r.URL.Path)
				}
				return err
			}
			t, err := io.ReadAll(res.Body)
			if err != nil {
				return err
			}
			b := bytes.NewBufferString(input[i].Content)
			r, err = http.NewRequest(http.MethodPost, u.String(), b)
			if err != nil {
				return err
			}
			r.Header.Add("Authorization", "Bearer "+string(t))
			res, err = http.DefaultClient.Do(r)
			if err != nil || res.StatusCode != http.StatusNoContent {
				if err == nil {
					err = fmt.Errorf("status: %s path: %s", res.Status,
						r.URL.Path)
				}
				return err
			}
			fmt.Fprintf(os.Stderr, "Info: posted content %d/%d\n", i+1,
				len(input))
		}
		return nil
	},
}

func getAuthToken(u *url.URL, username, password, path string) (string, error) {
	u.Path = path
	r, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return "", err
	}
	r.SetBasicAuth(username, password)
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return "", err
	}
	b, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("could not authorize: %s: %s", res.Status,
			string(b))
	}
	return string(b), nil
}

func init() {
	toolsCmd.AddCommand(postDataCmd)
	postDataCmd.Flags().StringP("host", "a", defaultHost, "wiki host URL")
	postDataCmd.Flags().StringP("username", "u", "", "username")
	postDataCmd.Flags().StringP("password", "p", "", "password")
}
