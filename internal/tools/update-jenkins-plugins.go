package tools

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	defaultPluginSite = "https://plugins.jenkins.io/"
	versionREString   = `<h5>Version: (.*?)</h5>`
)

var updateJenkinsPluginsCmd = &cobra.Command{
	Use:   "update-jenkins-plugins [plugins-file]",
	Short: "Updates Jenkins plugins file in-place",
	Args:  cobra.ExactArgs(1),
	PreRun: func(cmd *cobra.Command, _ []string) {
		_ = viper.BindPFlags(cmd.Flags())
	},
	RunE: func(_ *cobra.Command, args []string) error {
		versionRE := regexp.MustCompile(versionREString)
		stat, err := os.Stat(args[0])
		if err != nil {
			return err
		}
		contents, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}
		plugins := parsePluginFile(contents)
		sortedPluginNames := sortKeys(plugins)
		pluginsSite := viper.GetString("plugins-site")
		for _, k := range sortedPluginNames {
			fmt.Printf("Updating plugin: %s\n", k)
			cv, err := currentVersion(k, pluginsSite, versionRE)
			if err != nil {
				return err
			}
			v := plugins[k]
			switch {
			case v == "":
				plugins[k] = cv
				fmt.Printf("  Updated to: %s\n", cv)
			case v == cv:
				fmt.Printf("  Already at latest version: %s\n", v)
			case v != cv:
				plugins[k] = cv
				fmt.Printf("  Updated from: %s -> %s\n", v, cv)
			}
		}
		return writePluginFile(plugins, sortedPluginNames, args[0],
			stat.Mode())
	},
}

func init() {
	toolsCmd.AddCommand(updateJenkinsPluginsCmd)
	updateJenkinsPluginsCmd.Flags().String("plugins-site", defaultPluginSite,
		"Jenkins plugins site")
}

func parsePluginFile(contents []byte) map[string]string {
	m := make(map[string]string)
	b := bytes.NewBuffer(contents)
	s := bufio.NewScanner(b)
	for s.Scan() {
		if s.Text() != "" {
			pv := strings.SplitN(s.Text(), ":", 2)
			if len(pv) == 2 {
				m[pv[0]] = pv[1]
			} else {
				m[pv[0]] = ""
			}
		}
	}
	return m
}

func currentVersion(plugin, pluginsSite string,
	versionRE *regexp.Regexp,
) (string, error) {
	resp, err := http.Get(pluginsSite + plugin + "/")
	if err != nil {
		return "", err
	}
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	m := versionRE.FindStringSubmatch(string(b))
	if m[1] == "" {
		return "", fmt.Errorf("could not parse version")
	}
	return m[1], nil
}

func sortKeys(m map[string]string) []string {
	sorted := make([]string, 0, len(m))
	for k := range m {
		sorted = append(sorted, k)
	}
	sort.Strings(sorted)
	return sorted
}

func writePluginFile(plugins map[string]string, sortedKeys []string,
	filename string, perm os.FileMode,
) error {
	var b strings.Builder
	for _, k := range sortedKeys {
		b.WriteString(fmt.Sprintf("%s:%s\n", k, plugins[k]))
	}
	return os.WriteFile(filename, []byte(b.String()), perm)
}
