package tools

import (
	"fmt"
	"sort"
	"strings"

	"github.com/spf13/cobra"
)

var tags = map[string][]string{
	"omnibus": {
		"no_tools",
		"omnibusdist",
	},
	"omnibus-compressed": {
		"no_tools",
		"omnibusdist",
		"compressdist",
	},
	"api": {
		"no_auth_reconciler",
		"no_bolt",
		"no_config_yaml",
		"no_storage_auth",
		"no_tools",
		"no_ui",
	},
	"migrate": {
		"no_api",
		"no_auth_config",
		"no_auth_reconciler",
		"no_bolt",
		"no_config_yaml",
		"no_postgres_storage",
		"no_redis",
		"no_server",
		"no_service",
		"no_storage_auth",
		"no_tools",
		"no_ui",
	},
	"secrets": {
		"no_api",
		"no_bolt",
		"no_config_yaml",
		"no_migrate",
		"no_migrate",
		"no_postgres_config",
		"no_postgres_storage",
		"no_server",
		"no_storage_auth",
		"no_tools",
		"no_ui",
	},
	"ui": {
		"no_api",
		"no_auth_reconciler",
		"no_bolt",
		"no_config_yaml",
		"no_migrate",
		"no_postgres_config",
		"no_postgres_storage",
		"no_redis",
		"no_storage_auth",
		"no_tools",
		"compressdist",
	},
}

var tagsCmd = &cobra.Command{
	Use:   "tags",
	Short: "print Go build tags for binary",
	Long:  "print Go build tags for binaries:\n" + verticalSlice(mapKeysToSlice(tags), 2),
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return fmt.Errorf("requires 1 binary of: %v", mapKeysToSlice(tags))
		}
		keys := mapKeysToSlice(tags)
		if notIn(args[0], keys) {
			return fmt.Errorf("binary not in: %v", keys)
		}

		return nil
	},
	Run: func(_ *cobra.Command, args []string) {
		fmt.Printf("%s", strings.Join(tags[args[0]], " "))
	},
}

func init() {
	toolsCmd.AddCommand(tagsCmd)
}

func mapKeysToSlice(m map[string][]string) []string {
	l := make([]string, 0, len(m))
	for k := range m {
		l = append(l, k)
	}
	sort.Strings(l)

	return l
}

func verticalSlice(s []string, padding int) string {
	var b strings.Builder
	p := strings.Repeat(" ", padding)
	for _, k := range s {
		b.WriteString(p + k + "\n")
	}

	return b.String()
}

func notIn(s string, l []string) bool {
	for _, t := range l {
		if s == t {
			return false
		}
	}

	return true
}
