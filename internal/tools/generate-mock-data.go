package tools

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultSource = "http://jaspervdj.be/lorem-markdownum/markdown.txt"

var (
	generateMockDataCmd = &cobra.Command{
		Use: "generate-mock-data",
		PreRun: func(cmd *cobra.Command, _ []string) {
			_ = viper.BindPFlags(cmd.Flags())
		},
		RunE: func(_ *cobra.Command, _ []string) error {
			defaultOptions["num-blocks"] = strconv.FormatInt(
				viper.GetInt64("num-blocks"), 10)
			d, err := getMockData(viper.GetString("source"),
				viper.GetInt("num-subjects"), defaultOptions)
			if err != nil {
				return err
			}
			if viper.GetBool("plain") {
				_, err := fmt.Print(formatPlain(d))
				return err
			}
			return json.NewEncoder(os.Stdout).Encode(d)
		},
	}
	defaultOptions = map[string]string{
		"no-external-links": "on",
	}
)

func init() {
	toolsCmd.AddCommand(generateMockDataCmd)
	generateMockDataCmd.Flags().String("source", defaultSource,
		"source of generated content")
	generateMockDataCmd.Flags().IntP("num-subjects", "n", 1,
		"number of subjects")
	generateMockDataCmd.Flags().Int("num-blocks", 5,
		"number of blocks per subject")
	generateMockDataCmd.Flags().Bool("plain", false,
		"output plain-ish string instead of json")
}

type mock struct {
	Subject string `json:"subject"`
	Content string `json:"content"`
}

func getMockData(source string, n int,
	options map[string]string,
) ([]mock, error) {
	data := make([]mock, 0, n)
	for i := 0; i < n; i++ {
		c, err := getRawContent(source, options)
		if err != nil {
			return nil, err
		}
		s := getSubject(c)
		data = append(data, mock{
			Subject: s,
			Content: removeLinks(c),
		})
		fmt.Fprintf(os.Stderr, "info: got content %d/%d\n", i+1, n)
	}
	return data, nil
}

func getRawContent(source string, options map[string]string) (string, error) {
	u, err := url.Parse(source)
	if err != nil {
		return "", err
	}
	q := url.Values{}
	for k, v := range options {
		q.Set(k, v)
	}
	u.RawQuery = q.Encode()
	r, err := http.Get(u.String())
	if err != nil {
		return "", err
	}
	b, err := io.ReadAll(r.Body)
	if err != nil {
		return "", err
	}
	if r.StatusCode != http.StatusOK {
		return "", fmt.Errorf("could not get content: %s: %s", r.Status,
			string(b))
	}
	return string(b), nil
}

func getSubject(content string) string {
	t := strings.TrimPrefix(strings.SplitN(content, "\n", 2)[0], "# ")
	s := strings.Replace(t, " ", "_", -1)
	return s
}

func removeLinks(content string) string {
	return regexp.MustCompile(`(?s)\[([^\[\]]+)\]\(#[^\(\)]+\)`).
		ReplaceAllString(content, "$1")
}

func formatPlain(d []mock) string {
	sm := make([]string, len(d))
	for k, v := range d {
		sm[k] = v.Content
	}
	return strings.Join(sm, "\n")
}
