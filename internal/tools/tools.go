package tools

import (
	"github.com/spf13/cobra"
)

var toolsCmd = &cobra.Command{
	Use:   "tools",
	Short: "build tools",
}

func GetCommand() *cobra.Command {
	return toolsCmd
}
