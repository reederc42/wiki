package tools

import (
	"io"
	"os"
	"path"
	"text/template"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/config/env"
)

var (
	output       string
	templateFile string
	export       bool
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "prints configuration",
	RunE: func(_ *cobra.Command, _ []string) error {
		var o io.Writer
		if output != "" {
			f, err := os.Create(output)
			if err != nil {
				return err
			}
			o = f
			defer f.Close()
		} else {
			o = os.Stdout
		}
		if templateFile == "" {
			if export {
				config.SetEncoder(env.DefaultEncoder)
			}
			return config.Write(o)
		}
		tpl, err := template.New(path.Base(templateFile)).
			Funcs(template.FuncMap{
				// durationSub returns a - b
				"durationSub": func(a, b time.Duration) time.Duration {
					return time.Duration(a.Nanoseconds() - b.Nanoseconds())
				},
				// converts duration to milliseconds, for pipelines
				"toMilliseconds": func(d time.Duration) int64 {
					return d.Milliseconds()
				},
			}).ParseFiles(templateFile)
		if err != nil {
			return err
		}
		return tpl.Execute(o, config.Map())
	},
}

func init() {
	toolsCmd.AddCommand(configCmd)
	configCmd.Flags().StringVarP(&output, "output", "o", "",
		"output file (default stdout)")
	configCmd.Flags().StringVarP(&templateFile, "template", "t", "",
		"go template file (default prints yaml)")
	configCmd.Flags().BoolVarP(&export, "export", "e", false,
		"exports configuration as environment variables")
}

type CIConfig struct {
	// ContainerCMD command line utility that manages containers
	ContainerCMD string `yaml:"containerCMD"`
	// Runner CI step runner provider, such as docker/containerd and shell
	Runner string `yaml:"runner"`
	// Coverage if true collect code coverage
	Coverage bool `yaml:"coverage"`
	// GoDisablePostgres disables Go unit tests that require a postgres instance
	GoDisablePostgres bool `yaml:"goDisablePostgres"`
	// GoCache stores Go caches
	GoCache string `yaml:"goCache"`
	// LocalCleanup enables cleaning local filesystem of CI artifacts
	LocalCleanup bool `yaml:"localCleanup"`
	// LocalArchive archives CI artifacts into local tarball
	LocalArchive bool `yaml:"localArchive"`
	// PostgresPassword password for postgres test instances. Should be deterministic
	// and consistent for all steps
	PostgresPassword string `yaml:"postgresPassword"`
	// ClusterID id for test cluster. Should be deterministic and consistent for all
	// steps
	ClusterID string `yaml:"clusterID"`
	// BranchName used to override branch name detection
	BranchName string `yaml:"branchName"`
	// DockerRegistry deployment registry
	DockerRegistry string `yaml:"dockerRegistry"`
	// ArchiveTestImages if true, includes test images in artifacts
	ArchiveTestImages bool `yaml:"archiveTestImages"`
	// CleanupImages if false, does not clean up images during cleanup
	CleanupImages bool `yaml:"cleanupImages"`
	// RegistryMirror image registry mirror
	RegistryMirror string `yaml:"registryMirror"`
	// ContainerWorkspaceMount directory to mount in containers started by CI
	ContainerWorkspaceMount string `yaml:"containerWorkspaceMount"`
	// Kind kind test cluster configuration
	Kind KindConfig `yaml:"kind"`
	// Prod holds production configuration
	Prod ProductionConfig `yaml:"prod"`
}

// ProductionConfig holds production configuration
type ProductionConfig struct {
	// Kubeconfig kubeconfig file
	Kubeconfig string `yaml:"kubeconfig" env:"KUBECONFIG"`
	// PostgresPassword postgres password
	PostgresPassword string `yaml:"postgresPassword"`
	// VaultToken vault token
	VaultToken string `yaml:"vaultToken"`
	// VaultURL vault URL
	VaultURL string `yaml:"vaultURL"`
}

type KindConfig struct {
	// NodeImage node image
	NodeImage string `yaml:"nodeImage"`
	// DockerMirror docker mirror (without scheme)
	DockerMirror string `yaml:"dockerMirror"`
	// ExposePort exposes port on free host port
	ExposePort int `yaml:"exposePort"`
	// APIServerAddress ip address of api server
	APIServerAddress string `yaml:"apiServerAddress"`
}

var DefaultCIConfig = CIConfig{
	ContainerCMD:  "docker",
	Runner:        "container",
	Coverage:      true,
	LocalCleanup:  true,
	LocalArchive:  true,
	CleanupImages: true,
	Kind: KindConfig{
		NodeImage: "kindest/node:v1.27.3",
	},
}
