package postgres

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/model"
)

type dbUser struct {
	db *sqlx.DB
}

func (du *dbUser) Create(ctx context.Context, name,
	password string,
) (*model.User, error) {
	const query = `INSERT INTO wiki_user (name, password, created)
VALUES ($1, crypt($2, gen_salt('bf')), $3)
RETURNING id, name, created;`
	u := &model.User{}
	err := du.db.GetContext(ctx, u, query, name, password, time.Now().UTC())
	return u, err
}

func (du dbUser) List(ctx context.Context) ([]*model.User, error) {
	const query = `SELECT id, name, created FROM wiki_user;`
	users := []*model.User{}
	err := du.db.SelectContext(ctx, &users, query)
	return users, err
}

func (du dbUser) GetByNameAndPassword(ctx context.Context, name,
	password string,
) (*model.User, error) {
	const query = `SELECT id, name, created FROM wiki_user WHERE name = $1
AND password = crypt($2, password);`
	u := &model.User{}
	err := du.db.GetContext(ctx, u, query, name, password)
	return u, err
}

func (du dbUser) Get(ctx context.Context, id int) (*model.User, error) {
	const query = `SELECT id, name, created FROM wiki_user WHERE id = $1;`
	u := &model.User{}
	err := du.db.GetContext(ctx, u, query, id)
	return u, err
}

func NewDBUser(db *sqlx.DB) model.DBUser {
	return &dbUser{db: db}
}
