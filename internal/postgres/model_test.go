//go:build all || postgres

package postgres_test

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/DATA-DOG/go-txdb"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/reederc42/wiki/internal/postgres"
)

const dbConnectionTimeout = 30 * time.Second

func TestMain(m *testing.M) {
	c, err := testEntry(m)
	if err != nil {
		panic(err)
	}
	os.Exit(c)
}

func testEntry(m *testing.M) (int, error) {
	c := configure()
	txdb.Register("pgtx", "postgres",
		postgres.MakeConnectionString(&c))
	return m.Run(), nil
}

func getTestDB(t *testing.T) *sqlx.DB {
	t.Helper()
	ctx, cancel := context.WithTimeout(context.Background(), dbConnectionTimeout)
	defer cancel()

	var (
		db  *sqlx.DB
		err error
	)
	select {
	case <-ctx.Done():
		err = errors.WithMessage(err, ctx.Err().Error())
	case <-func() chan struct{} {
		done := make(chan struct{})
		go func() {
			for {
				connDB, connErr := sqlx.Open("pgtx", fmt.Sprintf("pgtx_%d", rand.Int()))
				if connErr != nil {
					err = connErr
					continue
				}
				s, sErr := connDB.Prepare("SELECT 1;")
				if sErr != nil {
					err = sErr
					continue
				}
				_, eErr := s.Exec()
				if eErr != nil {
					err = eErr
					continue
				}
				// successfully connected to db
				db = connDB
				done <- struct{}{}
				return
			}
		}()
		return done
	}():
		err = nil
	}
	require.NoError(t, err)
	return db
}

func configure() postgres.Connection {
	const (
		hostEnv     = "WIKI_POSTGRES_HOST"
		userEnv     = "WIKI_POSTGRES_USER"
		passwordEnv = "WIKI_POSTGRES_PASSWORD"
		databaseEnv = "WIKI_POSTGRES_DATABASE"
		portEnv     = "WIKI_POSTGRES_PORT"
		insecureEnv = "WIKI_POSTGRES_INSECURE"
	)
	c := postgres.DefaultConnection
	host := os.Getenv(hostEnv)
	if host != "" {
		c.Host = host
	}
	user := os.Getenv(userEnv)
	if user != "" {
		c.User = user
	}
	password := os.Getenv(passwordEnv)
	if password != "" {
		c.Password = password
	}
	database := os.Getenv(databaseEnv)
	if database != "" {
		c.Database = database
	}
	port := os.Getenv(portEnv)
	if port != "" {
		c.Port = port
	}
	insecureValue := os.Getenv(insecureEnv)
	if insecureValue != "" {
		if insecureValue == "true" {
			c.Insecure = true
		} else if insecureValue == "false" {
			c.Insecure = false
		}
	}
	return c
}
