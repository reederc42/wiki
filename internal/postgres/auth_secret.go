package postgres

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/model"
)

type dbAuthSecret struct {
	db *sqlx.DB
}

func (das *dbAuthSecret) CreateIfNotExists(ctx context.Context,
	as *model.Secret,
) error {
	const query = `INSERT INTO auth_secret (id, secret, retires, expires)
		VALUES ($1, $2, $3, $4)
		ON CONFLICT DO NOTHING;`
	_, err := das.db.ExecContext(ctx, query, as.ID, as.Secret, as.Retires,
		as.Expires)
	return err
}

func (das *dbAuthSecret) Create(ctx context.Context, secret string, retires,
	expires time.Time,
) error {
	const query = `INSERT INTO auth_secret (secret, retires, expires)
		VALUES ($1, $2, $3);`
	_, err := das.db.ExecContext(ctx, query, secret, retires, expires)
	return err
}

func (das *dbAuthSecret) List(ctx context.Context) ([]*model.Secret, error) {
	const query = `SELECT * FROM auth_secret ORDER BY id DESC;`
	l := []*model.Secret{}
	if err := das.db.SelectContext(ctx, &l, query); err != nil {
		return nil, err
	}
	return l, nil
}

func (das *dbAuthSecret) Delete(ctx context.Context, id int) error {
	const query = `DELETE FROM auth_secret WHERE id = $1;`
	_, err := das.db.ExecContext(ctx, query, id)
	return err
}

func NewDBAuthSecret(db *sqlx.DB) model.DBSecret {
	return &dbAuthSecret{db: db}
}
