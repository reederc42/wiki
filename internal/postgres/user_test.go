//go:build all || postgres

package postgres_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
	pg "gitlab.com/reederc42/wiki/internal/postgres"
)

func TestUser(t *testing.T) {
	t.Parallel()
	db := getTestDB(t)
	defer db.Close()
	model_test.User(t, model.Model{
		User: pg.NewDBUser(db),
	})
}

func TestUserGet(t *testing.T) {
	t.Parallel()
	db := getTestDB(t)
	defer db.Close()
	model_test.UserGet(t, model.Model{
		User: pg.NewDBUser(db),
	})
}
