package postgres

import (
	"context"
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/model"
)

type dbSubject struct {
	db *sqlx.DB
}

func (ds *dbSubject) Get(ctx context.Context, id int) (*model.Subject, error) {
	const query = `SELECT * FROM subject WHERE id=$1;`
	s := &model.Subject{}
	err := ds.db.GetContext(ctx, s, query, id)
	return s, err
}

func (ds *dbSubject) Create(ctx context.Context, name string) (*model.Subject,
	error,
) {
	const query = `INSERT INTO subject (name, created)
		VALUES ($1, $2)
		RETURNING *;`
	s := &model.Subject{}
	t := time.Now().UTC()
	err := ds.db.GetContext(ctx, s, query, name, t)
	return s, err
}

func (ds *dbSubject) GetByName(ctx context.Context, name string) (*model.Subject,
	error,
) {
	const query = `SELECT * FROM subject WHERE name=$1;`
	s := &model.Subject{}
	err := ds.db.GetContext(ctx, s, query, name)
	if err == sql.ErrNoRows {
		err = errors.ErrNotFound{
			Resource: name,
		}
	}
	return s, err
}

func (ds *dbSubject) List(ctx context.Context) ([]string, error) {
	const query = `SELECT name FROM subject;`
	s := []string{}
	if err := ds.db.SelectContext(ctx, &s, query); err != nil {
		return nil, err
	}
	return s, nil
}

func (ds *dbSubject) Random(ctx context.Context) (*model.Subject, error) {
	const query = `SELECT * FROM subject ORDER BY RANDOM() LIMIT 1;`
	s := &model.Subject{}
	err := ds.db.GetContext(ctx, s, query)
	if err == sql.ErrNoRows {
		return &model.Subject{Name: ""}, nil
	}
	return s, err
}

func NewDBSubject(db *sqlx.DB) model.DBSubject {
	return &dbSubject{db: db}
}
