package postgres

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/model"
)

type dbServiceSecret struct {
	db *sqlx.DB
}

func (dss *dbServiceSecret) CreateIfNotExists(ctx context.Context,
	as *model.Secret,
) error {
	const query = `INSERT INTO service_secret (id, secret, retires, expires)
		VALUES ($1, $2, $3, $4)
		ON CONFLICT DO NOTHING;`
	_, err := dss.db.ExecContext(ctx, query, as.ID, as.Secret, as.Retires,
		as.Expires)
	return err
}

func (dss *dbServiceSecret) Create(ctx context.Context, secret string, retires,
	expires time.Time,
) error {
	const query = `INSERT INTO service_secret (secret, retires, expires)
		VALUES ($1, $2, $3);`
	_, err := dss.db.ExecContext(ctx, query, secret, retires, expires)
	return err
}

func (dss *dbServiceSecret) List(ctx context.Context) ([]*model.Secret, error) {
	const query = `SELECT * FROM service_secret ORDER BY id DESC;`
	l := []*model.Secret{}
	if err := dss.db.SelectContext(ctx, &l, query); err != nil {
		return nil, err
	}
	return l, nil
}

func (dss *dbServiceSecret) Delete(ctx context.Context, id int) error {
	const query = `DELETE FROM service_secret WHERE id = $1;`
	_, err := dss.db.ExecContext(ctx, query, id)
	return err
}

func NewDBServiceSecret(db *sqlx.DB) model.DBSecret {
	return &dbServiceSecret{db: db}
}
