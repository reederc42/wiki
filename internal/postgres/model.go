package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/model"

	_ "github.com/lib/pq"
)

var DefaultConnection = Connection{
	Host:     "localhost",
	User:     "postgres",
	Database: "wiki",
	Insecure: false,
}

type Connection struct {
	// Host database host
	Host string
	// User database user
	User string
	// Password database password
	Password string
	// Database database name
	Database string
	// Port database port
	Port string
	// Insecure disable ssl
	Insecure bool
}

func New(conn Connection) (model.Model, func(), error) {
	db, err := sqlx.Open("postgres", MakeConnectionString(&conn))
	if err != nil {
		return model.Model{}, func() {}, err
	}
	return model.Model{
		AuthSecret:  NewDBAuthSecret(db),
		Subject:     NewDBSubject(db),
		SubjectEdit: NewDBSubjectEdit(db),
		User:        NewDBUser(db),
	}, func() { db.Close() }, nil
}

func MakeConnectionString(c *Connection) string {
	userPass := c.User
	if c.Password != "" {
		userPass += ":" + c.Password
	}
	conn := fmt.Sprintf("postgres://%s@%s:%s/%s", userPass, c.Host, c.Port,
		c.Database)
	if c.Insecure {
		conn += "?sslmode=disable"
	}
	return conn
}
