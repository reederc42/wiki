package postgres

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"

	"gitlab.com/reederc42/wiki/internal/model"
)

type dbSubjectEdit struct {
	db *sqlx.DB
}

func (dse *dbSubjectEdit) Create(ctx context.Context, subjectID, userID int,
	diff string,
) (*model.SubjectEdit, error) {
	const query = `INSERT INTO subject_edit (subject_id, user_id, diff, created)
		VALUES ($1, $2, $3, $4)
		RETURNING subject_id, user_id, diff, created;`
	se := &model.SubjectEdit{}
	t := time.Now().UTC()
	err := dse.db.GetContext(ctx, se, query, subjectID, userID, diff, t)
	return se, err
}

func (dse *dbSubjectEdit) GetBySubject(ctx context.Context,
	subjectID int,
) ([]*model.SubjectEdit, error) {
	const query = `SELECT subject_id, user_id, diff, created FROM subject_edit
		WHERE subject_id=$1
		ORDER BY created;`
	se := []*model.SubjectEdit{}
	if err := dse.db.SelectContext(ctx, &se, query, subjectID); err != nil {
		return nil, err
	}
	return se, nil
}

func NewDBSubjectEdit(db *sqlx.DB) model.DBSubjectEdit {
	return &dbSubjectEdit{db: db}
}
