-- +goose Up
-- +goose StatementBegin
CREATE TABLE service_secret (
	id serial NOT NULL,
	secret text NOT NULL,
	retires timestamptz NOT NULL,
	expires timestamptz NOT NULL,
	CONSTRAINT secret_secret_pk PRIMARY KEY (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE service_secret;
-- +goose StatementEnd
