-- +goose Up
-- +goose StatementBegin
CREATE EXTENSION pgcrypto;
CREATE TABLE IF NOT EXISTS wiki_user (
	id serial NOT NULL,
	name varchar(256) NOT NULL,
	password varchar(256) NOT NULL,
	created timestamptz NOT NULL,
	CONSTRAINT wiki_user_pk PRIMARY KEY (id),
	CONSTRAINT wiki_user_un UNIQUE (name)
);
CREATE TABLE IF NOT EXISTS subject (
	id serial NOT NULL,
	name varchar(256) NOT NULL,
	created timestamptz NOT NULL,
	CONSTRAINT subject_pk PRIMARY KEY (id),
	CONSTRAINT subject_un UNIQUE (name)
);
CREATE TABLE IF NOT EXISTS subject_edit (
	id serial NOT NULL,
	user_id int NOT NULL,
	subject_id int NOT NULL,
	diff text NOT NULL,
	created timestamptz NOT NULL,
	CONSTRAINT subject_edit_pk PRIMARY KEY (id),
	CONSTRAINT subject_edit_fk FOREIGN KEY (user_id) REFERENCES wiki_user(id),
	CONSTRAINT subject_edit_fk_1 FOREIGN KEY (subject_id) REFERENCES subject(id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE subject_edit;
DROP TABLE subject;
DROP TABLE wiki_user;
DROP EXTENSION pgcrypto;
-- +goose StatementEnd
