package migration

import (
	"embed"
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose/v3"
)

//go:embed *.sql
var FS embed.FS

const Dir = "."

func EnsureDatabase(db *sqlx.DB, name string) error {
	query := "CREATE DATABASE " + name + ";"
	_, err := db.Exec(query)
	if err != nil && !strings.Contains(err.Error(), "already exists") {
		return err
	}
	return nil
}

func MigrateDatabase(log *log.Logger, db *sqlx.DB, fs embed.FS,
	dir string,
) error {
	goose.SetLogger(log)
	goose.SetBaseFS(FS)
	return goose.Up(db.DB, dir)
}
