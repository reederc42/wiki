-- +goose Up
-- +goose StatementBegin
CREATE TABLE auth_secret (
	id serial NOT NULL,
	secret text NOT NULL,
	retires timestamptz NOT NULL,
	expires timestamptz NOT NULL,
	CONSTRAINT auth_secret_pk PRIMARY KEY (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE auth_secret;
-- +goose StatementEnd
