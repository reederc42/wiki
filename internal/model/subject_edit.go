package model

import (
	"context"
	"time"
)

type SubjectEdit struct {
	UserID    int       `db:"user_id"`
	SubjectID int       `db:"subject_id"`
	Diff      string    `db:"diff"`
	Created   time.Time `db:"created"`
}

type DBSubjectEdit interface {
	Create(ctx context.Context, subjectID, userID int,
		diff string) (*SubjectEdit, error)
	GetBySubject(ctx context.Context, subjectID int) ([]*SubjectEdit, error)
}
