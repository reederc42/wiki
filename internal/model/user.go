package model

import (
	"context"
	"time"
)

type User struct {
	ID      int       `db:"id"`
	Name    string    `db:"name"`
	Created time.Time `db:"created"`
}

type DBUser interface {
	Create(ctx context.Context, name, password string) (*User, error)
	List(ctx context.Context) ([]*User, error)
	GetByNameAndPassword(ctx context.Context, name,
		password string) (*User, error)
	Get(ctx context.Context, id int) (*User, error)
}
