package model_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/reederc42/wiki/internal/model"
)

func ServiceSecret(t *testing.T, m model.Model) {
	a1 := &model.Secret{
		ID:      1,
		Secret:  "abc",
		Retires: time.Now().UTC().Truncate(time.Microsecond),
		Expires: time.Now().UTC().Truncate(time.Microsecond),
	}
	a2 := &model.Secret{
		ID:      2,
		Secret:  "def",
		Retires: time.Now().UTC().Truncate(time.Microsecond),
		Expires: time.Now().UTC().Truncate(time.Microsecond),
	}
	err := m.ServiceSecret.CreateIfNotExists(context.Background(), a1)
	require.NoError(t, err)
	err = m.ServiceSecret.CreateIfNotExists(context.Background(), a2)
	require.NoError(t, err)
	l, err := m.ServiceSecret.List(context.Background())
	require.NoError(t, err)
	require.Equal(t, []*model.Secret{a2, a1}, l)
	err = m.ServiceSecret.CreateIfNotExists(context.Background(), a1)
	require.NoError(t, err)
	l, err = m.ServiceSecret.List(context.Background())
	require.NoError(t, err)
	require.Equal(t, []*model.Secret{a2, a1}, l)
	err = m.ServiceSecret.Delete(context.Background(), 2)
	require.NoError(t, err)
	l, err = m.ServiceSecret.List(context.Background())
	require.NoError(t, err)
	require.Equal(t, []*model.Secret{a1}, l)
}
