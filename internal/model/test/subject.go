package model_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/model"
)

func Subject(t *testing.T, m model.Model) {
	s1, err := m.Subject.Create(context.Background(), "snail")
	require.NoError(t, err)
	s2, err := m.Subject.GetByName(context.Background(), "snail")
	require.NoError(t, err)
	require.Equal(t, s1, s2)
	s3, err := m.Subject.GetByName(context.Background(), "slug")
	require.Equal(t, errors.ErrNotFound{Resource: "slug"}, err)
	require.Empty(t, s3)
	s4, err := m.Subject.Get(context.Background(), s1.ID)
	require.NoError(t, err)
	require.Equal(t, s1, s4)
	sList, err := m.Subject.List(context.Background())
	require.NoError(t, err)
	require.Equal(t, 1, len(sList))
	require.Equal(t, s1.Name, sList[0])
	s5, err := m.Subject.Random(context.Background())
	require.NoError(t, err)
	require.NotNil(t, s5)
}
