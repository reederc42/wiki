package model_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/reederc42/wiki/internal/model"
	"gitlab.com/reederc42/wiki/pkg/patch"
)

func SubjectEdit(t *testing.T, m model.Model) {
	p1 := patch.Patch{
		{
			K: patch.OpInsert,
			F: 0,
			V: "A",
		},
	}
	p2 := patch.Patch{
		{
			K: patch.OpDelete,
			T: 1,
			V: "B",
		},
	}
	s1, err := m.Subject.Create(context.Background(), "sheep")
	require.NoError(t, err)
	u1, err := m.User.Create(context.Background(), "bob", "pass")
	require.NoError(t, err)
	se1, err := m.SubjectEdit.Create(context.Background(), s1.ID, u1.ID, p1.String())
	require.NoError(t, err)
	se2, err := m.SubjectEdit.Create(context.Background(), s1.ID, u1.ID, p2.String())
	require.NoError(t, err)
	ses1 := []*model.SubjectEdit{se1, se2}
	ses2, err := m.SubjectEdit.GetBySubject(context.Background(), s1.ID)
	require.NoError(t, err)
	require.Equal(t, ses1, ses2)
}
