package model_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/reederc42/wiki/internal/model"
)

func User(t *testing.T, m model.Model) {
	u, err := m.User.Create(context.Background(), "bob", "pass")
	require.NoError(t, err)
	uList, err := m.User.List(context.Background())
	require.NoError(t, err)
	require.Equal(t, 1, len(uList))
	require.Equal(t, u, uList[0])
}

func UserGet(t *testing.T, m model.Model) {
	u, err := m.User.Create(context.Background(), "bob", "pass")
	require.NoError(t, err)
	u1, err := m.User.GetByNameAndPassword(context.Background(), "bob", "pass")
	require.NoError(t, err)
	require.Equal(t, u, u1)
	u2, err := m.User.Get(context.Background(), u.ID)
	require.NoError(t, err)
	require.Equal(t, u, u2)
	_, err = m.User.GetByNameAndPassword(context.Background(), "bob", "wrong")
	require.Error(t, err)
}
