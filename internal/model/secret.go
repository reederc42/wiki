package model

import (
	"context"
	"time"
)

type Secret struct {
	ID      int       `db:"id"`
	Secret  string    `db:"secret"`
	Retires time.Time `db:"retires"`
	Expires time.Time `db:"expires"`
}

type DBSecret interface {
	CreateIfNotExists(ctx context.Context, as *Secret) error
	Create(ctx context.Context, secret string, retires, expires time.Time) error
	List(ctx context.Context) ([]*Secret, error)
	Delete(ctx context.Context, id int) error
}
