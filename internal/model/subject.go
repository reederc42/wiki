package model

import (
	"context"
	"time"
)

type Subject struct {
	ID      int       `db:"id"`
	Name    string    `db:"name"`
	Created time.Time `db:"created"`
}

type DBSubject interface {
	Get(ctx context.Context, id int) (*Subject, error)
	Create(ctx context.Context, name string) (*Subject, error)
	GetByName(ctx context.Context, name string) (*Subject, error)
	List(ctx context.Context) ([]string, error)
	Random(ctx context.Context) (*Subject, error)
}
