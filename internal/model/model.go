// Package model currently supports a unified interface for PostgreSQL and boltDB
//
//	Preconditions:
//	  Model IDs are of type int
//	  DB annotations match SQL column names (for SQL backends only)
package model

type Model struct {
	AuthSecret    DBSecret
	ServiceSecret DBSecret
	SubjectEdit   DBSubjectEdit
	Subject       DBSubject
	User          DBUser
}
