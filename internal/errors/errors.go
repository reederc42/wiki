package errors

import (
	"fmt"
	"net/http"
)

type HTTPStatuser interface {
	HTTPStatus() int
}

type ErrNotImplemented struct {
	Functionality string
}

func (ni ErrNotImplemented) Error() string {
	return fmt.Sprintf("not implemented: %s", ni.Functionality)
}

func (ni ErrNotImplemented) HTTPStatus() int {
	return http.StatusNotImplemented
}

type ErrNotFound struct {
	Resource string
}

func (nf ErrNotFound) Error() string {
	return fmt.Sprintf("not found: %s", nf.Resource)
}

func (nf ErrNotFound) HTTPStatus() int {
	return http.StatusNotFound
}

type ErrUnauthorized struct{}

func (u ErrUnauthorized) Error() string {
	return "unauthorized"
}

func (u ErrUnauthorized) HTTPStatus() int {
	return http.StatusUnauthorized
}

type ErrInvalidInput struct {
	Message string
}

func (ii ErrInvalidInput) Error() string {
	return fmt.Sprintf("invalid input: %s", ii.Message)
}

func (ii ErrInvalidInput) HTTPStatus() int {
	return http.StatusBadRequest
}
