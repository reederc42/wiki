// Package httpserver wraps graceful shutdown and logging around http.Server
package httpserver

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"time"

	"go.uber.org/zap"
)

var DefaultHTTPConfig = HTTPConfig{
	Address:      ":http",
	ShutdownTime: 5 * time.Second,
}

type HTTPConfig struct {
	// Address TCP address of http server (Go format: prepend with ':')
	Address string `yaml:"address"`
	// DisableCORS allows all origins, methods, and headers
	DisableCORS bool `yaml:"disableCORS"`
	// ShutdownTime http server shutdown time
	ShutdownTime time.Duration `yaml:"shutdownTime"`
	// TLS TLS configuration
	TLS TLSConfig `yaml:"tls"`
}

type TLSConfig struct {
	// Enable use TLS
	Enable bool `yaml:"enable"`
	// CertFile TLS certificate file
	CertFile string `yaml:"certFile"`
	// KeyFile TLS private key file
	KeyFile string `yaml:"keyFile"`
}

type GracefulLoggedTLS struct {
	Addr                string
	Handler             http.Handler
	Logger              *zap.Logger
	TLS                 *tls.Config
	ShutdownGracePeriod time.Duration
	GracefulShutdown    chan struct{}
}

func (glt *GracefulLoggedTLS) Serve() error {
	srv := glt.makeServer()
	e := make(chan error, 1)
	// if shutdown channel exists, wait for shutdown request
	if glt.GracefulShutdown != nil {
		go func() {
			<-glt.GracefulShutdown
			glt.Logger.Info("http server shutdown requested")
			ctx, cancel := context.WithTimeout(context.Background(),
				glt.ShutdownGracePeriod)
			defer cancel()
			e <- srv.Shutdown(ctx)
		}()
	}
	// start server
	go func() {
		var err error
		glt.Logger.Info("http server started",
			zap.Bool("tls", glt.TLS != nil),
			zap.String("addr", glt.Addr))
		if glt.TLS != nil {
			err = srv.ListenAndServeTLS("", "")
		} else {
			err = srv.ListenAndServe()
		}
		if err != nil {
			if err != http.ErrServerClosed {
				e <- err
			}
		}
	}()
	// return first error received
	err := <-e
	glt.Logger.Info("http server shutdown")
	return err
}

func (glt *GracefulLoggedTLS) makeServer() *http.Server {
	return &http.Server{
		Addr:      glt.Addr,
		Handler:   glt.Handler,
		TLSConfig: glt.TLS,
		ErrorLog: log.New(&httpErrorLogToZapWriter{
			logger: glt.Logger,
		}, "", 0),
	}
}

type httpErrorLogToZapWriter struct {
	logger *zap.Logger
}

func (l *httpErrorLogToZapWriter) Write(p []byte) (int, error) {
	l.logger.Error(string(p))
	return len(p), nil
}

func HandlerDisableCORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		h.ServeHTTP(w, r)
	})
}
