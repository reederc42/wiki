//go:build all || bolt

package bolt_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
)

func TestServiceSecret(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.ServiceSecret(t, model.Model{
		ServiceSecret: bolt.NewDBServiceSecret(db),
	})
}
