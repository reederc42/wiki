//go:build all || bolt

package bolt_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
)

func TestSubjectEdit(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.SubjectEdit(t, model.Model{
		SubjectEdit: bolt.NewDBSubjectEdit(db),
		Subject:     bolt.NewDBSubject(db),
		User:        bolt.NewDBUser(db),
	})
}
