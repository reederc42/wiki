package bolt

import (
	"context"
	"encoding/json"
	"time"

	"golang.org/x/crypto/bcrypt"

	blt "go.etcd.io/bbolt"

	"gitlab.com/reederc42/wiki/internal/model"
)

func NewDBUser(db *blt.DB) model.DBUser {
	return &dbUser{db: db}
}

type dbUser struct {
	db *blt.DB
}

type userPassword struct {
	Password []byte `json:"p"`
	User     []byte `json:"u"`
}

func (du *dbUser) Create(ctx context.Context, name,
	password string,
) (*model.User, error) {
	var mu model.User
	if err := du.db.Update(func(tx *blt.Tx) error {
		un := tx.Bucket(idxUserName)
		if un.Get([]byte(name)) != nil {
			return ErrUserAlreadyExists
		}
		u := tx.Bucket(tblUser)
		id, err := u.NextSequence()
		if err != nil {
			return err
		}
		mu.ID = int(id)
		mu.Name = name
		mu.Created = time.Now().UTC().Truncate(time.Microsecond)
		b, err := json.Marshal(mu)
		if err != nil {
			return err
		}
		userKey := itob(id)
		if err := u.Put(userKey, b); err != nil {
			return err
		}
		p, err := bcrypt.GenerateFromPassword([]byte(password),
			bcrypt.DefaultCost)
		if err != nil {
			return err
		}
		up := userPassword{
			Password: p,
			User:     userKey,
		}
		c, err := json.Marshal(up)
		if err != nil {
			return err
		}
		return un.Put([]byte(name), c)
	}); err != nil {
		return nil, err
	}
	return &mu, nil
}

func (du *dbUser) List(ctx context.Context) ([]*model.User, error) {
	ul := make([]*model.User, 0)
	if err := du.db.View(func(tx *blt.Tx) error {
		u := tx.Bucket(tblUser)
		return u.ForEach(func(_, v []byte) error {
			var mu model.User
			if err := json.Unmarshal(v, &mu); err != nil {
				return err
			}
			ul = append(ul, &mu)
			return nil
		})
	}); err != nil {
		return nil, err
	}
	return ul, nil
}

func (du *dbUser) GetByNameAndPassword(ctx context.Context, name,
	password string,
) (*model.User, error) {
	var mu model.User
	if err := du.db.View(func(tx *blt.Tx) error {
		un := tx.Bucket(idxUserName)
		a := un.Get([]byte(name))
		if a == nil {
			return ErrNoUser
		}
		var up userPassword
		if err := json.Unmarshal(a, &up); err != nil {
			return err
		}
		if err := bcrypt.CompareHashAndPassword(up.Password,
			[]byte(password)); err != nil {
			return err
		}
		u := tx.Bucket(tblUser)
		b := u.Get(up.User)
		if b == nil {
			return ErrNoUser
		}
		return json.Unmarshal(b, &mu)
	}); err != nil {
		return nil, err
	}
	return &mu, nil
}

func (du *dbUser) Get(ctx context.Context, id int) (*model.User, error) {
	var mu model.User
	if err := du.db.View(func(tx *blt.Tx) error {
		u := tx.Bucket(tblUser)
		a := u.Get(itob(uint64(id)))
		if a == nil {
			return ErrNoUser
		}
		return json.Unmarshal(a, &mu)
	}); err != nil {
		return nil, err
	}
	return &mu, nil
}
