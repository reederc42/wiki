package bolt

import (
	"encoding/binary"

	"gitlab.com/reederc42/wiki/internal/model"

	blt "go.etcd.io/bbolt"
)

var DefaultBoltConfig = BoltConfig{
	Filename: "wiki.boltdb",
}

type BoltConfig struct {
	// Filename bolt db filename
	Filename string
}

var (
	tblUser               = []byte("tbl_user")
	tblSubject            = []byte("tbl_subject")
	tblSubjectEdit        = []byte("tbl_subject_edit")
	tblAuthSecret         = []byte("tbl_auth_secret")
	tblServiceSecret      = []byte("tbl_service_secret")
	idxSubjectName        = []byte("tbl_subject_name")
	idxSubjectEditSubject = []byte("idx_subject_edit_subject")
	idxUserName           = []byte("idx_user_name")
	buckets               = [][]byte{
		tblUser,
		tblSubject,
		tblSubjectEdit,
		tblAuthSecret,
		tblServiceSecret,
		idxSubjectName,
		idxSubjectEditSubject,
		idxUserName,
	}
)

// Open creates a new model and backing bolt database, or opens an existing db;
// ensures buckets exist
func Open(filename string) (model.Model, func(), error) {
	db, err := blt.Open(filename, 0o666, nil)
	if err != nil {
		return model.Model{}, func() {}, err
	}
	m, err := New(db)
	return m, func() { db.Close() }, err
}

// New creates a model from a bolt database; ensures buckets exist. Intended to
// be used with a shared bolt db
func New(db *blt.DB) (model.Model, error) {
	if err := Migrate(db); err != nil {
		return model.Model{}, err
	}
	return model.Model{
		AuthSecret:    NewDBAuthSecret(db),
		ServiceSecret: NewDBServiceSecret(db),
		Subject:       NewDBSubject(db),
		SubjectEdit:   NewDBSubjectEdit(db),
		User:          NewDBUser(db),
	}, nil
}

// Migrate ensures db is in a usable state
func Migrate(db *blt.DB) error {
	return db.Update(func(tx *blt.Tx) error {
		for _, b := range buckets {
			_, err := tx.CreateBucketIfNotExists(b)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func itob(i uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, i)
	return b
}
