//go:build all || bolt

package bolt_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
)

func TestUser(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.User(t, model.Model{
		User: bolt.NewDBUser(db),
	})
}

func TestUserGet(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.UserGet(t, model.Model{
		User: bolt.NewDBUser(db),
	})
}
