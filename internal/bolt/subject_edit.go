package bolt

import (
	"context"
	"encoding/json"
	"time"

	blt "go.etcd.io/bbolt"

	"gitlab.com/reederc42/wiki/internal/model"
)

func NewDBSubjectEdit(db *blt.DB) model.DBSubjectEdit {
	return &dbSubjectEdit{db: db}
}

type dbSubjectEdit struct {
	db *blt.DB
}

func (dse *dbSubjectEdit) Create(ctx context.Context, subjectID, userID int,
	diff string,
) (*model.SubjectEdit, error) {
	mse := model.SubjectEdit{
		UserID:    userID,
		SubjectID: subjectID,
		Diff:      diff,
	}
	if err := dse.db.Update(func(tx *blt.Tx) error {
		mse.Created = time.Now().UTC().Truncate(time.Microsecond)
		b, err := json.Marshal(mse)
		if err != nil {
			return err
		}
		se := tx.Bucket(tblSubjectEdit)
		id, err := se.NextSequence()
		if err != nil {
			return err
		}
		if err := se.Put(itob(id), b); err != nil {
			return err
		}
		var sel []uint64
		ses := tx.Bucket(idxSubjectEditSubject)
		if a := ses.Get(itob(uint64(subjectID))); a == nil {
			sel = make([]uint64, 1)
			sel[0] = id
		} else {
			if err := json.Unmarshal(a, &sel); err != nil {
				return err
			}
			sel = append(sel, id)
		}
		c, err := json.Marshal(sel)
		if err != nil {
			return err
		}
		return ses.Put(itob(uint64(subjectID)), c)
	}); err != nil {
		return nil, err
	}
	return &mse, nil
}

func (dse *dbSubjectEdit) GetBySubject(ctx context.Context,
	subjectID int,
) ([]*model.SubjectEdit, error) {
	msel := make([]*model.SubjectEdit, 0)
	if err := dse.db.View(func(tx *blt.Tx) error {
		a := tx.Bucket(idxSubjectEditSubject).Get(itob(uint64(subjectID)))
		if a == nil {
			return ErrNoSubjectEdit
		}
		var sel []uint64
		if err := json.Unmarshal(a, &sel); err != nil {
			return err
		}
		se := tx.Bucket(tblSubjectEdit)
		for _, v := range sel {
			b := se.Get(itob(v))
			if b == nil {
				return ErrNoSubjectEdit
			}
			var mse model.SubjectEdit
			if err := json.Unmarshal(b, &mse); err != nil {
				return err
			}
			msel = append(msel, &mse)
		}
		return nil
	}); err != nil {
		return nil, err
	}
	return msel, nil
}
