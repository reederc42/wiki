//go:build all || bolt

package bolt_test

import (
	"os"
	"testing"

	"go.etcd.io/bbolt"

	"gitlab.com/reederc42/wiki/internal/bolt"
)

func getTestDB(t *testing.T) (*bbolt.DB, func()) {
	t.Helper()
	f, err := os.CreateTemp("", "wiki_test_bolt_")
	if err != nil {
		t.Error(err)
	}
	db, err := bbolt.Open(f.Name(), 0o600, nil)
	if err != nil {
		t.Error(err)
	}
	if err := bolt.Migrate(db); err != nil {
		t.Error(err)
	}
	return db, func() {
		db.Close()
		f.Close()
		os.Remove(f.Name())
	}
}
