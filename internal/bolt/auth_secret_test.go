//go:build all || bolt

package bolt_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
)

func TestAuthSecret(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.AuthSecret(t, model.Model{
		AuthSecret: bolt.NewDBAuthSecret(db),
	})
}
