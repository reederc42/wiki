package bolt

import "errors"

var (
	ErrNoUser               = errors.New("user not found")
	ErrUserAlreadyExists    = errors.New("user already exists")
	ErrNoSubject            = errors.New("subject not found")
	ErrSubjectAlreadyExists = errors.New("subject already exists")
	ErrNoSubjectEdit        = errors.New("subject edit not found")
)
