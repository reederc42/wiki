package bolt

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"

	blt "go.etcd.io/bbolt"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/model"
)

func NewDBSubject(db *blt.DB) model.DBSubject {
	return &dbSubject{db: db}
}

type dbSubject struct {
	db *blt.DB
}

func (ds *dbSubject) Get(ctx context.Context, id int) (*model.Subject, error) {
	var ms model.Subject
	if err := ds.db.View(func(tx *blt.Tx) error {
		s := tx.Bucket(tblSubject).Get(itob(uint64(id)))
		if s == nil {
			return ErrNoSubject
		}
		return json.Unmarshal(s, &ms)
	}); err != nil {
		return nil, err
	}
	return &ms, nil
}

func (ds *dbSubject) Create(ctx context.Context,
	name string,
) (*model.Subject, error) {
	var ms model.Subject
	if err := ds.db.Update(func(tx *blt.Tx) error {
		sn := tx.Bucket(idxSubjectName)
		if sn.Get([]byte(name)) != nil {
			return ErrSubjectAlreadyExists
		}
		s := tx.Bucket(tblSubject)
		id, err := s.NextSequence()
		if err != nil {
			return err
		}
		ms.ID = int(id)
		ms.Name = name
		ms.Created = time.Now().UTC().Truncate(time.Microsecond)
		b, err := json.Marshal(ms)
		if err != nil {
			return err
		}
		subjectKey := itob(uint64(int(id)))
		if err := sn.Put([]byte(name), subjectKey); err != nil {
			return err
		}
		return s.Put(subjectKey, b)
	}); err != nil {
		return nil, err
	}
	return &ms, nil
}

func (ds *dbSubject) GetByName(ctx context.Context,
	name string,
) (*model.Subject, error) {
	var ms model.Subject
	if err := ds.db.View(func(tx *blt.Tx) error {
		sn := tx.Bucket(idxSubjectName)
		a := sn.Get([]byte(name))
		if a == nil {
			return errors.ErrNotFound{
				Resource: name,
			}
		}
		b := tx.Bucket(tblSubject).Get(a)
		if b == nil {
			return errors.ErrNotFound{
				Resource: name,
			}
		}
		return json.Unmarshal(b, &ms)
	}); err != nil {
		return nil, err
	}
	return &ms, nil
}

func (ds *dbSubject) List(ctx context.Context) ([]string, error) {
	sl := make([]string, 0)
	if err := ds.db.View(func(tx *blt.Tx) error {
		sn := tx.Bucket(idxSubjectName)
		return sn.ForEach(func(k, _ []byte) error {
			sl = append(sl, string(k))
			return nil
		})
	}); err != nil {
		return nil, err
	}
	return sl, nil
}

func (ds *dbSubject) Random(ctx context.Context) (*model.Subject, error) {
	var ms model.Subject
	if err := ds.db.View(func(tx *blt.Tx) error {
		sl := make([][]byte, 0)
		sn := tx.Bucket(idxSubjectName)
		if err := sn.ForEach(func(_, v []byte) error {
			sl = append(sl, v)
			return nil
		}); err != nil {
			return err
		}
		if len(sl) == 0 {
			return ErrNoSubject
		}
		n := rand.Intn(len(sl))
		s := tx.Bucket(tblSubject).Get(sl[n])
		if s == nil {
			return ErrNoSubject
		}
		return json.Unmarshal(s, &ms)
	}); err != nil {
		if err == ErrNoSubject {
			return &model.Subject{Name: ""}, nil
		}
		return nil, err
	}
	return &ms, nil
}
