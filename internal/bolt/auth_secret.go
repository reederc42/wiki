package bolt

import (
	"context"
	"encoding/json"
	"time"

	blt "go.etcd.io/bbolt"

	"gitlab.com/reederc42/wiki/internal/model"
)

func NewDBAuthSecret(db *blt.DB) model.DBSecret {
	return &dbAuthSecret{db: db}
}

type dbAuthSecret struct {
	db *blt.DB
}

func (das *dbAuthSecret) CreateIfNotExists(ctx context.Context,
	as *model.Secret,
) error {
	return das.db.Update(func(tx *blt.Tx) error {
		asbkt := tx.Bucket(tblAuthSecret)
		if asbkt.Get(itob(uint64(as.ID))) != nil {
			return nil
		}
		a, err := json.Marshal(*as)
		if err != nil {
			return err
		}
		return asbkt.Put(itob(uint64(as.ID)), a)
	})
}

func (das *dbAuthSecret) Create(ctx context.Context, secret string, retires,
	expires time.Time,
) error {
	return das.db.Update(func(tx *blt.Tx) error {
		asbkt := tx.Bucket(tblAuthSecret)
		id, err := asbkt.NextSequence()
		if err != nil {
			return err
		}
		as := model.Secret{
			ID:      int(id),
			Secret:  secret,
			Retires: retires,
			Expires: expires,
		}
		a, err := json.Marshal(as)
		if err != nil {
			return err
		}
		return asbkt.Put(itob(uint64(as.ID)), a)
	})
}

func (das *dbAuthSecret) List(ctx context.Context) ([]*model.Secret, error) {
	masl := make([]*model.Secret, 0)
	if err := das.db.View(func(tx *blt.Tx) error {
		return tx.Bucket(tblAuthSecret).ForEach(func(_, v []byte) error {
			var mas model.Secret
			if err := json.Unmarshal(v, &mas); err != nil {
				return err
			}
			masl = append([]*model.Secret{&mas}, masl...)
			return nil
		})
	}); err != nil {
		return nil, err
	}
	return masl, nil
}

func (das *dbAuthSecret) Delete(ctx context.Context, id int) error {
	return das.db.Update(func(tx *blt.Tx) error {
		return tx.Bucket(tblAuthSecret).Delete(itob(uint64(id)))
	})
}
