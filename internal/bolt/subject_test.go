//go:build all || bolt

package bolt_test

import (
	"testing"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/internal/model"
	model_test "gitlab.com/reederc42/wiki/internal/model/test"
)

func TestSubject(t *testing.T) {
	db, close := getTestDB(t)
	defer close()
	model_test.Subject(t, model.Model{
		Subject: bolt.NewDBSubject(db),
	})
}
