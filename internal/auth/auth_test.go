package auth

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetBearerAuth(t *testing.T) {
	tests := []struct {
		Name          string
		Request       *http.Request
		ExpectedToken string
		ExpectedOk    bool
	}{
		{
			Name: "has header with token",
			Request: &http.Request{
				Header: http.Header{
					"Authorization": []string{"Bearer my-token"},
				},
			},
			ExpectedToken: "my-token",
			ExpectedOk:    true,
		},
		{
			Name:       "no header",
			Request:    &http.Request{},
			ExpectedOk: false,
		},
		{
			Name: "bad header",
			Request: &http.Request{
				Header: http.Header{
					"Authorization": []string{"Basic user:pass"},
				},
			},
			ExpectedOk: false,
		},
	}
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			token, ok := getBearerAuth(tests[i].Request)
			assert.Equal(t, tests[i].ExpectedToken, token)
			assert.Equal(t, tests[i].ExpectedOk, ok)
		})
	}
}
