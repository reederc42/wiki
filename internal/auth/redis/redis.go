package redis

import (
	"bytes"
	"context"
	"encoding/json"
	"net"
	"time"

	"github.com/mediocregopher/radix/v3"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/pkg/jwt"
)

type SecretStoreConfig struct {
	// Network ("tcp", "unix"), default "tcp"
	Network string `yaml:"network"`
	// Host single-node Redis hostname, default 127.0.0.1
	Host string `yaml:"host"`
	// Port Redis port, default 6379
	Port string `yaml:"port"`
	// PoolSize number of connections, default 10
	PoolSize int `yaml:"poolSize"`
	// RetryLimit number of attempts to connect, default 10
	RetryLimit int `yaml:"retryLimit"`
	// RetryTime time between attempts to connect, default 1s
	RetryTime time.Duration `yaml:"retryTime"`
}

var DefaultSecretStoreConfig = SecretStoreConfig{
	Network:    "tcp",
	Host:       "localhost",
	Port:       "6379",
	PoolSize:   10,
	RetryLimit: 10,
	RetryTime:  5 * time.Second,
}

type secretStore struct {
	pool *radix.Pool
	list string
}

type storedSecret struct {
	SecretString string
	Expires      time.Time
	Retires      time.Time
}

func (ss *secretStore) Latest(ctx context.Context) (auth.Secret, error) {
	var r string
	if err := ss.pool.Do(radix.Cmd(&r, "LINDEX", ss.list,
		"-1")); err != nil {
		return auth.Secret{}, err
	}
	if r == "" {
		return auth.Secret{}, auth.ErrNoValidSecret{}
	}
	return decodeSecret(r)
}

func (ss *secretStore) All(ctx context.Context) ([]auth.Secret, error) {
	var r []string
	if err := ss.pool.Do(radix.Cmd(&r, "LRANGE", ss.list, "0",
		"-1")); err != nil {
		return nil, err
	}
	if len(r) == 0 {
		return nil, nil
	}
	s := make([]auth.Secret, len(r))
	var err error
	for i := range r {
		s[i], err = decodeSecret(r[i])
		if err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (ss *secretStore) Create(ctx context.Context, s auth.Secret) error {
	a, err := encodeString(s)
	if err != nil {
		return err
	}
	return ss.pool.Do(radix.Cmd(nil, "RPUSH", ss.list, a))
}

func (ss *secretStore) Delete(ctx context.Context, s auth.Secret) error {
	a, err := encodeString(s)
	if err != nil {
		return err
	}
	return ss.pool.Do(radix.Cmd(nil, "LREM", ss.list, "1", a))
}

func encodeString(s auth.Secret) (string, error) {
	secretString, err := s.Secret.String()
	if err != nil {
		return "", err
	}
	ss := &storedSecret{
		SecretString: secretString,
		Expires:      s.Expires,
		Retires:      s.Retires,
	}
	var b bytes.Buffer
	err = json.NewEncoder(&b).Encode(ss)
	return b.String(), err
}

func decodeSecret(s string) (auth.Secret, error) {
	var ss storedSecret
	err := json.NewDecoder(bytes.NewBufferString(s)).Decode(&ss)
	if err != nil {
		return auth.Secret{}, err
	}
	sec, err := jwt.FromString(ss.SecretString)
	if err != nil {
		return auth.Secret{}, err
	}
	return auth.Secret{
		Secret:  sec,
		Expires: ss.Expires,
		Retires: ss.Retires,
	}, nil
}

func NewSecretStore(config SecretStoreConfig,
	list string,
) (auth.SecretStore, error) {
	pool, err := createPool(config)
	if err != nil {
		return nil, err
	}
	return &secretStore{
		pool: pool,
		list: list,
	}, nil
}

func createPool(config SecretStoreConfig) (*radix.Pool, error) {
	var (
		err error
		p   *radix.Pool
	)
	for i := 0; i == 0 || i < config.RetryLimit && err != nil; i++ {
		p, err = radix.NewPool(config.Network, net.JoinHostPort(config.Host,
			config.Port), config.PoolSize)
		time.Sleep(config.RetryTime)
	}
	return p, err
}
