package auth

import (
	"context"
	"net/http"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/gorilla/mux"

	"gitlab.com/reederc42/wiki/internal/errors"
)

func decodeSigninRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	if u, p, ok := r.BasicAuth(); ok {
		return signinInput{
			Username: u,
			Password: p,
		}, nil
	}
	if t, ok := getBearerAuth(r); ok {
		return signinInput{
			Token:    t,
			UseToken: true,
		}, nil
	}
	return nil, errors.ErrUnauthorized{}
}

func makeSigninEndpoint(s *Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		i := request.(signinInput)
		return s.signin(ctx, i)
	}
}

func decodeSignupRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	if u, p, ok := r.BasicAuth(); ok {
		return signupInput{
			Username: u,
			Password: p,
		}, nil
	}
	return nil, errors.ErrUnauthorized{}
}

func makeSignupEndpoint(s *Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		i := request.(signupInput)
		return s.signup(ctx, i)
	}
}

func decodeAuthorizeRequest(_ context.Context,
	r *http.Request,
) (interface{}, error) {
	if t, ok := getBearerAuth(r); ok {
		method := mux.Vars(r)["method"]
		pathPrefix := "/auth/v1/" + method
		path := strings.TrimPrefix(r.URL.Path, pathPrefix)
		return authorizeInput{
			RefreshToken: t,
			Method:       method,
			Path:         path,
		}, nil
	}
	return nil, errors.ErrUnauthorized{}
}

func makeAuthorizeEndpoint(s *Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		i := request.(authorizeInput)
		return s.authorize(ctx, i)
	}
}
