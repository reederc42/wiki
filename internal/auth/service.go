package auth

import (
	"context"
	"net/http"
	"regexp"
	"time"

	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/model"
	"gitlab.com/reederc42/wiki/pkg/jwt"
	"gitlab.com/reederc42/wiki/pkg/trace"
)

type Service struct {
	Model                model.Model
	ServiceSecrets       SecretStore
	AuthorizationSecrets SecretStore
	Config               Config
}

type signinInput struct {
	Username string
	Password string
	Token    string
	UseToken bool
}

// signin uses the authorization header to return a refresh token
func (s *Service) signin(ctx context.Context, i signinInput) (string, error) {
	l := trace.L(ctx).With(zap.String("function", "signin"))
	var id int
	if i.UseToken {
		c, err := s.decodeRefresh(ctx, i.Token)
		if err != nil {
			l.Error("bearer signin error", zap.Error(err))
			return "", errors.ErrUnauthorized{}
		}
		id = c.UserID
	} else {
		u, err := s.Model.User.GetByNameAndPassword(ctx, i.Username, i.Password)
		if err != nil {
			l.Error("basic signin error", zap.Error(err))
			return "", errors.ErrUnauthorized{}
		}
		id = u.ID
	}
	t, err := s.encodeRefresh(ctx, id)
	if err != nil {
		l.Error("error creating token", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	return t, nil
}

type signupInput struct {
	Username string
	Password string
}

// signup uses basic authorization to create a new user, if user doesn't exist
func (s *Service) signup(ctx context.Context, i signupInput) (string, error) {
	l := trace.L(ctx).With(zap.String("function", "signup"))
	u, err := s.Model.User.Create(ctx, i.Username, i.Password)
	if err != nil {
		l.Error("signup error", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	t, err := s.encodeRefresh(ctx, u.ID)
	if err != nil {
		l.Error("error creating token", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	return t, nil
}

type authorizeInput struct {
	RefreshToken string
	Method       string
	Path         string
}

// authorize uses refresh token to create service token for request
func (s *Service) authorize(ctx context.Context,
	i authorizeInput,
) (string, error) {
	l := trace.L(ctx).With(zap.String("function", "authorize"))
	// ensure refresh token is valid
	rc, err := s.decodeRefresh(ctx, i.RefreshToken)
	if err != nil {
		l.Error("authorize error", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	// check if user exists
	u, err := s.Model.User.Get(ctx, rc.UserID)
	if err != nil || u == nil {
		if err == nil {
			err = ErrUserDoesNotExist{rc.UserID}
		}
		l.Error("authorize error", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	// user exists, so they have access to resource
	l.Info("encoding service token", zap.String("method", i.Method),
		zap.String("path", i.Path))
	t, err := s.encodeService(ctx, rc.UserID, i.Method, i.Path)
	if err != nil {
		l.Error("error creating token", zap.Error(err))
		return "", errors.ErrUnauthorized{}
	}
	return t, nil
}

type serviceClaims struct {
	UserID int    `json:"user"`
	Method string `json:"method"`
	Path   string `json:"path"`
}

// encodeService encodes userID, method and path to JWT; may create/delete
// database secrets
func (s *Service) encodeService(ctx context.Context, userID int, method,
	path string,
) (string, error) {
	sec, err := s.ServiceSecrets.Latest(ctx)
	if err != nil {
		return "", err
	}
	if sec.Retired(time.Now()) {
		return "", ErrNoValidSecret{}
	}
	return jwt.Encode(
		serviceClaims{
			UserID: userID,
			Method: method,
			Path:   path,
		},
		time.Now().Add(s.Config.Service.TokenLifespan),
		sec.Secret,
	)
}

// decodeService attempts to decode and validate a JWT by all valid secrets
// (retired, not expired)
func (s *Service) decodeService(ctx context.Context,
	token string,
) (*serviceClaims, error) {
	secrets, err := s.ServiceSecrets.All(ctx)
	if err != nil {
		return nil, err
	}
	err = jwt.ErrCryptoFailure
	var c serviceClaims
	for i := 0; i < len(secrets) && err == jwt.ErrCryptoFailure; i++ {
		if !secrets[i].Expired(time.Now().Add(s.Config.Wiggle)) {
			err = jwt.Decode(&c, token, secrets[i].Secret, s.Config.Wiggle)
		}
	}
	return &c, err
}

type refreshClaims struct {
	UserID int `json:"user"`
}

// encodeRefresh encodes userID to JWT; may create/delete database secrets
func (s *Service) encodeRefresh(ctx context.Context,
	userID int,
) (string, error) {
	sec, err := s.AuthorizationSecrets.Latest(ctx)
	if err != nil {
		return "", err
	}
	if sec.Retired(time.Now()) {
		return "", ErrNoValidSecret{}
	}
	return jwt.Encode(
		refreshClaims{
			UserID: userID,
		},
		time.Now().Add(s.Config.Authorization.TokenLifespan),
		sec.Secret,
	)
}

// decodeRefresh attempts to decode and validate a JWT by all valid secrets
// (retired, not expired)
func (s *Service) decodeRefresh(ctx context.Context,
	token string,
) (*refreshClaims, error) {
	secrets, err := s.AuthorizationSecrets.All(ctx)
	if err != nil {
		return nil, err
	}
	err = jwt.ErrCryptoFailure
	var c refreshClaims
	for i := 0; i < len(secrets) && err == jwt.ErrCryptoFailure; i++ {
		if !secrets[i].Expired(time.Now().Add(s.Config.Wiggle)) {
			err = jwt.Decode(&c, token, secrets[i].Secret, s.Config.Wiggle)
		}
	}
	return &c, err
}

var bearerRE *regexp.Regexp = regexp.MustCompile(`Bearer (.*)`)

func getBearerAuth(r *http.Request) (string, bool) {
	h := r.Header.Get("Authorization")
	if m := bearerRE.FindStringSubmatch(h); len(m) == 2 {
		return m[1], true
	}
	return "", false
}
