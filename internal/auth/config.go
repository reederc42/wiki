package auth

import "time"

type Config struct {
	// Service service secret config
	Service SecretConfig `yaml:"service"`
	// Authorization authorization secret config
	Authorization SecretConfig `yam:"authorization"`
	// Wiggle times are tested against +/- wiggle
	Wiggle time.Duration `yaml:"wiggle"`
	// ErrorRequeue wait time until reconciler is rerun after error
	ErrorRequeue time.Duration `yaml:"errorRequeue"`
}

type SecretConfig struct {
	// client JWT lifespan
	TokenLifespan time.Duration `yaml:"tokenLifespan"`
	// server JWT secret lifespan
	SecretLifespan time.Duration `yaml:"secretLifespan"`
}

var DefaultConfig = Config{
	Service: SecretConfig{
		TokenLifespan:  10 * time.Second,
		SecretLifespan: 1 * time.Hour,
	},
	Authorization: SecretConfig{
		TokenLifespan:  1 * time.Hour,
		SecretLifespan: 6 * time.Hour,
	},
	Wiggle:       200 * time.Millisecond,
	ErrorRequeue: 5 * time.Second,
}
