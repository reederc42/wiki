// Package auth provides routes for authenticating requests, and tools for other
// packages to use authenticated requests. Using authenticated requests means
// adding a token object to the context
//
// A token object contains all the information necessary to authorize a request
// (a user id that can be used with the database)
package auth

import (
	"context"
	"net/http"
	"time"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/wiki"
	"gitlab.com/reederc42/wiki/pkg/jwt"
)

func Handle(r *mux.Router, s *Service, l *zap.Logger) *mux.Router {
	r.Path("/auth/v1/signin").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeSigninEndpoint(s),
			decodeSigninRequest,
			wiki.EncodePlainResponse,
			kithttp.ServerErrorEncoder(wiki.EncodeError),
			kithttp.ServerBefore(wiki.ServerBeforeAddTrace(l, "auth")),
			kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
		))
	r.Path("/auth/v1/signup").
		Methods(http.MethodPost).
		Handler(kithttp.NewServer(
			makeSignupEndpoint(s),
			decodeSignupRequest,
			wiki.EncodePlainResponse,
			kithttp.ServerErrorEncoder(wiki.EncodeError),
			kithttp.ServerBefore(wiki.ServerBeforeAddTrace(l, "auth")),
			kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
		))
	r.PathPrefix("/auth/v1/{method}").
		Methods(http.MethodGet).
		Handler(kithttp.NewServer(
			makeAuthorizeEndpoint(s),
			decodeAuthorizeRequest,
			wiki.EncodePlainResponse,
			kithttp.ServerErrorEncoder(wiki.EncodeError),
			kithttp.ServerBefore(wiki.ServerBeforeAddTrace(l, "auth")),
			kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
		))
	return r
}

type authContextKey struct{}

type authContextValue struct {
	Token  string
	Method string
	Path   string
}

// ServerBefore is used on routes that need to be authenticated; adds bearer
// token to context
func ServerBefore(ctx context.Context, r *http.Request) context.Context {
	if t, ok := getBearerAuth(r); ok {
		return context.WithValue(ctx, authContextKey{}, authContextValue{
			Token:  t,
			Method: r.Method,
			Path:   r.URL.Path,
		})
	}
	return ctx
}

type AuthorizeServiceFunc func(context.Context) (int, error)

// MakeAuthorizeServiceFunc returns a function that will get the user id from a
// context
func MakeAuthorizeServiceFunc(s *Service) AuthorizeServiceFunc {
	return func(ctx context.Context) (int, error) {
		v, ok := ctx.Value(authContextKey{}).(authContextValue)
		if !ok {
			return 0, ErrNoToken{}
		}
		c, err := s.decodeService(ctx, v.Token)
		if err != nil {
			return 0, err
		}
		if c.Method != v.Method || c.Path != v.Path {
			return 0, errors.ErrUnauthorized{}
		}
		return c.UserID, nil
	}
}

type SecretStore interface {
	// Latest gets the most-recently created secret, should be used to encode new
	// JWTs. User should use Retired() to ensure secret is usable.
	Latest(context.Context) (Secret, error)
	// All returns all secrets, should be used to decode JWTs. Returned secrets
	// may be expired; use Expired() to check if a secret is usable.
	All(context.Context) ([]Secret, error)
	// Create creates a new secret
	Create(context.Context, Secret) error
	// Delete deletes a secret; should be used by a secrets manager if a secret
	// is expired.
	Delete(context.Context, Secret) error
}

type Secret struct {
	// Secret the value of the secret
	Secret *jwt.Secret
	// Retires when the secret is retired, and should not be used to encrypt
	// tokens
	Retires time.Time
	// Expires when the secret is expired, and should not be used to decrypt
	// tokens
	Expires time.Time
	// Key holds implementation-specific details for deleting a secret
	Key interface{}
}

// Retired returns if secret is retired as of t
func (s *Secret) Retired(t time.Time) bool {
	return t.After(s.Retires)
}

// Expired returns if secret is expired as of t
func (s *Secret) Expired(t time.Time) bool {
	return t.After(s.Expires)
}

// Initialize ensures there is at least 1 secret that will be active for the
// secret lifespan (+/- wiggle)
func Initialize(ctx context.Context, ss SecretStore, cfg SecretConfig,
	wiggle time.Duration, logger *zap.Logger,
) error {
	logger.Info("initializing secrets")
	// get all secrets
	secrets, err := ss.All(ctx)
	if err != nil {
		return err
	}
	// count active secrets
	var activeSecrets int
	// time secret should expire
	expires := time.Now().Add(cfg.SecretLifespan)
	// time secret should retire
	retires := expires.Add(-cfg.TokenLifespan)
	for _, s := range secrets {
		if !s.Expired(expires.Add(wiggle)) &&
			!s.Retired(retires.Add(wiggle)) {
			activeSecrets++
		}
	}
	// create new secret if no active secrets within error requeue time
	if activeSecrets == 0 {
		logger.Info("no secrets found, creating new secret")
		sec, err := jwt.NewSecret()
		if err != nil {
			return err
		}
		err = ss.Create(ctx, Secret{
			Secret:  sec,
			Expires: expires,
			Retires: retires,
		})
		if err != nil {
			return err
		}
	}
	logger.Info("secrets initialized")
	return nil
}

// Reconcile returns requeue time and fatal errors
func Reconcile(ctx context.Context, ss SecretStore, logger *zap.Logger,
	cfg SecretConfig, wiggle time.Duration,
	errorRequeue time.Duration,
) (time.Duration, error) {
	logger.Info("reconciling secrets")
	// get all secrets
	secrets, err := ss.All(ctx)
	if err != nil {
		// error retrieving secrets, try again in errorRequeue time
		logger.Error("could not get secrets", zap.Error(err))
		return errorRequeue, nil
	}
	numRetired := 0
	var minRetires time.Time
	for i := range secrets {
		// if secret will retire within wiggle, count it as retired, so a new
		// secret may be created before we re-reconcile
		if secrets[i].Retired(time.Now().Add(wiggle)) {
			numRetired++
		} else {
			// find minimum retirement, to requeue at that time (minus wiggle)
			if minRetires.IsZero() || minRetires.After(secrets[i].Retires) {
				minRetires = secrets[i].Retires
			}
		}
		// if secret is expired, delete it
		if secrets[i].Expired(time.Now().UTC()) {
			if err := ss.Delete(ctx, secrets[i]); err != nil {
				// error deleting this secret is only recorded
				logger.Error("could not delete secret", zap.Error(err))
			}
		}
	}
	// if all secrets are expired, or there are no secrets, force create a new
	// secret
	if numRetired == len(secrets) {
		expires := time.Now().Add(cfg.SecretLifespan)
		retires := expires.Add(-cfg.TokenLifespan)
		sec, err := jwt.NewSecret()
		if err != nil {
			logger.Error("could not get new secret", zap.Error(err))
			return errorRequeue, nil
		}
		if err := ss.Create(ctx, Secret{
			Secret:  sec,
			Retires: retires,
			Expires: expires,
		}); err != nil {
			// error creating a new secret, try again in 5 seconds
			logger.Error("could not create secret", zap.Error(err))
			return errorRequeue, nil
		}
		if minRetires.IsZero() || minRetires.After(retires) {
			minRetires = retires
		}
	}
	// secrets successfully reconciled; requeue at minRetires - wiggle
	// minRetires should not be zero, but if it is we will reconcile immediately
	t := minRetires.Add(-wiggle)
	logger.Info("secrets reconciled successfully", zap.Time("requeueAt", t))
	return time.Until(t), nil
}
