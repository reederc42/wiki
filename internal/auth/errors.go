package auth

import "fmt"

type ErrInvalidToken struct{}

func (it ErrInvalidToken) Error() string {
	return "invalid token"
}

type ErrSigningMethodMismatch struct{}

func (smm ErrSigningMethodMismatch) Error() string {
	return "signing method mismatch"
}

type ErrNoToken struct{}

func (nt ErrNoToken) Error() string {
	return "no token in context"
}

type ErrNoValidSecret struct{}

func (nvs ErrNoValidSecret) Error() string {
	return "no valid secret"
}

type ErrTokenExpired struct{}

func (te ErrTokenExpired) Error() string {
	return "token expired"
}

type ErrUserDoesNotExist struct {
	UserID int
}

func (udne ErrUserDoesNotExist) Error() string {
	return fmt.Sprintf("user does not exist: %d", udne.UserID)
}
