// Package model implements the auth backend for any model interface
package model

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/internal/model"
	"gitlab.com/reederc42/wiki/pkg/jwt"
)

type secretStore struct {
	db             model.DBSecret
	secretLifespan time.Duration
	tokenLifespan  time.Duration
	wiggle         time.Duration
}

// Latest returns the most-recently created secret
func (ss *secretStore) Latest(ctx context.Context) (auth.Secret, error) {
	var s auth.Secret
	// get all secrets
	secrets, err := ss.db.List(ctx)
	if err != nil {
		return auth.Secret{}, err
	}
	// find first valid secret, or no valid secrets
	maxID := 0
	si := 0
	for ; si < len(secrets) &&
		time.Now().After(secrets[si].Retires.Add(ss.wiggle)); si++ {
		if secrets[si].ID > maxID {
			maxID = secrets[si].ID
		}
	}
	if si < len(secrets) {
		// found valid secret
		sec, err := jwt.FromString(secrets[si].Secret)
		if err != nil {
			return auth.Secret{}, err
		}
		s.Secret = sec
		s.Retires = secrets[si].Retires
		s.Expires = secrets[si].Expires
		s.Key = secrets[si].ID
	}
	if s.Secret == nil {
		return auth.Secret{}, auth.ErrNoValidSecret{}
	}
	return s, nil
}

// All returns all secrets
func (ss *secretStore) All(ctx context.Context) ([]auth.Secret, error) {
	ds, err := ss.db.List(ctx)
	if err != nil {
		return nil, err
	}
	if len(ds) == 0 {
		return nil, nil
	}
	s := make([]auth.Secret, 0, len(ds))
	for i := range ds {
		sec, err := jwt.FromString(ds[i].Secret)
		if err != nil {
			return nil, err
		}
		s = append(s, auth.Secret{
			Secret:  sec,
			Retires: ds[i].Retires,
			Expires: ds[i].Expires,
			Key:     ds[i].ID,
		})
	}
	return s, nil
}

// Create inserts a new secret
func (ss *secretStore) Create(ctx context.Context, s auth.Secret) error {
	sec, err := s.Secret.String()
	if err != nil {
		return err
	}
	return ss.db.Create(ctx, sec, s.Retires, s.Expires)
}

// Delete deletes the secret with id = Key
func (ss *secretStore) Delete(ctx context.Context, s auth.Secret) error {
	id, ok := s.Key.(int)
	if !ok {
		return fmt.Errorf("could not delete secret: key is not int")
	}
	return ss.db.Delete(ctx, id)
}

func NewSecretStore(dbSecret model.DBSecret, secretLifespan, tokenLifespan,
	wiggle time.Duration,
) auth.SecretStore {
	return &secretStore{
		db:             dbSecret,
		secretLifespan: secretLifespan,
		tokenLifespan:  tokenLifespan,
		wiggle:         wiggle,
	}
}
