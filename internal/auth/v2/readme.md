# Auth V2

Goals:
1. Replace identity token with per-request token

1. Remove userpass from disk, by replacing with refresh token

The purpose is for a service is to be able to idependently verify and trust a request. It is not perfectly independent, because there must be a secret shared between the service and authorizer with which to encrypt and sign the token. If admins want to revoke unexpired tokens, there must also be some revocation list (or allowlist), shared between the authorizer and service.

## Client Workflow

1. Client wants to `$method` a resource at `$url`

    * Client already has identity token from authorizer

1. Client requests access to `$url` by `$method` from authorizer

    1. Client makes GET request to authorizer, without query parameters:

        ```
        Authorization: Bearer <identity token>
        GET /auth/v2/$method/$url
        ```

    1. Authorizer returns signed, encrypted JWT (service token) with payload:

        ```json
        {
            "user": 1234,
            "method": "$method",
            "url": "$url"
        }
        ```

1. Client uses token with service, service attempts to verify token

    ```
    Authorization: Bearer <service token>
    $method $url
    ```

    1. Service uses shared secret to attempt to decrypt token

    1. If successful, service checks token signature

    1. If signature if verified, service checks if signature has been revoked
    
        * Either not in revocation list
        
        * Or in allowlist

        * Signature is used as unique token identifier, but if this proves onerous will be replaced with ULID or UUID stored in token

    1. If signature has not been revoked, service allows access to resource (service does the request)

## Token Secret Store/Service

The Token Secret Store (or Service), or TSS, is used by both the authorizer and service to provide shared secrets the authorizer and service will use to encrypt, decrypt, and sign JWTs.

The TSS should be secured and only accessible by trusted services, such as api services and authorizers. TSS itself will provide no authorization mechanisms.

### API

TSS provides one api: SecretsForService

SecretsForService returns all valid secrets for a service. TSS may be implemented as a client-only service, such as for the synchronous omnibus version of wiki, or as a webservice such as for the k8s version of wiki.

Example HTTP request and response, for webservice implementation of TSS, to get secrets for service "api":

* Request:

    ```
    GET /tss/v1/secrets/api
    ```

* Response:

    ```json
    [
        {
            "SecretBytes": "<base64 secret bytes>",
            "Expires": 123456,
            "Retires": 12300
        },
        {
            "SecretBytes": "<base64 secret bytes>",
            "Expires": 123000,
            "Retires": 122000
        }
    ]
    ```

TSS will always provide at least one valid secret with this request. It is up to TSS to determine validity and manage secret generation.

Services may cache secrets until their expiration; right now there isn't planned to be a way to revoke them. Services shouldn't permanently persist secrets, so that if a secret is compromised the admin can restart services to invalidate the secret.

TSS is also responsible for providing a reasonable number of secrets and a reasonable use of disk or memory. This means TSS should implement some reaper for old secrets.

## Implementation Notes

### Synchronous

The synchronous implementation is for the omnibus or on-prem version of k8s, or more specifically the boltdb implementation of the secret store and auth. Because boltdb is synchronous, there is no need for a separate service to provide secrets: TSS can be a client-only library, where if the secret bucket is empty, another secret can be created.

The reaper will also be implemented in the client library, where any expired secrets will be reaped.

Currently omnibus wiki can use postgres as the auth backend, TBD if it can/should be used for the non-webservice version of TSS.

### Asynchronous

The asynchronous implementation is almost exclusively for redis, although postgres could also be used. The asynchronous implementation will provide a webservice as described above. The webservice could use a separate thread for generating and reaping secrets; an individual request will not generate a secret, and instead wait until there is a valid secret.

### RW Lock

The refresh, identity, and api tokens will all be encrypted with different secrets. The api token secrets do not need to be persisted. In the synchronous case, a simple in-memory list may be kept. This is also true for the local async cache. TSS will need to be pre-configured with the set of services, and if they are to be persisted. And locally the secrets will be in-memory and accessed via a reader-writer lock.

### JWT

claims:
user: int, user id
method: string, http method "GET", "POST", etc
path: string, path of request

### Client

Clients of the auth service are endpoints, where the auth layer adds the user to the context.

internal/auth defines two parts: the auth api, and the client layer. The client layer uses the secret store to decode and store (in the request context) service-level claims, which right now is only the userID. The auth api authenticates requests: using basic auth it will return a refresh token, and using the refresh token will return a service token, which is then taken to the client layer.

V2 will have a similar secret store, but it will manage multiple sources of secrets, as well as use a local cache with a RWLock.

### Request Flow

Flow inside service

1. Service receives request that needs authorization

    * if bearer token is not present, returns unauthorized

1. Service gets token secrets

    1. auth client lib uses RWLock to get valid secrets; if the latest secret is "close" to expiring, attempts to acquire writer lock

        1. With writer lock, re-reads secrets; if the latest is still close to expiring, gets a new secret

            * on-prem creates a new secret directly; web service makes request to secret service

1. Service attempts to decode token

    * Possibly instead, just calls Validate(token, method, path) on the auth client lib

    * The issue is the auth layer needs extra claims "mixed in" with the data the service needs; can we include arbitrary depth? that would allow a "service payload" within the claims

    * the auth lib can return a map!

        ```Go
        // ValidateAndDecode returns any claims not consumed by the validator, or error. The returned error will always be unauthorized or nil, with no reason.
        func ValidateAndDecode(token, method, path string) (map[string]interface{}, error)
        ```

    * It probably shouldn't return a map; json decodes numbers into float64 instead of int