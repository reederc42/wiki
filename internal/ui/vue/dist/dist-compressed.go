//go:build compressdist && !omnibusdist

//go:generate go run ../../bundle-ui -o zz-generated.dist-compressed.go -d ../../../../_vue/dist/ -p dist -s ../../content-types.yaml -z -t "compressdist && !omnibusdist && !nodist"

package dist

const DistCompressed = true
