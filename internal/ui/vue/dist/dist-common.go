// Package dist contains generators and generated source files for compiled vue
//
//	projects
package dist

import "gitlab.com/reederc42/wiki/internal/ui"

var Files map[string]ui.File
