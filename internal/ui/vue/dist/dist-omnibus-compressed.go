//go:build compressdist && omnibusdist

//go:generate go run ../../bundle-ui -o zz-generated.dist-omnibus-compressed.go -d ../../../../_vue/dist-omnibus/ -p dist -s ../../content-types.yaml -z -t "compressdist && omnibusdist && !nodist"

package dist

const DistCompressed = true
