//go:build !compressdist && omnibusdist

//go:generate go run ../../bundle-ui -o zz-generated.dist-omnibus.go -d ../../../../_vue/dist-omnibus/ -p dist -s ../../content-types.yaml -t "!compressdist && omnibusdist && !nodist"

package dist

const DistCompressed = false
