package vue

import "time"

type Config struct {
	// node environment ("development", "production") (default "production")
	NodeEnv string `yaml:"nodeEnv"`
	// dist name
	Dist string `yaml:"dist"`
	// vue app api client ("mock", "server")
	APIClient string `yaml:"apiClient"`
	// vue app api client host, not valid if api client is "mock"
	APIClientHost string `yaml:"apiClientHost"`
	// vue app api client path, not valid if api client is "mock"
	APIClientPath string `yaml:"apiClientPath"`
	// subject list refresh period
	APIListRefresh time.Duration `yaml:"apiListRefresh"`
	// vue app auth client ("mock", "server")
	AuthClient string `yaml:"authClient"`
	// vue app auth client host, not valid if auth client is "mock"
	AuthClientHost string `yaml:"authClientHost"`
	// vue app auth client path, not valid if auth client is "mock"
	AuthClientPath string `yaml:"authClientPath"`
	// if true, fonts are bundled into app
	ServeFonts bool `yaml:"serveFonts"`
}

var DefaultConfig = Config{
	NodeEnv:        "production",
	Dist:           "",
	APIClient:      "server",
	APIClientHost:  "",
	APIClientPath:  "/api/v1/",
	APIListRefresh: 5 * time.Minute,
	AuthClient:     "server",
	AuthClientHost: "",
	AuthClientPath: "/auth/v1/",
	ServeFonts:     false,
}
