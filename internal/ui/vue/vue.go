// Package vue implements a gorilla/go-kit in-memory file server, with history
// mode fallback
package vue

import (
	"bytes"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/errors"
	"gitlab.com/reederc42/wiki/internal/ui"
	"gitlab.com/reederc42/wiki/internal/ui/vue/dist"
	"gitlab.com/reederc42/wiki/internal/wiki"
)

func Handle(r *mux.Router, l *zap.Logger) (*mux.Router, error) {
	f, err := unpackFiles(dist.Files, l, dist.DistCompressed)
	if err != nil {
		return nil, err
	}
	s := &service{
		Files: f,
	}
	getHandler := kithttp.NewServer(
		makeGetEndpoint(s),
		decodeGetRequest,
		encodeGetResponse,
		kithttp.ServerErrorEncoder(wiki.EncodeError),
		kithttp.ServerBefore(wiki.ServerBeforeAddTrace(l, "assets")),
		kithttp.ServerFinalizer(wiki.ServerFinalizerEndTrace),
	)
	r.PathPrefix("/").
		Methods(http.MethodGet).
		Handler(getHandler)
	return r, nil
}

func unpackFiles(files map[string]ui.File,
	logger *zap.Logger, compressed bool,
) (map[string]ui.File, error) {
	m := make(map[string]ui.File)
	for k, v := range files {
		l := logger.With(zap.String("component", "assets"),
			zap.String("file", k), zap.Bool("compressed", compressed))
		c := v.Contents
		if compressed {
			z, err := decompressFile(c)
			if err != nil {
				return nil, fmt.Errorf("%v file: %s", err, k)
			}
			c = z
		}
		m[k] = ui.File{
			ContentType: v.ContentType,
			Contents:    c,
		}
		l.Info("unpacked file")
	}
	return m, nil
}

func decompressFile(contents []byte) ([]byte, error) {
	ib := bytes.NewBuffer(contents)
	zr, err := gzip.NewReader(ib)
	if err != nil {
		return nil, err
	}
	defer zr.Close()
	return io.ReadAll(zr)
}

type Service interface {
	Get(context.Context, string) (*ui.File, error)
}

type getRequest struct {
	Resource string
}

type getResponse struct {
	File *ui.File
}

func decodeGetRequest(_ context.Context, r *http.Request) (interface{}, error) {
	if r.URL.Path == "/" || strings.HasPrefix(r.URL.Path, "/wiki") {
		r.URL.Path = "/index.html"
	}
	return getRequest{
		Resource: r.URL.Path,
	}, nil
}

func encodeGetResponse(_ context.Context, w http.ResponseWriter,
	response interface{},
) error {
	res := response.(getResponse)
	w.Header().Add("Content-Type", res.File.ContentType)
	_, _ = w.Write(res.File.Contents)
	return nil
}

func makeGetEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getRequest)
		f, err := s.Get(ctx, req.Resource)
		if err != nil {
			return nil, err
		}
		return getResponse{
			File: f,
		}, nil
	}
}

type service struct {
	Files map[string]ui.File
}

func (s *service) Get(_ context.Context, resource string) (*ui.File, error) {
	if f, ok := s.Files[resource]; ok {
		return &f, nil
	}
	return nil, errors.ErrNotFound{
		Resource: resource,
	}
}
