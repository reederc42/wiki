package main

import (
	"flag"
	"io"
	"os"
	"strings"

	"gopkg.in/yaml.v3"
)

/*
1. get content types
2. walk provided directory
	2a. for each directory, append dir name
	2b. for each file, add file contents and type to map

Example:
File structure:
dist/
-index.html

Go structure:
File["index.html"]{
	ContentType: "plain/html",
	Content: []bytes("<html></html>"),
}
*/

func main() {
	var (
		output     string
		source     string
		directory  string
		pkg        string
		varName    string
		buildTags  string
		cacheTypes bool
		compress   bool
	)
	flag.StringVar(&output, "o", os.Stdout.Name(), "output file")
	flag.StringVar(&source, "s",
		"https://raw.githubusercontent.com/nginx/nginx/master/conf/mime.types",
		"external source of MIME types; if source starts with 'http://' or "+
			"'https://', it must be an NGINX mime.types file; otherwise, it "+
			"must be a YAML/JSON file.")
	flag.StringVar(&directory, "d", ".", "input directory")
	flag.StringVar(&pkg, "p", "main", "output package")
	flag.StringVar(&varName, "v", "Files", "package-level var name")
	flag.StringVar(&buildTags, "t", "", "build tags for output file")
	flag.BoolVar(&cacheTypes, "c", false, "output is MIME types as YAML map, "+
		"from source 's'; ignores other options")
	flag.BoolVar(&compress, "z", false, "gzip compress files before storing")
	flag.Parse()

	var (
		o   io.Writer
		f   *os.File
		err error
	)
	if output == os.Stdout.Name() {
		o = os.Stdout
	} else {
		f, err = os.Create(output)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		o = f
	}
	var contentTypes map[string]string
	if strings.HasPrefix(source, "https://") ||
		strings.HasPrefix(source, "http://") {
		contentTypes, err = getContentTypesFromHTTP(source)
	} else {
		contentTypes, err = getContentTypesFromFile(source)
	}
	if err != nil {
		if f != nil {
			f.Close()
			os.Remove(f.Name())
		}
		panic(err)
	}
	if cacheTypes {
		err = yaml.NewEncoder(o).Encode(contentTypes)
	} else {
		err = compile(o, contentTypes, directory, pkg, varName, buildTags,
			compress)
	}
	if err != nil {
		if f != nil {
			f.Close()
			os.Remove(f.Name())
		}
		panic(err)
	}
}
