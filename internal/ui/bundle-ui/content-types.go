package main

import (
	"io"
	"net/http"
	"os"
	"regexp"
	"strings"

	"gopkg.in/yaml.v3"
)

// getContentTypesFromFile returns a map of file extensions to MIME types, taken
// from file. File is expected to be YAML
func getContentTypesFromFile(file string) (map[string]string, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	m := make(map[string]string)
	if err := yaml.NewDecoder(f).Decode(m); err != nil {
		return nil, err
	}
	return m, nil
}

// getContentTypesFromHTTP returns a map of file extensions to MIME types, taken
// from source. Source is expected to be NGINX
func getContentTypesFromHTTP(source string) (map[string]string, error) {
	externalSource, err := getExternalSource(source)
	if err != nil {
		return nil, err
	}
	types := splitTypeLines(stripEmptyLines(getTypeBlock(externalSource)))
	typeLists := make([][]string, 0)
	for _, t := range types {
		if len(t) > 1 {
			typeLists = append(typeLists, splitWhitespace(t))
		}
	}
	typeMap := pivotTypeLists(typeLists)
	// add .js.map special case
	typeMap["map"] = "application/json"
	return typeMap, nil
}

func getExternalSource(externalSource string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, externalSource, nil)
	if err != nil {
		return "", err
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	rawBody, err := io.ReadAll(res.Body)
	return string(rawBody), err
}

func getTypeBlock(input string) string {
	r := regexp.MustCompile(`(?s){\s*\n(.*)}`)
	m := r.FindStringSubmatch(input)
	return m[1]
}

func stripEmptyLines(input string) string {
	emptyLineRE := regexp.MustCompile(`\n\s*\n`)
	return emptyLineRE.ReplaceAllString(input, "\n")
}

func splitTypeLines(input string) []string {
	return strings.Split(input, ";")
}

func splitWhitespace(input string) []string {
	out := make([]string, 0)
	wsRE := regexp.MustCompile(`\s+`)
	l := wsRE.Split(input, -1)
	for _, v := range l {
		if v != "" {
			out = append(out, v)
		}
	}
	return out
}

func pivotTypeLists(input [][]string) map[string]string {
	out := make(map[string]string)
	for _, typeList := range input {
		mapValue := typeList[0]
		for _, v := range typeList[1:] {
			out[v] = mapValue
		}
	}
	return out
}
