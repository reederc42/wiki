package ui

type File struct {
	ContentType string
	Contents    []byte
}
