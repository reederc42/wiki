kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
{{ if .ci.Kind.APIServerAddress -}}
networking:
  apiServerAddress: {{ .ci.Kind.APIServerAddress }}
  apiServerPort: -1
{{ end -}}
nodes:
- role: control-plane
  image: {{ .ci.Kind.NodeImage }}
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "role=ingress-controller"
  {{ if .ci.Kind.ExposePort -}}
  extraPortMappings:
  - containerPort: {{ .ci.Kind.ExposePort }}
  {{ end -}}
{{ if .ci.Kind.DockerMirror }}
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
    endpoint = ["http://{{ .ci.Kind.DockerMirror }}"]
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.configs."docker.io".tls]
    insecure_skip_verify = true
{{ end -}}
