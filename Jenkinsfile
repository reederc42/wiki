import org.jenkinsci.plugins.pipeline.modeldefinition.Utils

// *** pipeline ***

final PIPELINE_STAGES = [
    'Test-Build': 'test_build',
    'Test-Node': 'test_node',
    'Test-Go': 'test_go',
    'Test-Python': 'test_python',
    'Build-Node': 'build_node',
    'Build-Go': 'build_go',
    'Omnibus-Build': 'omnibus_build',
    'Omnibus-E2E': 'omnibus_e2e',
    'K8s-Build': 'k8s_build',
    'K8s-E2E': 'k8s_e2e',
    'Tag-Latest': 'tag_latest',
    'Omnibus-Deploy': 'omnibus_deploy',
    'K8s-Deploy': 'k8s_deploy'
]

final ALWAYS_STAGES = [
    'Collect-Coverage': 'collect_coverage',
    'Archive': 'archive',
    'Cleanup': 'cleanup'
]

def pipeline = {
    PIPELINE_STAGES.each{ k, v ->
        stage(k) {
            withCredentials([
                file(credentialsId: 'kubeconfig', variable: 'KUBECONFIG'),
                string(credentialsId: 'db-password', variable: 'WIKI_CI_PROD_POSTGRES_PASSWORD'),
                string(credentialsId: 'vault-url', variable: 'WIKI_CI_PROD_VAULT_URL'),
                string(credentialsId: 'vault-token', variable: 'WIKI_CI_PROD_VAULT_TOKEN')
            ]) {
                runStage(v)
            }
        }
    }
}

def always = {
    ALWAYS_STAGES.each { k, v ->
        try {
            stage(k) {
                try {
                    runStage(v)
                } catch (Exception e) {
                    print("Ignoring exception: ${e}")
                    throw e
                }
            }
        } catch (Exception e) {}
    }
}

// *** functions ***

def archiveStage(String stage) {
    artifacts = sh(returnStdout: true, script: "_ci/ci.py --list-artifacts ${stage}")
    def junitPrefix = 'junit: '
    def archivePrefix = 'archive: '
    def list = artifacts.split('\n')
    list.each { l ->
        if (l.contains(junitPrefix)) {
            junit(testResults: l - junitPrefix, skipPublishingChecks: true)
        } else if (l.contains(archivePrefix)) {
            archiveArtifacts(artifacts: l - archivePrefix)
        }
    }
}

def runStage(String stage) {
    if (skipStage(stage))
        return
    try {
        sh "_ci/ci.py ${stage}"
    } finally {
        archiveStage(stage)
    }
}

Boolean skipStage(String stage) {
    def skipMessage = sh(returnStdout: true, script: "_ci/ci.py ${stage} --check-skip")
    if (skipMessage.contains("skipped ${STAGE_NAME}:")) {
        Utils.markStageSkippedForConditional(STAGE_NAME)
        echo skipMessage
        return true
    }
    return false
}

// *** pipeline entry ***

node {
    checkout scm

    env.WIKI_CI_CONTAINER_CMD = 'docker'
    env.WIKI_CI_RUNNER = 'container'
    env.WIKI_CI_REGISTRY_MIRROR = env.DOCKER_MIRROR
    env.WIKI_CI_DOCKER_REGISTRY = env.DOCKER_REGISTRY
    env.WIKI_CI_LOCAL_ARCHIVE = 'false'
    env.WIKI_CI_LOCAL_CLEANUP = 'false'
    env.WIKI_CI_CONTAINER_WORKSPACE_MOUNT = env.CONTAINER_WORKSPACE_MOUNT
    env.WIKI_CI_BRANCH_NAME = env.BRANCH_NAME
    env.PYTHONUNBUFFERED = 'true'

    env.WIKI_CI_COVERAGE = 'false'
    if (env.JOB_NAME.split('/')[0].contains('coverage')) {
        env.WIKI_CI_COVERAGE = 'true'
    }

    try {
        pipeline()
    } finally {
        always()
    }

    cleanWs()
}
