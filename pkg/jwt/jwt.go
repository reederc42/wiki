// Package jwt implements policy-based JWTs, including encryption, decryption,
// signing, and secrets generation
package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"strings"
	"time"

	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

var (
	ErrExpired       = jwt.ErrExpired
	ErrCryptoFailure = jose.ErrCryptoFailure
)

const keySize = 2048

type Secret struct {
	EncryptionPvt *rsa.PrivateKey
	EncryptionPub *rsa.PublicKey
	SignaturePvt  *rsa.PrivateKey
	SignaturePub  *rsa.PublicKey
}

func FromString(s string) (*Secret, error) {
	var sec Secret
	d := []byte(s)
	for i := 0; i < 4; i++ {
		b, r := pem.Decode(d)
		if b == nil {
			return nil, errors.New("unexpected EOF")
		}
		d = r
		switch b.Type {
		case "ENCRYPTION PRIVATE KEY":
			k, err := readPrivateKey(b.Bytes)
			if err != nil {
				return nil, err
			}
			sec.EncryptionPvt = k
		case "ENCRYPTION PUBLIC KEY":
			k, err := readPublicKey(b.Bytes)
			if err != nil {
				return nil, err
			}
			sec.EncryptionPub = k
		case "SIGNATURE PRIVATE KEY":
			k, err := readPrivateKey(b.Bytes)
			if err != nil {
				return nil, err
			}
			sec.SignaturePvt = k
		case "SIGNATURE PUBLIC KEY":
			k, err := readPublicKey(b.Bytes)
			if err != nil {
				return nil, err
			}
			sec.SignaturePub = k
		}
	}
	return &sec, nil
}

func (s *Secret) String() (string, error) {
	var b strings.Builder
	if err := writePrivateKey(&b, "ENCRYPTION PRIVATE KEY",
		s.EncryptionPvt); err != nil {
		return "", err
	}
	if err := writePublicKey(&b, "ENCRYPTION PUBLIC KEY",
		s.EncryptionPub); err != nil {
		return "", err
	}
	if err := writePrivateKey(&b, "SIGNATURE PRIVATE KEY",
		s.SignaturePvt); err != nil {
		return "", err
	}
	if err := writePublicKey(&b, "SIGNATURE PUBLIC KEY",
		s.SignaturePub); err != nil {
		return "", err
	}
	return b.String(), nil
}

func readPrivateKey(data []byte) (*rsa.PrivateKey, error) {
	p, err := x509.ParsePKCS8PrivateKey(data)
	if err != nil {
		return nil, err
	}
	k, ok := p.(*rsa.PrivateKey)
	if !ok {
		return nil, errors.New("unsupported private key type")
	}
	return k, nil
}

func readPublicKey(data []byte) (*rsa.PublicKey, error) {
	p, err := x509.ParsePKIXPublicKey(data)
	if err != nil {
		return nil, err
	}
	k, ok := p.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("unsupported public key type")
	}
	return k, nil
}

func writePrivateKey(b *strings.Builder, typ string,
	key *rsa.PrivateKey,
) error {
	k, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return err
	}
	p := pem.EncodeToMemory(&pem.Block{
		Type:  typ,
		Bytes: k,
	})
	_, err = b.WriteString(string(p))
	return err
}

func writePublicKey(b *strings.Builder, typ string, key *rsa.PublicKey) error {
	k, err := x509.MarshalPKIXPublicKey(key)
	if err != nil {
		return err
	}
	p := pem.EncodeToMemory(&pem.Block{
		Type:  typ,
		Bytes: k,
	})
	_, err = b.WriteString(string(p))
	return err
}

// Encode encodes a JWT from claims with expiry
func Encode(claims interface{}, expiry time.Time,
	secret *Secret,
) (string, error) {
	sig, err := jose.NewSigner(jose.SigningKey{
		Algorithm: jose.RS256,
		Key:       secret.SignaturePvt,
	}, nil)
	if err != nil {
		return "", err
	}
	enc, err := jose.NewEncrypter(jose.A128GCM, jose.Recipient{
		Algorithm: jose.RSA_OAEP,
		Key:       secret.EncryptionPub,
	}, (&jose.EncrypterOptions{}).WithType("JWT").WithContentType("JWT"))
	if err != nil {
		return "", err
	}
	exp := jwt.Claims{
		Expiry: jwt.NewNumericDate(expiry),
	}
	return jwt.SignedAndEncrypted(sig, enc).Claims(exp).Claims(claims).
		CompactSerialize()
}

// Decode decodes claims into dest from a token, checking token is expired
func Decode(dest interface{}, token string, secret *Secret,
	leeway time.Duration,
) error {
	tok, err := jwt.ParseSignedAndEncrypted(token)
	if err != nil {
		return err
	}
	t, err := tok.Decrypt(secret.EncryptionPvt)
	if err != nil {
		return err
	}
	var exp jwt.Claims
	if err := t.Claims(secret.SignaturePub, dest, &exp); err != nil {
		return err
	}
	return exp.ValidateWithLeeway(jwt.Expected{
		Time: time.Now(),
	}, leeway)
}

func NewSecret() (*Secret, error) {
	enc, err := rsa.GenerateKey(rand.Reader, keySize)
	if err != nil {
		return nil, err
	}
	sig, err := rsa.GenerateKey(rand.Reader, keySize)
	if err != nil {
		return nil, err
	}
	return &Secret{
		EncryptionPvt: enc,
		EncryptionPub: &enc.PublicKey,
		SignaturePvt:  sig,
		SignaturePub:  &sig.PublicKey,
	}, nil
}
