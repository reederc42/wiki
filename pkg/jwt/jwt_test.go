package jwt

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

type testClaims struct {
	UserID int
}

const leeway = 200 * time.Millisecond

func TestJWT(t *testing.T) {
	secret, err := NewSecret()
	require.NoError(t, err)
	decodeSecret, err := NewSecret()
	require.NoError(t, err)
	tests := []struct {
		Name          string
		Claims        testClaims
		Expiration    time.Time
		EncodeSecret  *Secret
		DecodeSecret  *Secret
		ExpectedError error
	}{
		{
			Name: "happy path",
			Claims: testClaims{
				UserID: 1,
			},
			Expiration:   time.Now().Add(1 * time.Hour),
			EncodeSecret: secret,
			DecodeSecret: secret,
		},
		{
			Name: "expired",
			Claims: testClaims{
				UserID: 2,
			},
			Expiration:    time.Now().Add(-1 * time.Hour),
			ExpectedError: jwt.ErrExpired,
			EncodeSecret:  secret,
			DecodeSecret:  secret,
		},
		{
			Name: "wrong secret",
			Claims: testClaims{
				UserID: 0,
			},
			Expiration:    time.Now().Add(1 * time.Hour),
			EncodeSecret:  secret,
			DecodeSecret:  decodeSecret,
			ExpectedError: jose.ErrCryptoFailure,
		},
	}
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			tok, err := Encode(tests[i].Claims, tests[i].Expiration,
				tests[i].EncodeSecret)
			require.NoError(t, err)
			var c testClaims
			err = Decode(&c, tok, tests[i].DecodeSecret, leeway)
			require.Equal(t, tests[i].ExpectedError, err)
			require.Equal(t, tests[i].Claims, c)
		})
	}
}

func TestSecretEncode(t *testing.T) {
	sec, err := NewSecret()
	require.NoError(t, err)
	s, err := sec.String()
	require.NoError(t, err)
	sec1, err := FromString(s)
	require.NoError(t, err)
	require.Equal(t, sec, sec1)
}
