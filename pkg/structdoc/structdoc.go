// Package structdoc allows programmatic access to struct documentation.
// structdoc was inspired by godoc, which produces HTML pages from comments in
// your code, but structdoc specifically acts only on structs. structdoc
// provides a code generator to extract documentation. Get and Set are not
// thread-safe. It is recommended that Set is only called in init() functions,
// and Get is never called in init() functions.
package structdoc

import (
	"reflect"

	"gitlab.com/reederc42/wiki/pkg/unwind"
)

var globalDoc map[string]Doc

// Doc holds the documentation for a struct
type Doc struct {
	// Doc is the multiline string value of the comment above a struct or field
	Doc string
	// Order sets key order, which can control how structdocs for structures
	// like maps are displayed
	Order int
	// Items are any fields of the current struct or field
	Items map[string]Doc
}

// Set adds a Doc to the global type map. Panics if t is not a struct. Get and
// Set are not thread-safe, and Set should only be used during initialization.
func Set(t interface{}, d Doc) {
	v := unwind.Unwind(reflect.ValueOf(t))
	if v.Kind() != reflect.Struct {
		panic("t must be a struct")
	}
	if len(globalDoc) == 0 {
		globalDoc = make(map[string]Doc)
	}
	globalDoc[typeName(v)] = d
}

// Get gets a Doc from the global type map. Panics if t is not a struct, returns
// empty Doc if t is not in global type map. Get and Set are not thread-safe,
// and Get should not be used until all Sets are called. Get is recursive and
// will Get each substruct, if it's set.
func Get(t interface{}) (Doc, bool) {
	v := unwind.Unwind(reflect.ValueOf(t))
	if v.Kind() != reflect.Struct {
		panic("t must be a struct")
	}
	n := typeName(v)
	d, ok := globalDoc[n]
	if !ok {
		return d, ok
	}
	if d.Items == nil {
		d.Items = make(map[string]Doc)
	}
	j := v.Type()
	for i := 0; i < j.NumField(); i++ {
		f := j.Field(i)
		if !f.IsExported() || f.Type.Kind() != reflect.Struct {
			continue
		}
		dt, ok := Get(v.Field(i).Interface())
		if ok {
			mergeDoc(d.Items, f.Name, dt)
		}
	}
	return d, ok
}

func mergeDoc(items map[string]Doc, name string, sub Doc) {
	d, ok := items[name]
	if !ok {
		items[name] = sub
		return
	}
	// more specific overrides less specific, so existing doc overrides sub doc
	if d.Doc == "" {
		d.Doc = sub.Doc
	}
	if d.Items == nil {
		d.Items = make(map[string]Doc)
	}
	for k, v := range sub.Items {
		if _, ok := d.Items[k]; !ok {
			d.Items[k] = v
		}
	}
	items[name] = d
}

// New creates a new Doc with all keys present, without any doc filled in. If
// using existing docs, New attempts to get existing docs (for example, for sub
// structures). If t has existing docs, then New(t, true) == Get(t). Panics if t
// is not a struct.
func New(t interface{}, useExistingDoc bool) Doc {
	v := unwind.Unwind(reflect.ValueOf(t))
	if v.Kind() != reflect.Struct {
		panic("t must be a struct")
	}
	return newDoc(v, useExistingDoc)
}

func newDoc(v reflect.Value, useExistingDoc bool) Doc {
	if useExistingDoc {
		d, ok := Get(v.Interface())
		if ok {
			return d
		}
	}
	d := Doc{Items: make(map[string]Doc)}
	t := v.Type()
	for i := 0; i < t.NumField(); i++ {
		j := t.Field(i)
		if !j.IsExported() {
			continue
		}
		k := v.Field(i)
		if k.Kind() == reflect.Struct {
			d.Items[j.Name] = newDoc(k, useExistingDoc)
		} else {
			d.Items[j.Name] = Doc{}
		}
	}
	return d
}

func typeName(v reflect.Value) string {
	return v.Type().PkgPath() + v.Type().Name()
}
