// Package main implements the structdoc generator, which takes Go source and creates Go source that initializes global
// structdoc for the specified type.
//
// Go type definition
// Go types are declared in GenDecl, which has-a []Spec
// Each Spec must be a TypeSpec, which has-a []Field
// Each Field has-a Name; this Name is placed in the Doc.Items map of its parent
// Each Field may have a CommentGroup, whose Text() is placed in the Doc.Items[Name].Doc of its parent
// Each Field has a Type, which may be StructType, which must be traversed
//
// A type might be a TypeSpec or GenDecl
package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"os"
	"strings"
	"text/template"

	"golang.org/x/tools/go/ast/astutil"

	"gitlab.com/reederc42/wiki/pkg/structdoc"

	"github.com/spf13/pflag"
)

var (
	sourceFile    string
	typeName      string
	trimFieldName bool
	pkg           string
	outputFile    string
	importPath    string
	buildTags     string
)

func init() {
	pflag.StringVarP(&sourceFile, "source-file", "f", "", "Go source file that defines type")
	pflag.StringVarP(&typeName, "type", "t", "", "type name")
	pflag.BoolVarP(&trimFieldName, "trim-field-name", "r", false, "remove field name from beginning of comment")
	pflag.StringVarP(&pkg, "package", "p", "", "package name for generated Go file")
	pflag.StringVarP(&outputFile, "output", "o", "", "output file name (default stdout)")
	pflag.StringVarP(&importPath, "import", "i", "", "import path of type, required if placing generated source "+
		"outside of type definition package")
	pflag.StringVarP(&buildTags, "build-tags", "b", "", "optional build tags for generated source")
}

func main() {
	pflag.Parse()
	if err := cmd(); err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}
}

func cmd() error {
	if sourceFile == "" || !strings.HasSuffix(sourceFile, ".go") {
		return fmt.Errorf("file must be Go source")
	}
	if typeName == "" {
		return fmt.Errorf("must provide type")
	}
	var fset token.FileSet
	f, err := parser.ParseFile(&fset, sourceFile, nil, parser.ParseComments)
	if err != nil {
		return err
	}
	t, i := findType(typeName, f)
	if t == nil {
		return fmt.Errorf("could not find type %s", typeName)
	}
	var doc structdoc.Doc
	if err := convertTypeToDoc(t, i, trimFieldName, &doc); err != nil {
		return err
	}
	data := sourceData{
		BuildTags:       buildTags,
		Pkg:             pkg,
		StructdocImport: structdocImport,
		TypeImport:      importPath,
		StructdocDoc:    fmt.Sprintf("%#v", doc),
	}
	if importPath != "" {
		ts := strings.Split(importPath, "/")
		data.Type = ts[len(ts)-1] + "." + typeName
	} else {
		data.Type = typeName
	}
	w := os.Stdout
	if outputFile != "" {
		f, err := os.Create(outputFile)
		if err != nil {
			return err
		}
		defer f.Close()
		w = f
	}
	return writeSource(data, w)
}

// findType find the general declaration and index of type name in file f
func findType(name string, f *ast.File) (*ast.GenDecl, int) {
	var out *ast.GenDecl
	var index int
	_ = astutil.Apply(f, func(c *astutil.Cursor) bool {
		decl, ok := c.Node().(*ast.GenDecl)
		if !ok || decl.Tok != token.TYPE {
			return true
		}
		for i, d := range decl.Specs {
			ts, ok := d.(*ast.TypeSpec)
			if !ok {
				return true
			}
			if ts.Name.String() == name {
				out = decl
				index = i
				return false
			}
		}
		return true
	}, nil)
	return out, index
}

// convertTypeToDoc converts a type declaration node to structdoc. Doc should be non-nil but empty structdoc. Type spec
// comments are prioritized over gen decl comments, and gen decl comments are ignored completely if there is more than
// 1 type spec in the gen decl. This ignores the type comment for parenthesized lists of types, i.e. type (/* types */),
// but maintains the type comment for a single type declaration, i.e. type MyType.
func convertTypeToDoc(t ast.Node, index int, trimFieldName bool, doc *structdoc.Doc) error {
	if t == nil {
		return fmt.Errorf("node is nil")
	}
	var ts *ast.TypeSpec
	if gd, ok := t.(*ast.GenDecl); ok {
		if len(gd.Specs) == 1 && gd.Doc != nil {
			doc.Doc = gd.Doc.Text()
		}
		t = gd.Specs[index]
	}
	if s, ok := t.(*ast.TypeSpec); ok {
		ts = s
	} else {
		return fmt.Errorf("node not type spec")
	}
	if ts == nil {
		return fmt.Errorf("node is nil")
	}
	if ts.Doc != nil {
		d := ts.Doc.Text()
		if d != "" {
			doc.Doc = d
		}
	}
	if st, ok := ts.Type.(*ast.StructType); ok {
		return convertStructTypeToDoc(st, trimFieldName, doc)
	}
	return fmt.Errorf("node not struct type")
}

func convertStructTypeToDoc(t *ast.StructType, trimFieldName bool, doc *structdoc.Doc) error {
	if t == nil {
		return fmt.Errorf("node is nil")
	}
	doc.Items = make(map[string]structdoc.Doc)
	if t.Fields == nil {
		return fmt.Errorf("empty field list")
	}
	for _, f := range t.Fields.List {
		if f == nil {
			continue
		}
		var name string
		if len(f.Names) == 1 && f.Names[0] != nil && ast.IsExported(f.Names[0].String()) {
			name = f.Names[0].String()
		} else {
			continue
		}
		var d structdoc.Doc
		d.Doc = getComment(f.Doc, trimFieldName, name)
		if st, ok := f.Type.(*ast.StructType); ok {
			if err := convertStructTypeToDoc(st, trimFieldName, &d); err != nil {
				return err
			}
		}
		doc.Items[name] = d
	}
	return nil
}

func getComment(c *ast.CommentGroup, trimFieldName bool, name string) string {
	if c == nil {
		return ""
	}
	d := strings.TrimSuffix(c.Text(), "\n")
	if trimFieldName {
		d = strings.TrimLeft(strings.TrimPrefix(d, name), " ")
	}
	return d
}

type sourceData struct {
	BuildTags       string
	Pkg             string
	StructdocImport string
	TypeImport      string
	Type            string
	StructdocDoc    string
}

func writeSource(data sourceData, w io.Writer) error {
	data.StructdocImport = "\"" + data.StructdocImport + "\""
	if data.TypeImport != "" {
		data.TypeImport = "\"" + data.TypeImport + "\""
	}
	if data.BuildTags != "" {
		data.BuildTags = "//go:build " + data.BuildTags
	}
	t, err := template.New("").Parse(tpl)
	if err != nil {
		return err
	}
	return t.Execute(w, data)
}

const structdocImport = "gitlab.com/reederc42/wiki/pkg/structdoc"

const tpl = `{{ .BuildTags }}
// Code generated by structdoc. DO NOT EDIT.
package {{ .Pkg }}

import (
	{{ .StructdocImport }}
	{{ .TypeImport }}
)

func init() {
	structdoc.Set(
		{{ .Type }}{},
		{{ .StructdocDoc }},
	)
}
`
