package structdoc

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testStruct struct {
	Field1 int
	Field2 map[string]int
	Field3 testSubStruct
}

type testSubStruct struct {
	Field1 uint
}

func TestSet(t *testing.T) {
	defer deleteDoc(testStruct{})
	// Panic test
	assert.Panics(t, func() {
		Set("string", Doc{})
	}, "t is a struct")
	// Happy path
	assert.NotPanics(t, func() {
		Set(testStruct{}, Doc{})
	}, "t is not a zero-valued struct")
}

func TestGet(t *testing.T) {
	defer deleteDoc(testStruct{})
	assert.NotPanics(t, func() {
		d, ok := Get(testStruct{})
		assert.Equal(t, Doc{}, d)
		assert.False(t, ok)
	})
	expected := Doc{
		Doc:   "struct comment",
		Items: make(map[string]Doc),
	}
	Set(testStruct{}, expected)
	// Panic test
	assert.Panics(t, func() {
		_, _ = Get("string")
	}, "t is a struct")
	// Happy path
	assert.NotPanics(t, func() {
		_, _ = Get(testStruct{})
	}, "t is not a zero-valued struct")
	d, _ := Get(testStruct{})
	assert.Equal(t, expected, d)
}

func TestNew(t *testing.T) {
	expected := Doc{
		Items: map[string]Doc{
			"Field1": {},
			"Field2": {},
			"Field3": {
				Items: map[string]Doc{
					"Field1": {},
				},
			},
		},
	}
	actual := New(testStruct{}, false)
	assert.Equal(t, expected, actual)
}

// deleteDoc is a helper function to delete documentation from global docs
func deleteDoc(t interface{}) {
	delete(globalDoc, typeName(reflect.ValueOf(t)))
}
