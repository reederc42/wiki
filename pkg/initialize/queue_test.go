package initialize

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var callOrder []string

type (
	fooKey   struct{}
	errorKey struct{}
)

func foo(ctx context.Context) (context.Context, error) {
	callOrder = append(callOrder, "foo")
	return context.WithValue(ctx, fooKey{}, "foo"), nil
}

func bar(ctx context.Context) (context.Context, error) {
	callOrder = append(callOrder, "bar")
	return ctx, nil
}

func fooError(ctx context.Context) (context.Context, error) {
	callOrder = append(callOrder, "fooError")
	return context.WithValue(ctx, errorKey{}, "fooError"), fmt.Errorf("some error")
}

func TestExecute_NoError(t *testing.T) {
	var q Queue
	callOrder = make([]string, 0)
	expected := []string{"foo", "bar"}
	q.Push(foo, 0)
	q.Push(bar, 1)
	ctx, err := q.Execute(context.Background())
	assert.NoError(t, err)
	assert.Equal(t, expected, callOrder)
	assert.Equal(t, "foo", ctx.Value(fooKey{}))
}

func TestExecute_Error(t *testing.T) {
	var q Queue
	callOrder = make([]string, 0)
	expected := []string{"bar", "fooError"}
	q.Push(foo, 2)
	q.Push(fooError, 1)
	q.Push(bar, 0)
	ctx, err := q.Execute(context.Background())
	assert.Error(t, fmt.Errorf("some error"), err)
	assert.Equal(t, expected, callOrder)
	assert.Nil(t, ctx.Value(fooKey{}))
}

func TestPublic(t *testing.T) {
	q := NewQueueWithCap(10)
	callOrder = make([]string, 0)
	expected := []string{"bar", "foo"}
	q.Push(foo, 3)
	q.Push(bar, -1)
	ctx := context.Background()
	for _, i := range q.List() {
		c, err := i(ctx)
		assert.NoError(t, err)
		ctx = c
	}
	assert.Equal(t, expected, callOrder)
	q.Delete()
	assert.Zero(t, len(q.i))
	assert.Zero(t, len(q.p))
}
