// Package initialize provides a min-priority queue for initialization
// functions, that can be added using Go's builtin init() func. State is passed
// between initializers using contexts.
package initialize

import (
	"context"
	"sort"
)

type Func func(context.Context) (context.Context, error)

// Queue is a min-priority queue of initialization functions. It can be used
// without initialization.
type Queue struct {
	i []Func
	p []int
}

// NewQueueWithCap returns a min-priority queue of initialization functions,
// with a capacity of cap.
func NewQueueWithCap(cap int) Queue {
	return Queue{
		i: make([]Func, 0, cap),
		p: make([]int, 0, cap),
	}
}

// Push adds initializer function i with priority p
func (q *Queue) Push(i Func, p int) {
	if q.i == nil {
		q.i = make([]Func, 0)
		q.p = make([]int, 0)
	}
	q.i = append(q.i, i)
	q.p = append(q.p, p)
}

// List returns the prioritized list of initializers
func (q *Queue) List() []Func {
	return q.list()
}

// Delete frees the lists of initializers
func (q *Queue) Delete() {
	q.del()
}

// Execute executes initializers in priority order, returning the context and
// error of the first error encountered. Queue is deleted after Execute.
func (q *Queue) Execute(ctx context.Context) (context.Context, error) {
	defer q.del()
	for _, i := range q.list() {
		c, err := i(ctx)
		if err != nil {
			return c, err
		}
		ctx = c
	}
	return ctx, nil
}

func (q *Queue) list() []Func {
	sort.Sort(&queueSorter{q})
	return q.i
}

func (q *Queue) del() {
	q.i = nil
	q.p = nil
}

// queueSorter implements sort interface for Queue
type queueSorter struct {
	*Queue
}

// Len is implemented for sorting
func (q *queueSorter) Len() int {
	return len(q.i)
}

// Swap is implemented for sorting
func (q *queueSorter) Swap(i, j int) {
	q.i[i], q.i[j] = q.i[j], q.i[i]
	q.p[i], q.p[j] = q.p[j], q.p[i]
}

// Less is implemented for sorting
func (q *queueSorter) Less(i, j int) bool {
	return q.p[i] < q.p[j]
}
