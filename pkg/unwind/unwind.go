package unwind

import "reflect"

// Unwind returns underlying non-nil value of interface or pointer. Post:
// changes value of v
func Unwind(v reflect.Value) reflect.Value {
	for (v.Kind() == reflect.Interface || v.Kind() == reflect.Ptr) &&
		!v.IsNil() {
		v = v.Elem()
	}
	return v
}
