// Package trace implements zap- and context-based request tracing
package trace

import (
	"context"
	"io"
	"math/rand"
	"sync"
	"time"

	"github.com/oklog/ulid/v2"
	"go.uber.org/zap"
)

var entropies = sync.Pool{
	New: func() interface{} {
		return newEntropy()
	},
}

type traceContextKey struct{}

// New annotates l with a new trace. This is intended for the beginning of a
// request or trace: log producers should use L().
func New(l *zap.Logger) *zap.Logger {
	return l.With(zap.String("trace_id", newID()), zap.Time("trace_start",
		time.Now()))
}

// Set adds the supplied logger (assumed to be a trace) to the context
func Set(ctx context.Context, l *zap.Logger) context.Context {
	return context.WithValue(ctx, traceContextKey{}, l)
}

// L gets the trace of the context. If the context does not have a trace, it will
// panic.
func L(ctx context.Context) *zap.Logger {
	t := ctx.Value(traceContextKey{})
	if t == nil {
		panic("context does not have trace")
	}
	return t.(*zap.Logger)
}

func newID() string {
	entropy := entropies.Get().(io.Reader)
	if entropy == nil {
		panic("trace: no entropy")
	}
	u := ulid.MustNew(ulid.Timestamp(time.Now()), entropy).String()
	entropies.Put(entropy)
	return u
}

func newEntropy() io.Reader {
	return ulid.Monotonic(rand.New(rand.NewSource(time.Now().UnixNano())), 0)
}
