package yamldoc

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

type testType struct {
	Str     string
	StrList []string
	Int     int
}

var testDoc = structdoc.Doc{
	Items: map[string]structdoc.Doc{
		"key1": {
			Doc: "This is key 1",
			Items: map[string]structdoc.Doc{
				"Str": {
					Doc: "this is a string inside key1",
				},
				"StrList": {
					Doc: `this is
a multiline string`,
				},
			},
		},
	},
}

func TestMarshalWithDoc(t *testing.T) {
	structdoc.Set(testType{}, structdoc.Doc{
		Doc: "testType is the type used for tests",
		Items: map[string]structdoc.Doc{
			"Str": {
				Doc: `this is a
multiline string for key Str`,
			},
			"StrList": {
				Doc: "a list of strings",
			},
		},
	})
	defer structdoc.Set(testType{}, structdoc.Doc{})

	m := map[interface{}]interface{}{
		"key1": &testType{
			Str:     "some string",
			StrList: []string{"one"},
			Int:     0,
		},
		"key2": nil,
		"key3": []*testType{
			{
				Str:     "string1",
				StrList: []string{"two"},
				Int:     1,
			},
			{
				Str:     "string 2",
				StrList: []string{"three"},
				Int:     2,
			},
		},
	}

	_, err := marshalWithDoc(m, testDoc)
	assert.NoError(t, err)
	// fmt.Printf("%s\n", b)
	// TODO: walk through nodes and ensure comments are set as expected
}
