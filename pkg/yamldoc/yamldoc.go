// Package yamldoc extends the yaml.v3 interface to include documentation.
// Integrates gitlab.com/reederc42/wiki/pkg/structdoc to allow the user to set
// custom documentation or use globally set struct documentation.
package yamldoc

import (
	"fmt"
	"reflect"
	"sort"
	"strings"

	"gopkg.in/yaml.v3"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/pkg/unwind"
)

// Marshal serializes the value of in according to yaml.v3, adding struct docs
func Marshal(in interface{}) ([]byte, error) {
	return marshalWithDoc(in, structdoc.Doc{})
}

// MarshalWithDoc serializes the value of in according to yaml.v3, adding struct
// docs and custom docs.
func MarshalWithDoc(in interface{}, doc structdoc.Doc) ([]byte, error) {
	return marshalWithDoc(in, doc)
}

// Unmarshal deserializes in into out, according to yaml.v3
func Unmarshal(in []byte, out interface{}) error {
	return yaml.Unmarshal(in, out)
}

type Node struct {
	*yaml.Node
}

func (n *Node) Decode(v interface{}) error {
	if n.Node == nil {
		n.Node = &yaml.Node{}
	}
	return n.Node.Decode(v)
}

func (n *Node) Encode(v interface{}) error {
	return n.encodeWithDoc(v, structdoc.Doc{})
}

func (n *Node) EncodeWithDoc(v interface{}, doc structdoc.Doc) error {
	return n.encodeWithDoc(v, doc)
}

func (n *Node) encodeWithDoc(v interface{}, d structdoc.Doc) error {
	n.Node = &yaml.Node{}
	if err := n.Node.Encode(v); err != nil {
		return err
	}
	return addDoc(n.Node, reflect.ValueOf(v), d)
}

func (n *Node) IsZero() bool {
	return n.Node.IsZero()
}

func (n *Node) LongTag() string {
	return n.Node.LongTag()
}

func (n *Node) SetString(s string) {
	n.Node.SetString(s)
}

func (n *Node) ShortTag() string {
	return n.Node.ShortTag()
}

func marshalWithDoc(in interface{}, doc structdoc.Doc) ([]byte, error) {
	n := &yaml.Node{}
	if err := n.Encode(in); err != nil {
		return nil, err
	}
	if err := addDoc(n, reflect.ValueOf(in), doc); err != nil {
		return nil, err
	}
	return yaml.Marshal(n)
}

// addDoc adds documentation d to n. in is required because the keys of d use
// field names, instead of n's yaml key names. d overrides the global structdocs
// and if a key is not present in d, it is ignored. Panics if n is nil. Sorts
// nodes by order field in docs
func addDoc(n *yaml.Node, in reflect.Value, d structdoc.Doc) error {
	in = unwind.Unwind(in)
	// set the header comment if it hasn't been supplied
	if n.HeadComment == "" {
		// if in is a struct, check for global struct doc
		if in.Kind() == reflect.Struct {
			d = getDocForStruct(in, d)
		}
		n.HeadComment = d.Doc
	}
	// walk through substructures
	switch n.Kind {
	case yaml.SequenceNode:
		// sequences can only be documented if their elements are structs in the
		// global struct docs
		return addDocToSequence(n, in)
	case yaml.MappingNode:
		switch in.Kind() {
		case reflect.Struct:
			return addDocToStruct(n, in, d)
		case reflect.Map:
			return addDocToMap(n, in, d)
		}
	}
	return nil
}

// getDocForStruct gets the docs for struct v
func getDocForStruct(v reflect.Value, d structdoc.Doc) structdoc.Doc {
	if d.Doc != "" || len(d.Items) != 0 {
		return d
	}
	o, _ := structdoc.Get(v.Interface())
	return o
}

func addDocToSequence(n *yaml.Node, in reflect.Value) error {
	for i, c := range n.Content {
		if !canIndex(in) {
			return fmt.Errorf("mismatched types: cannot index %s",
				in.Kind().String())
		}
		v := in.Index(i)
		if err := addDoc(c, v, structdoc.Doc{}); err != nil {
			return err
		}
	}
	return nil
}

func canIndex(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Array, reflect.Slice, reflect.String:
		return true
	}
	return false
}

type nodeSorter struct {
	docs  []*structdoc.Doc
	nodes []*yaml.Node
}

func (n *nodeSorter) Len() int {
	return len(n.docs)
}

func (n *nodeSorter) Swap(i, j int) {
	n.docs[i], n.docs[j] = n.docs[j], n.docs[i]
	n.nodes[2*i], n.nodes[2*j] = n.nodes[2*j], n.nodes[2*i]
	n.nodes[2*i+1], n.nodes[2*j+1] = n.nodes[2*j+1], n.nodes[2*i+1]
}

func (n *nodeSorter) Less(i, j int) bool {
	return n.docs[i].Order < n.docs[j].Order
}

// addDocToStruct adds docs to keys only, assumes d is the correct docs for
// struct and that struct doc has already been added
func addDocToStruct(n *yaml.Node, in reflect.Value, d structdoc.Doc) error {
	fields := mapStructFields(in)
	docs := make([]*structdoc.Doc, 0, len(n.Content)/2)
	for i := 0; i < len(n.Content); i += 2 {
		v := fields[n.Content[i].Value]
		var vd structdoc.Doc
		if len(d.Items) > 0 {
			vd = d.Items[v.name]
		}
		docs = append(docs, &vd)
		if err := addDoc(n.Content[i], v.value,
			structdoc.Doc{Doc: vd.Doc}); err != nil {
			return err
		}
		if err := addDoc(n.Content[i+1], v.value,
			structdoc.Doc{Items: vd.Items}); err != nil {
			return err
		}
	}
	sorter := &nodeSorter{
		docs:  docs,
		nodes: n.Content,
	}
	sort.Stable(sorter)
	return nil
}

type structField struct {
	name  string
	value reflect.Value
}

func mapStructFields(v reflect.Value) map[string]structField {
	if v.Kind() != reflect.Struct {
		return nil
	}
	m := make(map[string]structField)
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		f := t.Field(i)
		// ignore if field is not exported
		if !f.IsExported() {
			continue
		}
		y := getYAMLName(f)
		m[y] = structField{
			name:  f.Name,
			value: v.Field(i),
		}
	}
	return m
}

func getYAMLName(f reflect.StructField) string {
	n := f.Tag.Get("yaml")
	if n == "" {
		n = strings.ToLower(f.Name)
	}
	return n
}

func addDocToMap(n *yaml.Node, in reflect.Value, d structdoc.Doc) error {
	docs := make([]*structdoc.Doc, 0, len(n.Content)/2)
	for i := 0; i < len(n.Content); i += 2 {
		v := in.MapIndex(reflect.ValueOf(n.Content[i].Value))
		var vd structdoc.Doc
		if len(d.Items) > 0 {
			vd = d.Items[n.Content[i].Value]
		}
		docs = append(docs, &vd)
		if err := addDoc(n.Content[i], v,
			structdoc.Doc{Doc: vd.Doc}); err != nil {
			return err
		}
		if err := addDoc(n.Content[i+1], v,
			structdoc.Doc{Items: vd.Items}); err != nil {
			return err
		}
	}
	sorter := &nodeSorter{
		docs:  docs,
		nodes: n.Content,
	}
	sort.Stable(sorter)
	return nil
}

// printNodes is used in testing to visualize yaml node graph
// func printNodes(n *yaml.Node, indent string, count *int) int {
// 	k, v := "", "null"
// 	switch n.Kind {
// 	case yaml.DocumentNode:
// 		k = "Document"
// 	case yaml.SequenceNode:
// 		k = "Sequence"
// 	case yaml.MappingNode:
// 		k = "Mapping"
// 	case yaml.ScalarNode:
// 		k = "Scalar"
// 	case yaml.AliasNode:
// 		k = "Alias"
// 	}
// 	if n.Value != "" {
// 		v = n.Value
// 	}
// 	fmt.Printf("%sKind: %s Value: %s\n", indent, k, v)
// 	*count++
// 	for _, c := range n.Content {
// 		printNodes(c, indent+"    ", count)
// 	}
// 	return *count
// }
