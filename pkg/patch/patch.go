// Package patch implements Myer's diff algorithm with portable patches. A patch
// is a set of diff operations, which can be applied to or removed from a
// string. A patch is portable because it can be converted to and from a string.
// Strings recovered by applying or removing patches always have ending newlines
package patch

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Patch a set of changes that can be applied to or removed from a string to
// produce another string
type Patch []Op

// Op is a single operation
type Op struct {
	K OpKind
	F int
	T int
	V string
}

// OpKind represents a kind of operation: insert, delete, etc
type OpKind int

const (
	// OpNoChange represents an invalid/default operation
	OpNoChange OpKind = iota
	// OpInsert represents an insert operation
	OpInsert
	// OpDelete represents a delete operation
	OpDelete
	// OpReplace represents a replacement operation
	OpReplace
)

// Find returns a patch from original to new
func Find(original, new string) Patch {
	o := split(original, "\n")
	n := split(new, "\n")
	d := recoverPath(paths(o, n), len(o), len(n))
	ops := make([]Op, 0, len(d))
	for i := 1; i < len(d); i++ {
		op := Op{
			K: OpNoChange,
			F: d[i][1] - 1,
			T: d[i][0] - 1,
		}
		switch {
		case d[i][0] == d[i-1][0]:
			op.K = OpInsert
			op.V = n[d[i][1]-1]
			ops = append(ops, op)
		case d[i][1] == d[i-1][1]:
			op.K = OpDelete
			op.V = o[d[i][0]-1]
			ops = append(ops, op)
		}
	}
	return Patch(ops)
}

// FromBytes returns a patch from a byte array representation of Patch
func FromBytes(patch []byte) (Patch, error) {
	b := bytes.NewBuffer(patch)
	var p Patch
	err := gob.NewDecoder(b).Decode(&p)
	return p, err
}

// FromString returns a patch from a string representation of Patch
func FromString(patch string) (Patch, error) {
	scanner := bufio.NewScanner(bytes.NewBufferString(patch))
	const (
		newHunk = iota
		toLine
		divider
		fromLine
	)
	var (
		expectedState = newHunk
		err           error
		h             hunk
		linesSeen     int
		ops           []Op
	)
	line := 0
	for scanner.Scan() {
		l := scanner.Text()
		switch l[0] {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			if expectedState != newHunk {
				return nil, unexpectedToken(l[0], line)
			}
			h, err = parseHunk(l)
			if err != nil {
				return nil, fmt.Errorf("could not parse patch: %v", err)
			}
			linesSeen = 0
			switch h.kind {
			case OpDelete, OpReplace:
				expectedState = toLine
			case OpInsert:
				expectedState = fromLine
			}
		case '<':
			if expectedState != toLine {
				return nil, unexpectedToken(l[0], line)
			}
			op := Op{
				K: OpDelete,
				F: h.fromStart - 1,
				T: h.toStart + linesSeen - 1,
				V: getLineValue(l),
			}
			if h.kind == OpReplace {
				op.F--
			}
			ops = append(ops, op)
			linesSeen++
			if h.toEnd < h.toStart+linesSeen {
				if h.kind == OpReplace {
					expectedState = divider
				} else {
					expectedState = newHunk
				}
			}
		case '-':
			if expectedState != divider {
				return nil, unexpectedToken(l[0], line)
			}
			expectedState = fromLine
			linesSeen = 0
		case '>':
			if expectedState != fromLine {
				return nil, unexpectedToken(l[0], line)
			}
			op := Op{
				K: OpInsert,
				F: h.fromStart + linesSeen - 1,
				T: h.toStart - 1,
				V: getLineValue(l),
			}
			if h.kind == OpReplace && h.toEnd != 0 {
				op.T = h.toEnd - 1
			}
			ops = append(ops, op)
			linesSeen++
			if h.fromEnd < h.fromStart+linesSeen {
				expectedState = newHunk
			}
		}
		line++
	}
	return Patch(ops), nil
}

func getLineValue(line string) string {
	if len(line) <= 2 {
		return ""
	}
	return line[2:]
}

func unexpectedToken(t byte, line int) error {
	return fmt.Errorf("could not parse patch: unexpected token: %s at line %d",
		string(t), line)
}

var hunkRE = regexp.MustCompile(`(\d+)(?:,(\d+))?([acd])(\d+)(?:,(\d+))?`)

func parseHunk(s string) (hunk, error) {
	m := hunkRE.FindStringSubmatch(s)
	if len(m) != 6 {
		return hunk{}, fmt.Errorf("could not parse hunk")
	}
	var (
		h   hunk
		i   int
		err error
	)
	// set toStart
	i, err = parseInt(m[1])
	if err != nil {
		return hunk{}, fmt.Errorf("could not parse hunk: %v", err)
	}
	h.toStart = i
	// set toEnd
	i, err = parseInt(m[2])
	if err != nil {
		return hunk{}, fmt.Errorf("could not parse hunk: %v", err)
	}
	h.toEnd = i
	// set kind
	switch m[3] {
	case "a":
		h.kind = OpInsert
	case "c":
		h.kind = OpReplace
	case "d":
		h.kind = OpDelete
	default:
		return hunk{}, fmt.Errorf("could not parse hunk: %s",
			"invalid operation kind")
	}
	// set fromStart
	i, err = parseInt(m[4])
	if err != nil {
		return hunk{}, fmt.Errorf("could not parse hunk: %v", err)
	}
	h.fromStart = i
	// set fromEnd
	i, err = parseInt(m[5])
	if err != nil {
		return hunk{}, fmt.Errorf("could not parse hunk: %v", err)
	}
	h.fromEnd = i
	return h, nil
}

func parseInt(s string) (int, error) {
	if len(s) == 0 {
		return 0, nil
	}
	return strconv.Atoi(s)
}

// Bytes returns a portable byte array representation of Patch
func (p Patch) Bytes() []byte {
	var b bytes.Buffer
	_ = gob.NewEncoder(&b).Encode(p)
	return b.Bytes()
}

// String returns a portable string representation of Patch
func (p Patch) String() string {
	var s string
	lastOp := Op{
		K: OpNoChange,
	}
	var h hunk
	for _, op := range p {
		if isNewHunk(op, lastOp) {
			if lastOp.K != OpNoChange {
				s += hunkToString(h)
			}
			h = hunk{
				kind:      op.K,
				toStart:   op.T,
				toEnd:     op.T,
				fromStart: op.F,
				fromEnd:   op.F,
			}
		}
		if op.K == OpInsert && lastOp.K == OpDelete && op.T == lastOp.T {
			h.fromStart = op.F
			h.kind = OpReplace
		}
		switch op.K {
		case OpDelete:
			h.toEnd = op.T
			h.toLines = append(h.toLines, op.V)
		case OpInsert:
			h.fromEnd = op.F
			h.fromLines = append(h.fromLines, op.V)
		}
		lastOp = op
	}
	if h.kind != OpNoChange {
		s += hunkToString(h)
	}
	return s
}

type hunk struct {
	kind      OpKind
	toStart   int
	toEnd     int
	toLines   []string
	fromStart int
	fromEnd   int
	fromLines []string
}

func hunkToString(h hunk) string {
	var s string
	if h.toStart == h.toEnd {
		s = fmt.Sprintf("%d", h.toStart+1)
	} else {
		s = fmt.Sprintf("%d,%d", h.toStart+1, h.toEnd+1)
	}
	switch h.kind {
	case OpDelete:
		s += "d"
	case OpInsert:
		s += "a"
	case OpReplace:
		s += "c"
	}
	if h.fromStart == h.fromEnd {
		s += fmt.Sprintf("%d\n", h.fromStart+1)
	} else {
		s += fmt.Sprintf("%d,%d\n", h.fromStart+1, h.fromEnd+1)
	}
	for _, l := range h.toLines {
		s += "< " + l + "\n"
	}
	if h.kind == OpReplace {
		s += "---\n"
	}
	for _, l := range h.fromLines {
		s += "> " + l + "\n"
	}
	return s
}

func isNewHunk(op, lastOp Op) bool {
	if op.K == OpDelete {
		if lastOp.K == OpNoChange || lastOp.K == OpInsert ||
			lastOp.K == OpDelete && lastOp.F != op.F {
			return true
		}
	}
	if op.K == OpInsert {
		if lastOp.K == OpNoChange || lastOp.K == OpDelete && lastOp.T != op.T ||
			lastOp.K == OpInsert && lastOp.T != op.T {
			return true
		}
	}
	return false
}

// Apply applies patch to original
func (p Patch) Apply(original string) string {
	o := split(original, "\n")
	n := make([]string, 0)
	// original cursor
	x := 0
	// patch cursor
	i := 0
	for x < len(o) || i < len(p) {
		switch {
		case i >= len(p) && x < len(o) ||
			p[i].K == OpInsert && len(n) < p[i].F ||
			p[i].K == OpDelete && x < p[i].T:
			n = append(n, o[x])
			x++
		case p[i].K == OpInsert:
			n = append(n, p[i].V)
			i++
		case p[i].K == OpDelete:
			x++
			i++
		}
	}
	return strings.Join(n, "\n")
}

// Remove removes patch from new
func (p Patch) Remove(new string) string {
	n := split(new, "\n")
	o := make([]string, 0)
	// new cursor
	y := 0
	// patch cursor
	i := 0
	for y < len(n) || i < len(p) {
		switch {
		case i >= len(p) && y < len(n) ||
			p[i].K == OpDelete && len(o) < p[i].T ||
			p[i].K == OpInsert && y < p[i].F:
			o = append(o, n[y])
			y++
		case p[i].K == OpDelete:
			o = append(o, p[i].V)
			i++
		case p[i].K == OpInsert:
			y++
			i++
		}
	}
	return strings.Join(o, "\n")
}

func paths(original, new []string) [][]int {
	n := len(original)
	m := len(new)
	v := make([]int, 2*(n+m)+1)
	v0 := len(v) / 2
	vd := make([][]int, 0)
	found := false
	for d := 0; d <= n+m && !found; d++ {
		for k := -d; k <= d && !found; k += 2 {
			var x int
			if k == -d || k != d && v[k-1+v0] < v[k+1+v0] {
				x = v[k+1+v0]
			} else {
				x = v[k-1+v0] + 1
			}
			y := x - k
			for {
				if x < n && y < m && original[x] == new[y] {
					x++
					y++
				} else {
					break
				}
			}
			v[k+v0] = x
			if x == n && y == m {
				found = true
			}
		}
		a := make([]int, len(v))
		copy(a, v)
		vd = append(vd, a)
	}
	return vd
}

func recoverPath(vd [][]int, x, y int) [][2]int {
	v0 := (2*(x+y) + 1) / 2
	p := make([][2]int, 0)
	for d := len(vd) - 1; d >= 0; d-- {
		v := vd[d]
		k := x - y
		var pk int
		if k == -d || k != d && v[k-1+v0] < v[k+1+v0] {
			pk = k + 1
		} else {
			pk = k - 1
		}
		px := v[pk+v0]
		py := px - pk
		for {
			if x > px && y > py {
				p = append(p, [2]int{x, y})
				x--
				y--
			} else {
				break
			}
		}
		p = append(p, [2]int{x, y})
		x = px
		y = py
	}
	return reverse(p)
}

func reverse(a [][2]int) [][2]int {
	for i := 0; i < len(a)/2; i++ {
		e := len(a) - 1 - i
		t := a[e]
		a[e] = a[i]
		a[i] = t
	}
	return a
}

// split splits s by sep; if s is empty, returns nil
func split(s, sep string) []string {
	if s == "" {
		return nil
	}
	return strings.Split(s, sep)
}
