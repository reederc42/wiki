package patch

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// readFileString panics on errors
func readFileString(name string) string {
	b, err := os.ReadFile(name)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func dataFilename(name string) string {
	return "testdata/" + name + ".txt"
}

var (
	lao    = readFileString(dataFilename("lao"))
	tzu    = readFileString(dataFilename("tzu"))
	myersA = readFileString(dataFilename("myersA"))
	myersB = readFileString(dataFilename("myersB"))
	// midA     = readFileString(dataFilename("midA")) //currently unused; kept because test files are included
	// midB     = readFileString(dataFilename("midB")) //currently unused; kept because test files are included
	replaceA = readFileString(dataFilename("replaceA"))
	replaceB = readFileString(dataFilename("replaceB"))
)

func TestPath(t *testing.T) {
	tests := []struct {
		Name         string
		Original     string
		New          string
		ExpectedPath [][2]int
	}{
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
			ExpectedPath: [][2]int{
				{0, 0},
				{1, 0},
				{2, 0},
				{3, 1},
				{3, 2},
				{4, 3},
				{5, 4},
				{6, 4},
				{7, 5},
				{7, 6},
			},
		},
		{
			Name:     "lao to tzu",
			Original: lao,
			New:      tzu,
			ExpectedPath: [][2]int{
				{0, 0},
				{1, 0},
				{2, 0},
				{3, 1},
				{4, 1},
				{4, 2},
				{4, 3},
				{5, 4},
				{6, 5},
				{7, 6},
				{8, 7},
				{9, 8},
				{10, 9},
				{11, 10},
				{11, 11},
				{11, 12},
				{11, 13},
			},
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
			ExpectedPath: [][2]int{
				{0, 0},
				{0, 1},
				{0, 2},
				{0, 3},
				{0, 4},
				{0, 5},
				{0, 6},
			},
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
			ExpectedPath: [][2]int{
				{0, 0},
				{1, 0},
				{2, 0},
				{3, 0},
				{4, 0},
				{5, 0},
				{6, 0},
				{7, 0},
			},
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			o := split(tests[i].Original, "\n")
			n := split(tests[i].New, "\n")
			vd := paths(o, n)
			p := recoverPath(vd, len(o), len(n))
			assert.Equal(t, tests[i].ExpectedPath, p)
		})
	}
}

func TestFind(t *testing.T) {
	tests := []struct {
		Name          string
		Original      string
		New           string
		ExpectedPatch Patch
	}{
		{
			Name:     "lao to tzu",
			Original: lao,
			New:      tzu,
			ExpectedPatch: Patch{
				{
					K: OpDelete,
					F: -1,
					T: 0,
					V: "The Way that can be told of is not the eternal Way;",
				},
				{
					K: OpDelete,
					F: -1,
					T: 1,
					V: "The name that can be named is not the eternal name.",
				},
				{
					K: OpDelete,
					F: 0,
					T: 3,
					V: "The Named is the mother of all things.",
				},
				{
					K: OpInsert,
					F: 1,
					T: 3,
					V: "The named is the mother of all things.",
				},
				{
					K: OpInsert,
					F: 2,
					T: 3,
					V: "",
				},
				{
					K: OpInsert,
					F: 10,
					T: 10,
					V: "They both may be called deep and profound.",
				},
				{
					K: OpInsert,
					F: 11,
					T: 10,
					V: "Deeper and more profound,",
				},
				{
					K: OpInsert,
					F: 12,
					T: 10,
					V: "The door of all subtleties!",
				},
			},
		},
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
			ExpectedPatch: Patch{
				{
					K: OpDelete,
					F: -1,
					T: 0,
					V: "A",
				},
				{
					K: OpDelete,
					F: -1,
					T: 1,
					V: "B",
				},
				{
					K: OpInsert,
					F: 1,
					T: 2,
					V: "B",
				},
				{
					K: OpDelete,
					F: 3,
					T: 5,
					V: "B",
				},
				{
					K: OpInsert,
					F: 5,
					T: 6,
					V: "C",
				},
			},
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
			ExpectedPatch: Patch{
				{
					K: OpInsert,
					F: 0,
					T: -1,
					V: "C",
				},
				{
					K: OpInsert,
					F: 1,
					T: -1,
					V: "B",
				},
				{
					K: OpInsert,
					F: 2,
					T: -1,
					V: "A",
				},
				{
					K: OpInsert,
					F: 3,
					T: -1,
					V: "B",
				},
				{
					K: OpInsert,
					F: 4,
					T: -1,
					V: "A",
				},
				{
					K: OpInsert,
					F: 5,
					T: -1,
					V: "C",
				},
			},
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
			ExpectedPatch: Patch{
				{
					K: OpDelete,
					F: -1,
					T: 0,
					V: "A",
				},
				{
					K: OpDelete,
					F: -1,
					T: 1,
					V: "B",
				},
				{
					K: OpDelete,
					F: -1,
					T: 2,
					V: "C",
				},
				{
					K: OpDelete,
					F: -1,
					T: 3,
					V: "A",
				},
				{
					K: OpDelete,
					F: -1,
					T: 4,
					V: "B",
				},
				{
					K: OpDelete,
					F: -1,
					T: 5,
					V: "B",
				},
				{
					K: OpDelete,
					F: -1,
					T: 6,
					V: "A",
				},
			},
		},
		{
			Name:     "replace",
			Original: replaceA,
			New:      replaceB,
			ExpectedPatch: Patch{
				{
					K: OpDelete,
					F: 1,
					T: 2,
					V: "G",
				},
				{
					K: OpDelete,
					F: 1,
					T: 3,
					V: "H",
				},
				{
					K: OpDelete,
					F: 1,
					T: 4,
					V: "I",
				},
				{
					K: OpInsert,
					F: 2,
					T: 4,
					V: "J",
				},
				{
					K: OpInsert,
					F: 3,
					T: 4,
					V: "K",
				},
				{
					K: OpInsert,
					F: 4,
					T: 4,
					V: "L",
				},
			},
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p := Find(tests[i].Original, tests[i].New)
			assert.Equal(t, tests[i].ExpectedPatch, p)
		})
	}
}

func TestToFromBytes(t *testing.T) {
	tests := []struct {
		Name     string
		Original string
		New      string
	}{
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
		},
		{
			Name:     "lao to tzu",
			Original: lao,
			New:      tzu,
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p0 := Find(tests[i].Original, tests[i].New)
			p1, err := FromBytes(p0.Bytes())
			assert.NoError(t, err)
			assert.Equal(t, p0, p1)
		})
	}
}

func TestToFromString(t *testing.T) {
	tests := []struct {
		Name     string
		Original string
		New      string
	}{
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
		},
		{
			Name:     "lao to tzu",
			Original: lao,
			New:      tzu,
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p0 := Find(tests[i].Original, tests[i].New)
			p1, err := FromString(p0.String())
			assert.NoError(t, err)
			assert.Equal(t, p0, p1)
		})
	}
}

func TestString(t *testing.T) {
	tests := []struct {
		Name     string
		Patch    Patch
		Expected string
	}{
		{
			Name:  "lao to tzu",
			Patch: Find(lao, tzu),
			Expected: `1,2d0
< The Way that can be told of is not the eternal Way;
< The name that can be named is not the eternal name.
4c2,3
< The Named is the mother of all things.
---
> The named is the mother of all things.
> 
11a11,13
> They both may be called deep and profound.
> Deeper and more profound,
> The door of all subtleties!
`,
		},
		{
			Name:  "replace",
			Patch: Find(replaceA, replaceB),
			Expected: `3,5c3,5
< G
< H
< I
---
> J
> K
> L
`,
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			assert.Equal(t, tests[i].Expected, tests[i].Patch.String())
		})
	}
}

func TestFromString(t *testing.T) {
	tests := []struct {
		Name     string
		Patch    string
		Expected Patch
	}{
		{
			Name: "lao to tzu",
			Patch: `1,2d0
< The Way that can be told of is not the eternal Way;
< The name that can be named is not the eternal name.
4c2,3
< The Named is the mother of all things.
---
> The named is the mother of all things.
>
11a11,13
> They both may be called deep and profound.
> Deeper and more profound,
> The door of all subtleties!`,
			Expected: Patch{
				{
					K: OpDelete,
					F: -1,
					T: 0,
					V: "The Way that can be told of is not the eternal Way;",
				},
				{
					K: OpDelete,
					F: -1,
					T: 1,
					V: "The name that can be named is not the eternal name.",
				},
				{
					K: OpDelete,
					F: 0,
					T: 3,
					V: "The Named is the mother of all things.",
				},
				{
					K: OpInsert,
					F: 1,
					T: 3,
					V: "The named is the mother of all things.",
				},
				{
					K: OpInsert,
					F: 2,
					T: 3,
					V: "",
				},
				{
					K: OpInsert,
					F: 10,
					T: 10,
					V: "They both may be called deep and profound.",
				},
				{
					K: OpInsert,
					F: 11,
					T: 10,
					V: "Deeper and more profound,",
				},
				{
					K: OpInsert,
					F: 12,
					T: 10,
					V: "The door of all subtleties!",
				},
			},
		},
		{
			Name: "replace",
			Patch: `3,5c3,5
< G
< H
< I
---
> J
> K
> L
`,
			Expected: Patch{
				{
					K: OpDelete,
					F: 1,
					T: 2,
					V: "G",
				},
				{
					K: OpDelete,
					F: 1,
					T: 3,
					V: "H",
				},
				{
					K: OpDelete,
					F: 1,
					T: 4,
					V: "I",
				},
				{
					K: OpInsert,
					F: 2,
					T: 4,
					V: "J",
				},
				{
					K: OpInsert,
					F: 3,
					T: 4,
					V: "K",
				},
				{
					K: OpInsert,
					F: 4,
					T: 4,
					V: "L",
				},
			},
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p, err := FromString(tests[i].Patch)
			assert.NoError(t, err)
			assert.Equal(t, tests[i].Expected, p)
		})
	}
}

func TestApply(t *testing.T) {
	tests := []struct {
		Name     string
		Original string
		New      string
	}{
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
		},
		{
			Name:     "lao to tzu",
			Original: lao,
			New:      tzu,
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
		},
		{
			Name:     "replace",
			Original: replaceA,
			New:      replaceB,
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p := Find(tests[i].Original, tests[i].New)
			n := p.Apply(tests[i].Original)
			assert.Equal(t, tests[i].New, n)
		})
	}
}

func TestRemove(t *testing.T) {
	tests := []struct {
		Name     string
		Original string
		New      string
	}{
		{
			Name:     "myers strings",
			Original: myersA,
			New:      myersB,
		},
		{
			Name:     "tzu to lao",
			Original: lao,
			New:      tzu,
		},
		{
			Name:     "original is empty",
			Original: "",
			New:      myersB,
		},
		{
			Name:     "new is empty",
			Original: myersA,
			New:      "",
		},
		{
			Name:     "replace",
			Original: replaceA,
			New:      replaceB,
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			p := Find(tests[i].Original, tests[i].New)
			o := p.Remove(tests[i].New)
			assert.Equal(t, tests[i].Original, o)
		})
	}
}

func TestParseHunk(t *testing.T) {
	tests := []struct {
		Name     string
		Str      string
		Expected hunk
		Error    error
	}{
		{
			Name: "delete",
			Str:  "1,2d0",
			Expected: hunk{
				kind:      OpDelete,
				toStart:   1,
				toEnd:     2,
				fromStart: 0,
				fromEnd:   0,
			},
		},
		{
			Name: "replace 1 to",
			Str:  "4c2,3",
			Expected: hunk{
				kind:      OpReplace,
				toStart:   4,
				toEnd:     0,
				fromStart: 2,
				fromEnd:   3,
			},
		},
		{
			Name: "replace 1 from",
			Str:  "2,3c4",
			Expected: hunk{
				kind:      OpReplace,
				toStart:   2,
				toEnd:     3,
				fromStart: 4,
				fromEnd:   0,
			},
		},
		{
			Name: "replace",
			Str:  "2,3c4,5",
			Expected: hunk{
				kind:      OpReplace,
				toStart:   2,
				toEnd:     3,
				fromStart: 4,
				fromEnd:   5,
			},
		},
		{
			Name: "insert",
			Str:  "11a11,13",
			Expected: hunk{
				kind:      OpInsert,
				toStart:   11,
				toEnd:     0,
				fromStart: 11,
				fromEnd:   13,
			},
		},
		{
			Name:  "invalid",
			Str:   "11z11,13",
			Error: errors.New("could not parse hunk"),
		},
	}
	assert.Greater(t, len(tests), 0, "no tests defined")
	for i := range tests {
		t.Run(tests[i].Name, func(t *testing.T) {
			h, err := parseHunk(tests[i].Str)
			assert.Equal(t, tests[i].Error, err)
			assert.Equal(t, tests[i].Expected, h)
		})
	}
}
