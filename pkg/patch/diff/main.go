package main

import (
	"fmt"
	"os"

	"gitlab.com/reederc42/wiki/pkg/patch"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "requires exactly 2 arguments, %d provided",
			len(os.Args))
	}
	from, err := readFileString(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "error reading file: %s\n", err)
	}
	to, err := readFileString(os.Args[2])
	if err != nil {
		fmt.Fprintf(os.Stderr, "error reading file: %s\n", err)
	}
	fmt.Print(patch.Find(from, to).String())
}

func readFileString(name string) (string, error) {
	b, err := os.ReadFile(name)
	return string(b), err
}
