# Config V2

Config v2 intends to solve the problem of structured and self-documenting, but still independent, configuration. Config v2 will provide a way of defining arbitrarily complex structures at run time, and when asked can produce documentation of all configuration options. A reasonable stretch goal is to include secret handling, giving the user the option to redact, show, or hide secret configuration values such as passwords.

Like other config packages, there will be an optional package-level, or global, config object, that is implemented by a concrete, exported type. This allows the user to use instances of config.

Config v2 is intended to solve problems found in the wiki codebase, where configuration is normalized only by convention. Packages should be independent and should only import other packages needed for their limited functionality. Naturally, this leads to a super-config where every possible package and config is imported. This means that binary (in this case, tools) must import every configured package, even if it will never use it.

Hopefully this package will allow wiki to have a single main.go, and use build tags to specify which components will be included. These components will provide their own configuration which will be mixed into the total config.

## Interface

### Add

Add a key at an arbitrary path. The key holds all information needed to document itself, such as a description and default value(s). Add is not thread-safe, and will panic if there is an existing key at path.

### Get

Gets the value of the key at path. Config may provide specific Get(Type) getters, but the user is expected to verify the returned key is the correct type. Returns nil if there is no key at that path.

### Write

Writes annotated YAML with provided writer. In the future, if secrets are supported, will provide levels of disclosure so that secrets won't be written accidentally.

### Collect

Analogous to the flag package's Parse, collects all config sources and sets the configuration. Collect is not thread-safe, but after calling Collect, subsequent calls to Get are thread-safe.

### BindPFlag

pflags may be bound to a path. If the flag is changed, it's value is set for the key. Flags from the default flag package cannot be used because they don't have a way of detecting if the user set them, which would cause collision with the default values. I'm leaning toward using the flag default value; in that case, the user could know they're going to bind a flag to a path, and so they would be allowed to leave the Key default nil.

## Limitations

This package is similar to a structured version of viper. However, it is not intended to provide access to struct fields via Get; the user is expected to Get the configured struct and access it directly. Moreover, config is not intended to make configuration discoverable; individual components should define and get only their own configuration.

Collect is not thread-safe, and must be called before any concurrent access via Get. Add is also not thread-safe, and all Adds must be performed before Collect; subsequent calls to Add will be ignored (although since Add and Collect aren't thread-safe there could be a data race if they are used concurrently). Get is thread-safe because it only reads the config state.

## Implementation Notes

It is possible to have structs inside an interface{}, but the yaml package doesn't understand it -- it appears the yaml package only looks at the top-level structure and doesn't get picky inside it.

The first part of this implementation is to extend the yaml package to intelligently override supplied structs (or other mapping values) if they are hidden behind interface{}

TBD if this includes mapping nodes inside sequences

### Pseudocode

Unmarshal:
  If target is not struct or map:
    node.Decode(target.Interface()) // may return type error
    exit

  Else if node is mapping:
    For each pair of node contents:
      key = contents[0].Value
      value = contents[1]

      If target is struct:
        field = target.FieldByName(key)
      Else if target is map:
        field = target.MapIndex(key)

      If field is not zero Value:
        If field.CanAddr():
          Goto Unmarshal with target = field.Addr() and node = value
        Else:
          Goto Unmarshal with target = field and node = value

        If field.Kind() is not Pointer:
          If not field.CanAddr():
            Return type error

          field = field.Addr()

        Goto Unmarshal with target = field and node = value

  Else:
    Return type error

* Value of unmarshaler data must be pointer

* Elements of mapping contexts must be addressable

## Testing Conclusions

1. Should probably be thinking of things in terms of settability

1. Maps are always addressable

1. Map keys must be pointers to be addressable

1. Standard decode into a map with pointer keys will always create new pointers

### More pseudocode

1. Unwind to get real value
1. If nil, return nil error
1. 