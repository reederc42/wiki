package config

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

func TestToStringSlice(t *testing.T) {
	tests := []struct {
		name     string
		path     interface{}
		expected []string
	}{
		{
			name:     "3 string path",
			path:     []string{"b", "a", "c"},
			expected: []string{"b", "a", "c"},
		},
		{
			name:     "number path",
			path:     5,
			expected: nil,
		},
		{
			name:     "string",
			path:     "asdf",
			expected: []string{"asdf"},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := toStringSlice(test.path)
			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestGetKeyByPath(t *testing.T) {
	tests := []struct {
		name     string
		m        map[string]interface{}
		path     []string
		expected interface{}
	}{
		{
			name: "2 path",
			m: map[string]interface{}{
				"foo": map[string]interface{}{
					"bar": "baz",
				},
			},
			path:     []string{"foo", "bar"},
			expected: "baz",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := getKeyByPath(test.m, test.path)
			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestAddDocAtPath(t *testing.T) {
	tests := []struct {
		name     string
		expected structdoc.Doc
		paths    [][]string
		docs     []structdoc.Doc
	}{
		{
			name: "single key",
			expected: structdoc.Doc{
				Items: map[string]structdoc.Doc{
					"key1": {
						Doc: "key1 doc",
					},
				},
			},
			paths: [][]string{
				{"key1"},
			},
			docs: []structdoc.Doc{
				{Doc: "key1 doc"},
			},
		},
		{
			name: "two keys",
			expected: structdoc.Doc{
				Items: map[string]structdoc.Doc{
					"key1": {
						Doc: "key1 doc",
						Items: map[string]structdoc.Doc{
							"key2": {
								Doc: "key2 doc",
							},
						},
					},
				},
			},
			paths: [][]string{
				{"key1"},
				{"key1", "key2"},
			},
			docs: []structdoc.Doc{
				{Doc: "key1 doc"},
				{Doc: "key2 doc"},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := structdoc.Doc{}
			for i := range test.paths {
				err := addDocAtPath(test.paths[i], &actual, test.docs[i])
				assert.NoError(t, err)
			}
			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestAddDocAtPath_DuplicateKeyError(t *testing.T) {
	paths := [][]string{
		{"key1", "key2"},
		{"key1"},
	}
	docs := []structdoc.Doc{
		{Doc: "key2 doc"},
		{Doc: "key1 doc"},
	}
	errors := make([]error, 2)
	base := structdoc.Doc{}
	for i := range paths {
		err := addDocAtPath(paths[i], &base, docs[i])
		errors = append(errors, err)
	}
	assert.Contains(t, errors, fmt.Errorf("duplicate key: key1"))
}

type testEncoder struct{}

func (te *testEncoder) Marshal(in interface{}, doc structdoc.Doc) ([]byte, error) {
	return nil, nil
}

func (te *testEncoder) Unmarshal(in []byte, out interface{}) error {
	return nil
}

func TestCollectAndWrite(t *testing.T) {
	key1 := true
	key2 := struct {
		Key21 string
		Key22 int
	}{
		"value1",
		2,
	}
	cfg := Config{
		EnvPrefix: "test",
		Encoder:   &testEncoder{},
	}
	cfg.Add(Key{
		Path:    "key1",
		Default: &key1,
	})
	cfg.Add(Key{
		Path:    "key2",
		Default: &key2,
		Doc: structdoc.Doc{
			Items: map[string]structdoc.Doc{
				"Key21": {},
				"Key22": {},
			},
		},
	})
	err := cfg.Collect()
	assert.NoError(t, err)

	var b bytes.Buffer
	err = cfg.Write(&b)
	assert.NoError(t, err)
}
