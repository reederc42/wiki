// Package config implements structured, self-documenting, and independent configuration
package config

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/pkg/unwind"
)

// EnvTag use this tag to override calculated environment variable name
const EnvTag = "env"

type Key struct {
	// Path contains key and parents; must either be single, non-empty string,
	// or string slice
	Path interface{}
	// Default contains the default value(s) of the key. Also used to determine
	// type, so it may not be nil
	Default interface{}
	// Doc holds documentation for the key. May be multiline. If Doc is not set,
	// config will pull struct documentation from structdoc
	Doc structdoc.Doc

	// Secret indicates if the key is secret and will not be written by default
	// (TODO)
	// Secret bool
}

// Config holds configuration data from provided sources
type Config struct {
	// Files list of configuration files, merged first to last, meaning later
	// files take precedence over earlier files
	Files []string
	// EnvPrefix is prepended to environment variable names generated from Keys
	EnvPrefix string
	// Encoder is the source to collect config from
	Encoder Encoder

	keys      []Key
	data      map[string]interface{}
	collected bool
}

// Add adds key k to config. Always succeeds, any errors will be returned by
// Collect.
func (c *Config) Add(k Key) {
	if c.collected {
		panic("cannot add configuration keys after collecting configuration")
	}

	c.keys = append(c.keys, k)
}

// Get returns key at path if it exists. Returns nil if there is no key, or if
// Path is not a string or string slice.
func (c *Config) Get(path interface{}) interface{} {
	if !c.collected {
		panic("cannot get configuration keys before collecting configuration")
	}

	p := toStringSlice(path)
	if len(p) == 0 {
		return nil
	}
	return getKeyByPath(c.data, p)
}

// Map returns config data as map
func (c Config) Map() map[string]interface{} {
	return c.data
}

// getKeyByPath returns any key or nil at path in m. Any error state returns
// nil, such as a path that refers to a struct member
func getKeyByPath(m map[string]interface{}, path []string) interface{} {
	if len(path) == 0 || len(m) == 0 {
		return nil
	}
	n := m
	for i, p := range path {
		v := n[p]
		if v == nil {
			return nil
		}
		switch x := v.(type) {
		case map[string]interface{}:
			n = x
		case interface{}:
			if i == len(path)-1 {
				return x
			}
			return nil
		}
	}
	return nil
}

// toStringSlice ensures v is a string or string slice. Returns nil if path
// cannot be made into a string slice.
func toStringSlice(v interface{}) []string {
	switch x := v.(type) {
	case string:
		return []string{x}
	case []string:
		return x
	}
	return nil
}

// Collect collects configuration data and returns any errors
func (c *Config) Collect() error {
	c.collected = true
	sortKeysByPathLength(c.keys)
	m, err := convertKeyListToMap(c.keys)
	if err != nil {
		return err
	}
	c.data = m
	for _, f := range c.Files {
		if err := readFileWithEncoder(&c.data, f, c.Encoder); err != nil {
			return err
		}
	}
	if err := readEnv(reflect.ValueOf(&c.data),
		[]string{c.EnvPrefix}); err != nil {
		return err
	}
	return nil
}

// Write writes configuration to writer using configured source
func (c *Config) Write(w io.Writer) error {
	if c.Encoder == nil {
		return fmt.Errorf("cannot write with nil encoder")
	}
	doc, err := makeDocFromKeys(c.keys)
	if err != nil {
		return err
	}
	var parents []string
	if c.EnvPrefix != "" {
		parents = []string{c.EnvPrefix}
	}
	doc = addEnvToDoc(reflect.ValueOf(c.data), doc, parents)
	b, err := c.Encoder.Marshal(c.data, doc)
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

var global *Config

// SetFiles sets global config file sources
func SetFiles(files []string) {
	if global == nil {
		global = &Config{}
	}
	global.Files = files
}

// SetEnvPrefix sets global config environment variable prefix
func SetEnvPrefix(prefix string) {
	if global == nil {
		global = &Config{}
	}
	global.EnvPrefix = prefix
}

// SetEncoder sets global config encoder
func SetEncoder(encoder Encoder) {
	if global == nil {
		global = &Config{}
	}
	global.Encoder = encoder
}

// Add adds key k to config. Always succeeds, any errors will be returned by
// Collect.
func Add(k Key) {
	if global == nil {
		global = &Config{}
	}
	global.Add(k)
}

// Get returns key at path if it exists. Returns nil if there is no key.
func Get(path interface{}) interface{} {
	if global == nil {
		return nil
	}
	return global.Get(path)
}

// Collect collects configuration data and returns any errors
func Collect() error {
	if global == nil {
		return nil
	}
	return global.Collect()
}

// Write writes configuration to writer using configured source
func Write(w io.Writer) error {
	if global == nil {
		return nil
	}
	return global.Write(w)
}

// Map returns global config data as map
func Map() map[string]interface{} {
	if global == nil {
		return nil
	}
	return global.data
}

// Encoder is the interface for external providers
type Encoder interface {
	Marshal(in interface{}, doc structdoc.Doc) ([]byte, error)
	Unmarshal(in []byte, out interface{}) error
}

func sortKeysByPathLength(keys []Key) {
	sort.Slice(
		keys,
		func(i, j int) bool {
			return len(
				toStringSlice(
					keys[i].Path,
				),
			) < len(
				toStringSlice(
					keys[j].Path,
				),
			)
		},
	)
}

// convertKeyListToMap converts keys provided in add to map. Returns error if
// there are duplicate keys. Pre: keys should be sorted by path length
func convertKeyListToMap(keys []Key) (map[string]interface{}, error) {
	keyMap := make(map[string]interface{})
	for _, k := range keys {
		path := toStringSlice(k.Path)
		if len(path) < 1 {
			return nil, fmt.Errorf("key path is not string or slice")
		}
		parentPath := path[:len(path)-1]
		key := path[len(path)-1]
		// add default to data
		parent, err := makePathParents(parentPath, keyMap)
		if err != nil {
			return nil, err
		}
		if _, ok := parent[key]; ok {
			return nil, fmt.Errorf("duplicate key: %s", key)
		}
		parent[key] = k.Default
	}
	return keyMap, nil
}

func makeDocFromKeys(keys []Key) (structdoc.Doc, error) {
	doc := structdoc.Doc{}
	for _, k := range keys {
		path := toStringSlice(k.Path)
		if len(path) < 1 {
			return structdoc.Doc{}, fmt.Errorf("key path is not string or slice")
		}
		// add key doc to doc; if doc is empty, pulls from global doc
		d := k.Doc
		// if key is a struct, merge any structdocs
		if len(d.Items) == 0 &&
			unwind.Unwind(reflect.ValueOf(k.Default)).Kind() == reflect.Struct {
			sd, _ := structdoc.Get(k.Default)
			if d.Doc == "" {
				d.Doc = sd.Doc
			}
			d.Items = sd.Items
		}
		if err := addDocAtPath(path, &doc, d); err != nil {
			return structdoc.Doc{}, err
		}
	}
	return doc, nil
}

// makePathParents creates parents in path and returns parent map. Returns
// duplicate key error if a parent already exists and is not a map. Assumes path
// does not contain leaf key
func makePathParents(path []string,
	base map[string]interface{},
) (map[string]interface{}, error) {
	m := base
	for _, i := range path {
		e, ok := m[i]
		if !ok {
			// key does not exist, create new map and continue
			tm := make(map[string]interface{})
			m[i] = tm
			m = tm
			continue
		}
		// key exists and is map
		if tm, ok := e.(map[string]interface{}); ok {
			m = tm
			continue
		}
		// key exists and is not map
		return nil, fmt.Errorf("duplicate key: %s", i)
	}
	return m, nil
}

// makeDocParents creates doc parents in path and returns parent items
func makeDocParents(path []string,
	base *structdoc.Doc,
) map[string]structdoc.Doc {
	if base.Items == nil {
		base.Items = make(map[string]structdoc.Doc)
	}
	t := base.Items
	for _, p := range path {
		if _, ok := t[p]; ok {
			if t[p].Items == nil {
				t[p] = structdoc.Doc{
					Doc:   t[p].Doc,
					Items: make(map[string]structdoc.Doc),
				}
			}
		} else {
			t[p] = structdoc.Doc{
				Items: make(map[string]structdoc.Doc),
			}
		}
		t = t[p].Items
	}
	return t
}

// addDocAtPath adds doc to base at path; base cannot be nil. Returns duplicate
// key error if the leaf key already exists and is set
func addDocAtPath(path []string, base *structdoc.Doc, doc structdoc.Doc) error {
	parent := makeDocParents(path[:len(path)-1], base)
	key := path[len(path)-1]
	if _, ok := parent[key]; ok {
		return fmt.Errorf("duplicate key: %s", key)
	}
	parent[key] = doc
	return nil
}

func readFileWithEncoder(data *map[string]interface{}, file string,
	encoder Encoder,
) error {
	if encoder == nil {
		return fmt.Errorf("cannot read file with nil encoder")
	}
	b, err := os.ReadFile(file)
	if err != nil {
		return err
	}
	return encoder.Unmarshal(b, data)
}

func addEnvToDocFn(data reflect.Value, tag reflect.StructTag, parentDoc structdoc.Doc,
	name string, parents []string,
) error {
	env := tag.Get(EnvTag)
	if env == "" {
		env = strcase.ToScreamingSnake(strings.Join(append(parents, name), "_"))
	}
	doc := parentDoc.Items[name]
	if doc.Doc != "" {
		doc.Doc += "\n"
	}
	doc.Doc += "env: " + env
	parentDoc.Items[name] = doc
	return nil
}

func addEnvToDoc(data reflect.Value, doc structdoc.Doc,
	parents []string,
) structdoc.Doc {
	_ = walk(data, doc, "", "", parents, addEnvToDocFn)
	return doc
}

func readEnvFn(data reflect.Value, tag reflect.StructTag, _ structdoc.Doc, name string,
	parents []string,
) error {
	env := tag.Get(EnvTag)
	if env == "" {
		env = strcase.ToScreamingSnake(strings.Join(append(parents, name), "_"))
	}
	if isScannable(data) {
		if s := os.Getenv(env); s != "" {
			return scanValue(data, s)
		}
	}
	return nil
}

func readEnv(v reflect.Value, parents []string) error {
	return walk(v, structdoc.Doc{}, "", "", parents, readEnvFn)
}

func isScannable(v reflect.Value) bool {
	switch v.Interface().(type) {
	case bool,
		int,
		int8,
		int16,
		int32,
		int64,
		uint,
		uint8,
		uint16,
		uint32,
		uint64,
		float32,
		float64,
		complex64,
		complex128,
		string,
		time.Duration:
		return true
	}
	return false
}

// v must be settable
func scanValue(v reflect.Value, s string) error {
	var o reflect.Value
	switch v.Interface().(type) {
	case bool:
		b, err := strconv.ParseBool(s)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(b)
	case int:
		i, err := strconv.ParseInt(s, 0, 0)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(int(i))
	case int8:
		i, err := strconv.ParseInt(s, 0, 8)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(int8(i))
	case int16:
		i, err := strconv.ParseInt(s, 0, 16)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(int16(i))
	case int32:
		i, err := strconv.ParseInt(s, 0, 32)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(int32(i))
	case int64:
		i, err := strconv.ParseInt(s, 0, 64)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(i)
	case uint:
		u, err := strconv.ParseUint(s, 0, 0)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(uint(u))
	case uint8:
		u, err := strconv.ParseUint(s, 0, 8)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(uint8(u))
	case uint16:
		u, err := strconv.ParseUint(s, 0, 16)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(uint16(u))
	case uint32:
		u, err := strconv.ParseUint(s, 0, 32)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(uint32(u))
	case uint64:
		u, err := strconv.ParseUint(s, 0, 64)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(u)
	case float32:
		f, err := strconv.ParseFloat(s, 32)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(float32(f))
	case float64:
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(f)
	case complex64:
		var c complex64
		_, err := fmt.Sscanf(s, "%g", &c)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(c)
	case complex128:
		var c complex128
		_, err := fmt.Sscanf(s, "%g", &c)
		if err != nil {
			return err
		}
		o = reflect.ValueOf(c)
	case string:
		o = reflect.ValueOf(s)
	case time.Duration:
		if t, err := time.ParseDuration(s); err == nil {
			o = reflect.ValueOf(t)
		} else {
			return err
		}
	default:
		return fmt.Errorf("unhandled kind in config: %s", v.Kind().String())
	}
	v.Set(o)
	return nil
}

type walkFn func(data reflect.Value, tag reflect.StructTag, parentDoc structdoc.Doc,
	name string, parents []string) error

// walk walk through data and execute fn on each field. Returns first error encountered.
// uses parentDoc to allow modifying docs
func walk(data reflect.Value, parentDoc structdoc.Doc, tag reflect.StructTag,
	name string, parents []string, fn walkFn,
) error {
	data = unwind.Unwind(data)
	switch data.Kind() {
	case reflect.Map:
		if name != "" {
			parents = append(parents, name)
		}
		it := data.MapRange()
		for it.Next() {
			if it.Key().Kind() != reflect.String {
				// key is not string, return error until handled
				return fmt.Errorf("key is not string")
			}
			var doc structdoc.Doc
			if parentDoc.Items != nil {
				if name == "" {
					doc = parentDoc
				} else {
					doc = parentDoc.Items[name]
				}
			}
			key := it.Key().String()
			if err := walk(it.Value(), doc, "", key, parents, fn); err != nil {
				return err
			}
		}
	case reflect.Struct:
		if name != "" {
			parents = append(parents, name)
		}
		typ := data.Type()
		for i := 0; i < typ.NumField(); i++ {
			tf := typ.Field(i)
			var doc structdoc.Doc
			if parentDoc.Items != nil {
				if name == "" {
					doc = parentDoc
				} else {
					doc = parentDoc.Items[name]
				}
			}
			if err := walk(data.Field(i), doc, tf.Tag, tf.Name, parents,
				fn); err != nil {
				return err
			}
		}
	default:
		return fn(data, tag, parentDoc, name, parents)
	}
	return nil
}
