// Package env implements gitlab.com/reederc42/pkg/config.Encoder to export environment
// variables. User can set format string for each key with an environment variable, to
// support different shells. DefaultEncoder uses "export".
package env

import (
	"bytes"
	"fmt"
	"reflect"
	"regexp"
	"sync"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/pkg/unwind"
)

var (
	// DefaultEncoder produces a string using "export" and does not unset variables
	DefaultEncoder = &Encoder{
		Format:    "export %s=%s\n",
		OmitEmpty: true,
	}
	envRE *regexp.Regexp
	once  sync.Once
)

const envREString = `env: (.*)$`

type Encoder struct {
	Format    string
	OmitEmpty bool
}

func (e *Encoder) Marshal(in interface{}, doc structdoc.Doc) ([]byte, error) {
	once.Do(func() {
		envRE = regexp.MustCompile(envREString)
	})
	var b bytes.Buffer
	e.marshal(reflect.ValueOf(in), doc, &b)
	return b.Bytes(), nil
}

func (e *Encoder) Unmarshal(_ []byte, _ interface{}) error {
	return nil
}

func (e *Encoder) marshal(v reflect.Value, doc structdoc.Doc, buf *bytes.Buffer) {
	v = unwind.Unwind(v)
	if len(doc.Items) == 0 {
		env := e.getenv(doc.Doc)
		strValue := fmt.Sprintf("%v", v.Interface())
		if !e.OmitEmpty || strValue != "" {
			buf.WriteString(fmt.Sprintf(e.Format, env, strValue))
		}
		return
	}
	for k, i := range doc.Items {
		var u reflect.Value
		if v.Kind() == reflect.Map {
			u = v.MapIndex(reflect.ValueOf(k))
		} else {
			u = v.FieldByName(k)
		}
		e.marshal(u, i, buf)
	}
}

// getenv gets environment name from doc string
func (e *Encoder) getenv(s string) string {
	m := envRE.FindStringSubmatch(s)
	if len(m) == 2 {
		return m[1]
	}
	return ""
}
