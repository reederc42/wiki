package yaml

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

const unmarshalTestInput = `
key1:
  str: I'm a string
  strlist:
  - str1
  - str2
key2:
  str: I'm a different string
  int: 1
key3:
  subkey1: 24.6
key4:
  str: Some kinda string
  anotherKey:
`

type UnmarshalTestType struct {
	Str     string
	StrList []string
	Int     int
}

func TestUnmarshal(t *testing.T) {
	a := UnmarshalTestType{
		Str: "a string",
		StrList: []string{
			"str7",
			"str8",
		},
		Int: 6,
	}
	b := UnmarshalTestType{
		Str: "b string",
		StrList: []string{
			"str9",
			"str10",
		},
		Int: 7,
	}
	c := UnmarshalTestType{}
	o := map[interface{}]interface{}{
		"key1": &a,
		"key2": &b,
		"key4": c,
	}

	err := Unmarshal([]byte(unmarshalTestInput), &o)
	assert.NoError(t, err)

	expectedA := UnmarshalTestType{
		Str: "I'm a string",
		StrList: []string{
			"str1",
			"str2",
		},
		Int: 6,
	}
	assert.Equal(t, expectedA, a)

	expectedB := UnmarshalTestType{
		Str: "I'm a different string",
		StrList: []string{
			"str9",
			"str10",
		},
		Int: 1,
	}
	assert.Equal(t, expectedB, b)

	expectedKey3 := map[string]interface{}{
		"subkey1": 24.6,
	}
	assert.Equal(t, expectedKey3, o["key3"])

	assert.NotEqual(t, c, o["key4"])
}

type Addressable struct {
	Field          AddressableField
	InterfaceField interface{}
	Err            error
	Ptr            *int
}

type AddressableField struct {
	Number int
}

func TestAddressable_Pointer(t *testing.T) {
	var i interface{} = &Addressable{Field: AddressableField{5}}

	v := reflect.ValueOf(i)
	// cannot take address of pointer
	assert.False(t, v.CanAddr())

	// v now holds underlying struct
	v = v.Elem()
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Struct, v.Kind())

	// field of v
	v = v.FieldByName("Field")
	assert.True(t, v.CanAddr())
}

func TestAddressable_NotPointer(t *testing.T) {
	var i interface{} = Addressable{Field: AddressableField{5}}

	v := reflect.ValueOf(i)
	// cannot take address of non-pointer
	assert.False(t, v.CanAddr())
}

func TestAddressableField_Pointer(t *testing.T) {
	var i interface{} = &Addressable{InterfaceField: &AddressableField{5}}

	v := reflect.ValueOf(i)
	assert.False(t, v.CanAddr())

	v = v.Elem()
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Struct, v.Kind())

	v = v.FieldByName("InterfaceField")
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Interface, v.Kind())

	v = v.Elem()
	// cannot take address of pointer
	assert.False(t, v.CanAddr())
	assert.Equal(t, reflect.Ptr, v.Kind())

	v = v.Elem()
	// can take address of underlying struct
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Struct, v.Kind())
}

func TestAddressableField_NotPointer(t *testing.T) {
	var i interface{} = &Addressable{InterfaceField: AddressableField{5}}

	v := reflect.ValueOf(i)
	assert.False(t, v.CanAddr())

	v = v.Elem()
	assert.True(t, v.CanAddr())
	assert.Equal(t, v.Kind(), reflect.Struct)

	v = v.FieldByName("InterfaceField")
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Interface, v.Kind())

	v = v.Elem()
	// can address interface, because it's a part of addressable struct
	// but cannot address underlying value
	assert.False(t, v.CanAddr())
	assert.Equal(t, reflect.Struct, v.Kind())
}

func TestAddressableField_Decode(t *testing.T) {
	a := AddressableField{5}

	n := yaml.Node{
		Kind:  yaml.ScalarNode,
		Value: "10",
	}

	v := reflect.ValueOf(&Addressable{InterfaceField: &a})
	v = v.Elem()
	v = v.FieldByName("InterfaceField")
	assert.Equal(t, reflect.Interface, v.Kind())

	v = v.Elem()
	assert.Equal(t, reflect.Ptr, v.Kind())

	v = v.Elem()
	assert.Equal(t, reflect.Struct, v.Kind())

	v = v.FieldByName("Number")
	assert.True(t, v.CanAddr())

	err := n.Decode(v.Addr().Interface())
	assert.NoError(t, err)
	assert.Equal(t, 10, a.Number)
}

func TestAddressableMap_Decode(t *testing.T) {
	a := 5

	n := yaml.Node{
		Kind:  yaml.ScalarNode,
		Value: "10",
	}

	m := map[interface{}]interface{}{
		"key1": &a,
	}

	v := reflect.ValueOf(&m)
	v = v.Elem()
	v = v.MapIndex(reflect.ValueOf("key1"))
	assert.Equal(t, reflect.Interface, v.Kind())

	v = v.Elem()
	assert.Equal(t, reflect.Ptr, v.Kind())

	v = v.Elem()
	assert.Equal(t, reflect.Int, v.Kind())

	err := n.Decode(v.Addr().Interface())
	assert.NoError(t, err)
	assert.Equal(t, 10, a)
}

func TestUnmarshaler_DataNotPointer(t *testing.T) {
	a := 5
	m := map[interface{}]interface{}{
		"key1": &a,
	}
	u := Unmarshaler{
		Data: m,
	}

	n := yaml.Node{
		Kind:  yaml.ScalarNode,
		Value: "10",
	}

	v := reflect.ValueOf(u.Data)
	assert.Equal(t, reflect.Map, v.Kind())

	v = v.MapIndex(reflect.ValueOf("key1"))
	assert.Equal(t, reflect.Interface, v.Kind())

	v = v.Elem()
	assert.Equal(t, reflect.Ptr, v.Kind())

	v = v.Elem()
	assert.True(t, v.CanAddr())
	assert.Equal(t, reflect.Int, v.Kind())

	err := n.Decode(v.Addr().Interface())
	assert.NoError(t, err)
	assert.Equal(t, 10, a)
}

func TestUnmarshaler_SubDataNotPointer(t *testing.T) {
	// this test shows that maps must be handled at map level
	// probably by splitting the nil and non-nil values
	a := 5
	m := map[interface{}]interface{}{
		"key1": a,
	}
	u := Unmarshaler{
		Data: m,
	}

	n := yaml.Node{
		Kind:  yaml.ScalarNode,
		Value: "10",
	}

	v := reflect.ValueOf(u.Data)
	assert.Equal(t, reflect.Map, v.Kind())

	v = v.MapIndex(reflect.ValueOf("key1"))
	assert.Equal(t, reflect.Interface, v.Kind())

	va := v.Elem()
	// a can't be addressed, so it can't be decoded into
	assert.False(t, va.CanAddr())

	// decode into v, which should be an interface
	var i interface{}
	assert.Panics(t, func() {
		err := n.Decode(i)
		assert.NoError(t, err)
	})
}

func TestDecodeMap_PastBehavior(t *testing.T) {
	a := 5
	b := 6
	m := map[interface{}]interface{}{
		"key1": &a,
		"key2": b,
	}

	n := &yaml.Node{
		Kind: yaml.MappingNode,
		Content: []*yaml.Node{
			{
				Kind:  yaml.ScalarNode,
				Value: "key1",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "10",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "key2",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "11",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "key3",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "12",
			},
		},
	}

	err := n.Decode(m)
	assert.NoError(t, err)

	// because the map type is an interface, decoding does not change a
	assert.NotEqual(t, 10, a)

	// we wouldn't expect b to be changed because it was not a pointer
	assert.NotEqual(t, 11, b)
}

func TestDecodeMap(t *testing.T) {
	a := 5
	b := 6
	m := map[interface{}]*int{
		"key1": &a,
		"key2": &b,
	}

	// unaddressableNodes can be handled by standard decoder, addressable nodes
	// must be handled specially
	unaddressableNodes := &yaml.Node{
		Kind: yaml.MappingNode,
		Content: []*yaml.Node{
			{
				Kind:  yaml.ScalarNode,
				Value: "key2",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "11",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "key3",
			},
			{
				Kind:  yaml.ScalarNode,
				Value: "12",
			},
		},
	}

	addressableNodes := []*yaml.Node{
		{
			Kind:  yaml.ScalarNode,
			Value: "key1",
		},
		{
			Kind:  yaml.ScalarNode,
			Value: "10",
		},
	}

	// first, set addressable nodes
	mapValue := reflect.ValueOf(m)
	for i := 0; i < len(addressableNodes); i += 2 {
		// get key to update
		v := mapValue.MapIndex(reflect.ValueOf(addressableNodes[i].Value))
		// get actual value of key
		for v.Kind() == reflect.Interface || v.Kind() == reflect.Ptr {
			v = v.Elem()
		}
		// decode into value (would be recursive in implementation)
		err := addressableNodes[i+1].Decode(v.Addr().Interface())
		assert.NoError(t, err)
	}

	// second, set unaddressable nodes
	err := unaddressableNodes.Decode(m)
	assert.NoError(t, err)

	assert.Equal(t, 10, a)
	assert.Equal(t, 6, b)

	fmt.Printf("%+v\n", m)
}

// TestUnexportedField is skipped because it is used to visualize structures
// func TestUnexportedField(t *testing.T) {
// 	var b *int
// 	a := &Addressable{
// 		InterfaceField: b,
// 	}

// 	v := reflect.ValueOf(a).Elem()
// 	k := v.Type()
// 	for i := 0; i < k.NumField(); i++ {
// 		name := k.Field(i).Name
// 		vi := v.Field(i)
// 		for (vi.Kind() == reflect.Interface || vi.Kind() == reflect.Ptr) && !vi.IsNil() {
// 			vi = vi.Elem()
// 		}
// 		kind := vi.Kind().String()
// 		settable := vi.CanSet()
// 		isNil := (vi.Kind() == reflect.Interface || vi.Kind() == reflect.Ptr) && vi.IsNil()
// 		fmt.Printf("Name: %s Kind: %s Settable: %t Nil: %t\n", name, kind, settable, isNil)
// 	}

// 	n := yaml.Node{
// 		Kind: yaml.MappingNode,
// 		Content: []*yaml.Node{
// 			{
// 				Kind:  yaml.ScalarNode,
// 				Value: "interfacefield",
// 			},
// 			{
// 				Kind:  yaml.ScalarNode,
// 				Value: "10",
// 			},
// 		},
// 	}
// 	err := n.Decode(v.Addr().Interface())
// 	assert.NoError(t, err)
// 	fmt.Printf("%#v\n%#v\n", a, b)
// }

func TestSplitSlice(t *testing.T) {
	s := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	even := s[:0]
	odd := make([]int, 0, len(s))

	for _, v := range s {
		if v%2 == 0 {
			even = append(even, v)
		} else {
			odd = append(odd, v)
		}
	}

	assert.Equal(t, []int{0, 2, 4, 6, 8}, even)
	assert.Equal(t, []int{1, 3, 5, 7, 9}, odd)
}

var testMarshalDoc = structdoc.Doc{
	Items: map[string]structdoc.Doc{
		"key1": {
			Doc: "This is key 1",
			Items: map[string]structdoc.Doc{
				"Str": {
					Doc: "this is a string inside key1",
				},
				"StrList": {
					Doc: `this is
a multiline string`,
				},
			},
		},
	},
}

func TestMarshalWithDocs(t *testing.T) {
	m := map[interface{}]interface{}{
		"key1": &UnmarshalTestType{
			Str:     "some string",
			StrList: []string{"one"},
			Int:     0,
		},
		"key2": nil,
		"key3": []*UnmarshalTestType{
			{
				Str:     "string1",
				StrList: []string{"two"},
				Int:     1,
			},
			{
				Str:     "string 2",
				StrList: []string{"three"},
				Int:     2,
			},
		},
	}

	_, err := MarshalWithDoc(m, testMarshalDoc)
	assert.NoError(t, err)
	// fmt.Printf("%s\n", b)

	// TODO: walk through nodes and check for comments
}
