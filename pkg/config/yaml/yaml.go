package yaml

import (
	"fmt"
	"reflect"
	"strings"

	"gopkg.in/yaml.v3"

	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/pkg/unwind"
	"gitlab.com/reederc42/wiki/pkg/yamldoc"
)

func Unmarshal(in []byte, out interface{}) error {
	u := Unmarshaler{
		Data: out,
	}
	return yaml.Unmarshal(in, &u)
}

func Marshal(in interface{}) ([]byte, error) {
	return yamldoc.Marshal(in)
}

func MarshalWithDoc(in interface{}, doc structdoc.Doc) ([]byte, error) {
	return yamldoc.MarshalWithDoc(in, doc)
}

type Unmarshaler struct {
	Data interface{}
}

func (u *Unmarshaler) UnmarshalYAML(node *yaml.Node) error {
	return unmarshal(reflect.ValueOf(u.Data), node)
}

// ConfigEncoder implements the config.Encoder interface
type ConfigEncoder struct{}

func (s *ConfigEncoder) Marshal(in interface{}, doc structdoc.Doc) ([]byte, error) {
	return MarshalWithDoc(in, doc)
}

func (s *ConfigEncoder) Unmarshal(in []byte, out interface{}) error {
	return Unmarshal(in, out)
}

var (
	ErrNilValue          = fmt.Errorf("cannot unmarshal into nil value")
	ErrIncompatibleValue = fmt.Errorf("node is incompatible with value")
	ErrUnsettableValue   = fmt.Errorf("value cannot be set")
)

// unmarshal unmarshals node into target, attempting to preserve existing
// non-nil pointers and interfaces
func unmarshal(target reflect.Value, node *yaml.Node) error {
	// unwind target
	target = unwind.Unwind(target)
	if isNil(target) {
		return ErrNilValue
	}
	// check if target and node are compatible
	if !areCompatible(target, node) {
		return ErrIncompatibleValue
	}
	// target and node are compatible, If node and target are not mapping
	// values, try to decode.
	if node.Kind != yaml.MappingNode {
		if !target.CanSet() {
			return ErrUnsettableValue
		}
		return node.Decode(target.Addr().Interface())
	}
	// node and target are mapping values, start recursive unmarshaling
	if target.Kind() == reflect.Map {
		return unmarshalMap(target, node)
	}
	if target.Kind() == reflect.Struct {
		return unmarshalStruct(target, node)
	}
	return fmt.Errorf("unknown error")
}

func unmarshalMap(target reflect.Value, node *yaml.Node) error {
	return unmarshalMappingContext(target, node,
		func(name string) reflect.Value {
			return target.MapIndex(reflect.ValueOf(name))
		})
}

func unmarshalStruct(target reflect.Value, node *yaml.Node) error {
	fields := mapStructFields(target)
	return unmarshalMappingContext(target, node,
		func(name string) reflect.Value {
			return fields[name]
		})
}

// unmarshalMappingContext does the unmarshal work common to structs and maps
func unmarshalMappingContext(target reflect.Value, node *yaml.Node,
	indexFunc func(string) reflect.Value,
) error {
	// split settable values
	unsettableNodes := node.Content[:0]
	settableNodes := make([]*yaml.Node, 0, len(node.Content))
	settableValues := make([]reflect.Value, 0, len(node.Content)/2)
	for i := 0; i < len(node.Content); i += 2 {
		v := indexFunc(node.Content[i].Value)
		v = unwind.Unwind(v)
		if v.CanSet() && !isNil(v) {
			settableNodes = append(settableNodes, node.Content[i],
				node.Content[i+1])
			settableValues = append(settableValues, v)
		} else {
			unsettableNodes = append(unsettableNodes, node.Content[i],
				node.Content[i+1])
		}
	}
	// set settable values
	for i := range settableValues {
		if err := unmarshal(settableValues[i],
			settableNodes[2*i+1]); err != nil {
			return err
		}
	}
	// set unsettable values
	node.Content = unsettableNodes
	return node.Decode(target.Addr().Interface())
}

// mapStructFields maps struct fields to yaml keys the same way yaml does:
// yaml tag or lowercase name. Returns nil if value is not a struct.
func mapStructFields(v reflect.Value) map[string]reflect.Value {
	if v.Kind() != reflect.Struct {
		return nil
	}
	m := make(map[string]reflect.Value)
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		f := t.Field(i)
		// ignore if field is not exported
		if !f.IsExported() {
			continue
		}
		name := getYAMLName(f)
		m[name] = v.Field(i)
	}
	return m
}

// areCompatible returns true if v and n are compatible kinds (i.e., v is a map
// and n is a mapping node)
func areCompatible(v reflect.Value, n *yaml.Node) bool {
	switch n.Kind {
	case yaml.SequenceNode:
		return v.Kind() == reflect.Array || v.Kind() == reflect.Slice
	case yaml.MappingNode:
		return v.Kind() == reflect.Map || v.Kind() == reflect.Struct
	case yaml.ScalarNode:
		switch v.Kind() {
		case reflect.Invalid, reflect.Array, reflect.Chan, reflect.Func,
			reflect.Interface, reflect.Map, reflect.Ptr, reflect.Slice,
			reflect.Struct, reflect.UnsafePointer:
			return false
		default:
			return true
		}
	}
	return false
}

// isNil does not panic if v is not interface or pointer, returns false if v is
// not interface or pointer
func isNil(v reflect.Value) bool {
	return (v.Kind() == reflect.Interface || v.Kind() == reflect.Ptr) &&
		v.IsNil()
}

func getYAMLName(f reflect.StructField) string {
	n := f.Tag.Get("yaml")
	if n == "" {
		n = strings.ToLower(f.Name)
	}
	return n
}
