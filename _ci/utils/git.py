import os
import subprocess

import utils.constants as constants


def commit_id():
    p = subprocess.run(
        [
            "git",
            "rev-parse",
            "HEAD",
        ],
        stdout=subprocess.PIPE,
        check=True,
    )
    id = p.stdout.decode("utf-8").strip()[0 : constants.COMMIT_ID_LENGTH]

    return id


def is_main():
    # some environments will provide the branch name; this can also be used to
    # override is_main on a non-main branch
    return branch_name() == constants.MAIN_BRANCH_NAME


def branch_name():
    branch_name = os.getenv(constants.ENV_BRANCH_NAME)
    if branch_name is not None:
        return branch_name

    p = subprocess.run(
        [
            "git",
            "rev-parse",
            "--abbrev-ref",
            "HEAD",
        ],
        stdout=subprocess.PIPE,
        check=True,
    )
    return p.stdout.decode("utf-8").strip()
