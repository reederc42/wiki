import utils.constants as constants


def boolean(s):
    return s in constants.TRUE_STRINGS
