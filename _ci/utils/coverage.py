import os


def get_dir():
    dir = os.getcwd() + "/_coverage"
    if not os.path.exists(dir):
        os.mkdir(dir)
    return dir
