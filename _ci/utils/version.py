def check(actual, expected, name):
    """Assert actual == expected and print descriptive message with name on fail"""

    assert actual == expected, "%s version mismatch: %s != %s" % (
        name,
        expected,
        actual,
    )
