import json
import os

import utils.constants as constants
import utils.version as version


def check_kubectl_version(runner):
    v = runner.k8s_cli(
        """
        kubectl version --client --output=json
    """,
        capture_output=True,
    ).strip()

    version.check(
        json.loads(v)["clientVersion"]["gitVersion"],
        constants.KUBECTL_CLIENT_VERSION,
        "kubectl",
    )


def check_kind_version(runner):
    v = runner.k8s_cli(
        """
        kind --version
    """,
        capture_output=True,
    ).strip()

    version.check(v, constants.KIND_VERSION, "kind")


def check_kustomize_version(runner):
    v = runner.k8s_cli(
        """
        kustomize version
    """,
        capture_output=True,
    ).strip()

    version.check(v, constants.KUSTOMIZE_VERSION, "kustomize")


def check_vault_version(runner):
    v = runner.k8s_cli(
        """
        vault version
    """,
        capture_output=True,
    ).strip()

    version.check(v, constants.VAULT_VERSION, "vault")


def create_cluster(runner, cluster_id):
    env = {"KIND_EXPERIMENTAL_DOCKER_NETWORK": "bridge"}
    runner.k8s_cli(
        """
        kind create cluster --config kind.yaml --name %s --kubeconfig kubeconfig.yaml
    """
        % cluster_id,
        environ=env,
    )


def delete_cluster(runner, cluster_id):
    runner.k8s_cli(
        """
        kind delete cluster --name %s --kubeconfig kubeconfig.yaml
    """
        % cluster_id
    )


def cluster_id(build_id):
    return "cluster-%s" % build_id


def cluster_load_images(runner, images, cluster_id):
    for image in images:
        runner.k8s_cli(
            """
            kind load docker-image --name %s %s
        """
            % (cluster_id, image)
        )


def env_with_kubeconfig(kubeconfig=None):
    env = os.environ.copy()
    if kubeconfig is None:
        kubeconfig = "%s/kubeconfig.yaml" % os.getcwd()
    env["KUBECONFIG"] = kubeconfig

    return env
