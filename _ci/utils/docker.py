import json
import os
import subprocess

import utils.constants as constants

# Manage docker (or docker-compatible) containers and images.


def build(image, tag=None, dockerfile=None, context=".", args=None):
    """Build container image"""

    cmd = [
        __docker_cmd(),
        "build",
        "--progress=plain",
        "-t",
    ]

    if tag is None:
        cmd.append(image)
    else:
        cmd.append("%s:%s" % (image, tag))

    cmd.extend(__build_args(args))

    if dockerfile is not None:
        cmd.extend(
            [
                "-f",
                dockerfile,
            ]
        )

    cmd.append(context)

    subprocess.run(cmd, check=True)


def run(
    image,
    tag=None,
    environ=None,
    script=None,
    options=None,
    detached=False,
    capture_output=False,
    dry_run=False,
):
    """Run docker container. If detached, returns container id. If not detached,
    runs container interactively and will be stopped when script/command
    completes. If capture_output is True, which is only valid for non-detached
    containers, stdout and stderr will be returned instead of container id. If
    detached, script is appended after docker run. For non-detached containers,
    the current working directory is bind mounted to the container. If dry_run
    is True the command list will be returned. If non-detached and script is not
    None, image must support sh"""

    wd = os.getcwd()
    cmd = [
        __docker_cmd(),
        "run",
        "--rm",
    ]

    if detached:
        cmd.append("-d")
    else:
        cmd.extend(
            [
                "-u",
                "1000:1000",
                "-i",
                "-w",
                wd,
            ]
        )

    cmd.append("-v")
    workspace_mount = os.getenv(constants.ENV_CONTAINER_WORKSPACE_MOUNT)
    if workspace_mount is None:
        cmd.append("{wd}:{wd}".format(wd=wd))
    else:
        cmd.append(workspace_mount)

    cmd.extend(__env_args(environ))

    if options is not None:
        cmd.extend(options)

    if tag is None:
        cmd.append(image)
    else:
        cmd.append("%s:%s" % (image, tag))

    input = None
    if script is not None:
        if detached:
            cmd.append(script)
        else:
            cmd.extend(
                [
                    "sh",
                    "-s",
                ]
            )
            input = script.encode("utf-8")

    if dry_run:
        return (cmd, environ, script)

    if input is not None:
        input = "set -x;".encode("utf-8") + input

    p = subprocess.run(
        cmd, input=input, check=True, capture_output=capture_output or detached
    )

    if capture_output or detached:
        return p.stdout.decode("utf-8").strip()

    return None


def stop(container_id):
    """Stop docker container"""

    p = subprocess.run(
        [
            "docker",
            "stop",
            container_id,
        ],
        capture_output=True,
    )

    return p.stdout.decode("utf-8")


def inspect(container_id):
    cmd = [
        "docker",
        "inspect",
        container_id,
    ]
    p = subprocess.run(cmd, check=True, capture_output=True)

    return json.loads(p.stdout.decode("utf-8").strip())[0]


def inspect_image(image):
    cmd = [
        "docker",
        "image",
        "inspect",
        image,
    ]

    try:
        p = subprocess.run(cmd, check=True, capture_output=True)
        return json.loads(p.stdout.decode("utf-8").strip())[0]
    except Exception:
        return None


def remove_image(image):
    cmd = [
        "docker",
        "image",
        "rm",
        image,
    ]
    subprocess.run(cmd)


def push_images(images):
    for image in images:
        cmd = [
            "docker",
            "push",
            image,
        ]
        subprocess.run(cmd, check=True)


def save_images(images, filename, compress):
    existing_images = []
    for image in images:
        if inspect_image(image) is not None:
            existing_images.append(image)

    if len(existing_images) < 1:
        return

    save_cmd = [
        "docker",
        "save",
    ]
    save_cmd.extend(existing_images)
    save_cmd.extend(
        [
            "-o",
            filename,
        ]
    )
    subprocess.run(save_cmd, check=True)

    if compress:
        compress_cmd = [
            "gzip",
            "-v",
            "-9",
            filename,
        ]
        subprocess.run(compress_cmd, check=True)


def tag_image(image, old_tag, new_tag, old_registry=None, new_registry=None):
    old_image = "%s:%s" % (image, old_tag)
    if old_registry is not None:
        old_image = "%s/%s" % (old_registry, old_image)

    new_image = "%s:%s" % (image, new_tag)
    if new_registry is not None:
        new_image = "%s/%s" % (new_registry, new_image)

    cmd = [
        "docker",
        "tag",
        old_image,
        new_image,
    ]
    subprocess.run(cmd, check=True)

    print("retagged %s to %s" % (old_image, new_image))
    return new_image


def get_ip(container_id, network="bridge"):
    c = inspect(container_id)
    return c.get("NetworkSettings").get("Networks").get(network).get("IPAddress")


def get_network_ip(network="bridge"):
    cmd = [
        "docker",
        "network",
        "inspect",
        network,
    ]
    p = subprocess.run(cmd, check=True, stdout=subprocess.PIPE)
    return json.loads(p.stdout.decode("utf-8").strip())[0]["IPAM"]["Config"][0][
        "Gateway"
    ]


def create_volume(name):
    cmd = [
        "docker",
        "volume",
        "create",
        name,
    ]
    subprocess.run(cmd, check=True, stdout=subprocess.PIPE)


def delete_volume(name):
    cmd = [
        "docker",
        "volume",
        "rm",
        name,
    ]
    subprocess.run(cmd, check=True)


def __docker_cmd():
    return os.getenv(constants.ENV_CONTAINER_CMD, constants.DEFAULT_CONTAINER_CMD)


def __env_args(environ):
    args = []
    if environ is None:
        return args

    for k in environ:
        args.extend(
            [
                "-e",
                "%s=%s" % (k, environ[k]),
            ]
        )

    return args


def __build_args(args):
    build_args = []
    if args is None:
        return build_args

    for k in args:
        build_args.extend(
            [
                "--build-arg",
                "%s=%s" % (k, args[k]),
            ]
        )

    return build_args
