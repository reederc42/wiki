#!/usr/bin/python3

import argparse
import json
import os
import random
import string

import runners
import stages
import utils.constants as constants
import utils.docker as docker
import utils.git as git
import utils.parse as parse


def main():
    if not os.path.exists("go.mod"):
        raise Exception("ci must be run from module root")

    parser = argparse.ArgumentParser()
    parser.add_argument("stages", nargs="+", type=str)
    parser.add_argument("--check-skip", action="store_true")
    parser.add_argument("--list-artifacts", action="store_true")
    args = parser.parse_args()

    run_stages = [stages.map[s]() for s in args.stages]

    container = get_container()
    coverage_mode = parse.boolean(
        os.getenv(constants.ENV_COVERAGE_MODE, constants.DEFAULT_COVERAGE_MODE)
    )
    build_id = get_build_id(coverage_mode)
    runner = get_runner(build_id)
    skipped_stages = get_skipped_stages()

    os.environ[constants.ENV_POSTGRES_PASSWORD] = get_postgres_password()

    for stage in run_stages:
        if stage.name() in skipped_stages:
            print(
                "skipped %s: skipped by %s" % (stage.name(), constants.BRANCH_VARS_FILE)
            )
        else:
            run_stage(
                stage,
                args.list_artifacts,
                args.check_skip,
                runner,
                container,
                coverage_mode,
                build_id,
                skipped_stages,
            )


def get_runner(build_id):
    runner_name = os.getenv(constants.ENV_RUNNER, constants.DEFAULT_RUNNER)
    container_cmd = os.getenv(
        constants.ENV_CONTAINER_CMD, constants.DEFAULT_CONTAINER_CMD
    )
    if runner_name == "container" and container_cmd in constants.DOCKER_COMPATIBLE_CMDS:
        return runners.Docker(build_id)
    else:
        return runners.Shell()


def get_container():
    container_cmd = os.getenv(
        constants.ENV_CONTAINER_CMD, constants.DEFAULT_CONTAINER_CMD
    )
    if container_cmd is None or container_cmd == "":
        return None
    elif container_cmd in constants.DOCKER_COMPATIBLE_CMDS:
        return docker


def get_postgres_password():
    return "".join(
        random.choice(string.ascii_letters + string.digits) for i in range(16)
    )


def get_build_id(coverage_mode):
    id = git.commit_id()
    if coverage_mode:
        id += "-coverage"
    return id


def get_skipped_stages():
    try:
        with open(constants.BRANCH_VARS_FILE, "r") as r:
            branch_vars = json.load(r)
        if branch_vars and branch_vars["SKIP_STAGES"]:
            skipped_stages = branch_vars["SKIP_STAGES"]
    except Exception:
        skipped_stages = []

    return skipped_stages


def run_stage(
    stage,
    list_artifacts,
    check_skip,
    runner,
    container,
    coverage_mode,
    build_id,
    skipped_stages,
):
    skip_stage = stage.skip(runner, container, coverage_mode, skipped_stages)
    if not skip_stage[0]:
        if list_artifacts:
            stage.artifacts(runner, container, coverage_mode, build_id, skipped_stages)
        elif not check_skip:
            stage.stage(runner, container, coverage_mode, build_id, skipped_stages)
    else:
        print("skipped %s: %s" % (stage.name(), skip_stage[0]))


if __name__ == "__main__":
    main()
