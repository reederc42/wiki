import os

import utils.constants as constants
import utils.git as git
import utils.stage as stage


class K8sDeploy(stage.Stage):
    def name(self):
        return "K8s-Deploy"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        if not git.is_main():
            return (True, "branch is not main")
        if coverage_mode:
            return (True, "coverage mode is enabled")
        if os.getenv(constants.ENV_DOCKER_REGISTRY) is None:
            return (True, "no deployment registry defined")
        if container is None:
            return (True, "no container runtime")
        if os.getenv(constants.ENV_PROD_VAULT_TOKEN) is None:
            kubeconfig = os.getenv(constants.ENV_PROD_KUBECONFIG)
            if kubeconfig is None or os.path.getsize(kubeconfig) == 0:
                return (True, "no kubeconfig or vault defined")

        return (False, "")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("deploying k8s")

        vault_token = os.getenv(constants.ENV_PROD_VAULT_TOKEN)
        use_vault = vault_token is not None and vault_token != ""
        if use_vault:
            env = {
                "VAULT_TOKEN": vault_token,
            }
            vault_addr = os.getenv(constants.ENV_PROD_VAULT_URL)
            if vault_addr is not None:
                env["VAULT_ADDR"] = vault_addr
            runner.k8s_cli(
                """
                vault kv get -mount=secret -field=file prod-config > prod.config.yaml
                vault kv get -mount=secret -field=file prod-kubeconfig \
                    > prod.kubeconfig.yaml
            """,
                environ=env,
            )

        deploy_registry = os.getenv(constants.ENV_DOCKER_REGISTRY)
        k8s_images = [
            container.tag_image(
                "wiki-" + i, build_id, build_id, new_registry=deploy_registry
            )
            for i in constants.K8S_IMAGES
        ]
        container.push_images(k8s_images)

        env = {
            "WIKI_POSTGRES_PASSWORD": os.getenv(constants.ENV_PROD_POSTGRES_PASSWORD),
            "WIKI_POSTGRES_HOST": "wiki-db",
            "WIKI_POSTGRES_INSECURE": "true",
            "WIKI_REDIS_SECRET_STORE_HOST": "wiki-cache",
            "KUBECONFIG": os.getenv(constants.ENV_PROD_KUBECONFIG),
        }
        if use_vault:
            env = {
                "KUBECONFIG": "prod.kubeconfig.yaml",
                "PROD_CONFIG": "prod.config.yaml",
            }
        runner.golang(
            """
            go run . tools config --config "$PROD_CONFIG" \
                --template _kustomize/wiki/env.tpl \
                > _kustomize/wiki/.env
        """,
            environ=env,
        )
        runner.k8s_cli(
            """
            kubectl delete job -n wiki wiki-migrate || true
        """,
            environ=env,
        )
        for image in constants.K8S_IMAGES:
            runner.k8s_cli(
                """
                cd _kustomize/wiki
                kustomize edit set image wiki-{image}={registry}/wiki-{image}:{tag}
            """.format(
                    image=image,
                    tag=build_id,
                    registry=deploy_registry,
                )
            )
        runner.k8s_cli(
            """
            kustomize build _kustomize/wiki | kubectl apply -f -
        """,
            environ=env,
        )

        print("deployed k8s")
