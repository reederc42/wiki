import utils.stage as stage


class OmnibusBuild(stage.Stage):
    def name(self):
        return "Omnibus-Build"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("building omnibus")

        dockerfile = "_docker/omnibus/Dockerfile"
        if coverage_mode:
            dockerfile += ".coverage"
        container.build("wiki-omnibus", build_id, dockerfile)

        print("built omnibus")
