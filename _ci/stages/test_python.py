import utils.stage as stage


class TestPython(stage.Stage):
    def name(self):
        return "Test-Python"

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("testing python")

        runner.python(
            """
            cd _ci
            isort --check .
            black --check .
            flake8 .
        """,
        )

        runner.python(
            """
            cd _e2e
            isort --check .
            black --check .
            flake8 .
        """,
        )

        print("tested python")
