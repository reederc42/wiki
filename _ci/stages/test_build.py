import os

import utils.constants as constants
import utils.stage as stage


class TestBuild(stage.Stage):
    def name(self):
        return "Test-Build"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("building test images")

        for image in constants.TEST_IMAGES:
            dockerfile = "_docker/%s/Dockerfile" % image
            if coverage_mode and os.path.exists(dockerfile + ".coverage"):
                dockerfile += ".coverage"
            container.build("wiki-%s" % image, build_id, dockerfile)

        print("built test images")
