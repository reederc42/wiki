import glob
import os
import tarfile

import utils.constants as constants
import utils.parse as parse
import utils.stage as stage


class Archive(stage.Stage):
    def name(self):
        return "Archive"

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("archiving")

        local_archive = parse.boolean(
            os.getenv(constants.ENV_LOCAL_ARCHIVE, constants.DEFAULT_LOCAL_ARCHIVE)
        )

        if container is not None:
            if parse.boolean(
                os.getenv(
                    constants.ENV_ARCHIVE_TEST_IMAGES,
                    constants.DEFAULT_ARCHIVE_TEST_IMAGES,
                )
            ):
                save_images(
                    container,
                    constants.TEST_IMAGES,
                    "test-images",
                    not local_archive,
                    build_id,
                )
            save_images(
                container,
                constants.OMNIBUS_IMAGES,
                "omnibus-images",
                not local_archive,
                build_id,
            )
            save_images(
                container,
                constants.K8S_IMAGES,
                "k8s-images",
                not local_archive,
                build_id,
            )

        (junit_artifacts, artifacts) = self.find_artifacts()

        for artifact in junit_artifacts:
            prefix = os.path.basename(artifact)[: -len(".junit.xml")]
            prefix_junit(runner, artifact, prefix)

        if local_archive:
            tar = tarfile.open("archive-%s.tgz" % build_id, "w:gz")
            for artifact in junit_artifacts + artifacts:
                print("archiving: %s" % artifact)
                tar.add(artifact)

        print("archived")

    def find_artifacts(self):
        junit_artifacts = glob.glob("*.junit.xml")

        artifacts = glob.glob("*.out*")
        artifacts.extend(glob.glob("test-images.t*"))
        artifacts.extend(glob.glob("omnibus-images.t*"))
        artifacts.extend(glob.glob("k8s-images.t*"))
        artifacts.extend(glob.glob("coverage.html*"))
        artifacts.extend(glob.glob("coverage.json"))
        artifacts.extend(glob.glob("coverage.t*"))

        return (junit_artifacts, artifacts)

    def artifacts(self, runner, container, coverage_mode, build_id, skipped_stages):
        (junit_artifacts, artifacts) = self.find_artifacts()
        for artifact in junit_artifacts:
            print("junit: %s" % artifact)
        for artifact in artifacts:
            print("archive: %s" % artifact)

        return


def prefix_junit(runner, filename, prefix):
    runner.local(
        """
        perl -pi -e 's/<testcase(.*?) name="/<testcase\\1 name="%s::/g' %s
    """
        % (prefix, filename)
    )


def save_images(container, images, filename, compress, build_id):
    if compress:
        filename += ".tgz"
    else:
        filename += ".tar"

    full_images = []
    for image in images:
        full_images.append("%s-%s:%s" % ("wiki", image, build_id))

    print("saving docker images", full_images)

    container.save_images(full_images, filename, compress)
