import utils.constants as constants
import utils.stage as stage


class K8sBuild(stage.Stage):
    def name(self):
        return "K8s-Build"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("build k8s images")

        for image in constants.K8S_IMAGES:
            dockerfile = "_docker/%s/Dockerfile" % image
            if coverage_mode:
                dockerfile += ".coverage"
            container.build("wiki-%s" % image, build_id, dockerfile)

        print("built k8s images")
