import os

import utils.constants as constants
import utils.coverage as coverage
import utils.stage as stage
import utils.version as version


class TestGo(stage.Stage):
    def name(self):
        return "Test-Go"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("testing go")

        postgres_password = os.getenv(constants.ENV_POSTGRES_PASSWORD)
        pc = container.run(
            constants.POSTGRES_IMAGE,
            detached=True,
            environ={
                "POSTGRES_PASSWORD": postgres_password,
            },
        )

        if coverage_mode:
            coverage_dir = coverage.get_dir()
            runner.golang(
                """
                go mod vendor
            """
            )
        else:
            runner.golang(
                """
                go mod download
            """
            )

        try:
            check_golangci_lint_version(runner)
            check_gotestsum_version(runner)
            if coverage_mode:
                check_gocover_version(runner)

            test_options = "-tags all"
            env = {
                "WIKI_POSTGRES_HOST": container.get_ip(pc),
                "WIKI_POSTGRES_USER": "postgres",
                "WIKI_POSTGRES_PASSWORD": postgres_password,
                "WIKI_POSTGRES_DATABASE": "wiki",
                "WIKI_POSTGRES_INSECURE": "true",
            }
            if coverage_mode:
                test_options += " -coverprofile %s/unit-tests.out" % coverage_dir
                env["WIKI_COVER_PROFILE"] = "%s/tools-*.out" % coverage_dir

            runner.golang(
                """
                set -o pipefail; golangci-lint run | tee golangci-lint.out
            """,
                environ=env,
            )
            if coverage_mode:
                runner.golang(
                    """
                    gocover cover -v -m --cover-profile-env WIKI_COVER_PROFILE .
                """,
                    environ=env,
                )
            runner.golang(
                """
                go run . migrate -l console
            """,
                environ=env,
            )
            if coverage_mode:
                runner.golang(
                    """
                    gocover clean -v .
                """,
                    environ=env,
                )
            runner.golang(
                """
                gotestsum -f standard-verbose --junitfile %s.junit.xml -- %s ./...
            """
                % (self.name(), test_options),
                environ=env,
            )
            if coverage_mode:
                runner.golang(
                    """
                    gocover cover -v -m --cover-profile-env WIKI_COVER_PROFILE .
                """,
                    environ=env,
                )

        finally:
            container.stop(pc)

        print("tested go")


def check_golangci_lint_version(runner):
    v = runner.golang(
        """
            golangci-lint version --format short 2>&1
        """,
        capture_output=True,
    ).strip()

    version.check(v, constants.GOLANGCI_LINT_VERSION, "golangci-lint")


def check_gotestsum_version(runner):
    v = runner.golang(
        """
            gotestsum --version
        """,
        capture_output=True,
    ).strip()

    version.check(v, constants.GOTESTSUM_VERSION, "gotestsum")


def check_gocover_version(runner):
    v = runner.golang(
        """
        gocover version --short
    """,
        capture_output=True,
    ).strip()

    version.check(v, constants.GOCOVER_VERSION, "gocover")
