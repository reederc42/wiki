import os

import utils.stage as stage


class BuildNode(stage.Stage):
    def name(self):
        return "Build-Node"

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("building node")

        env = {}
        if coverage_mode:
            env["WIKI_COVER_PROFILE"] = "%s/_coverage/tools-*.out" % os.getcwd()

        if "K8s-Build" not in skipped_stages:
            runner.golang(
                """
                go run . tools config --template _vue/env.tpl -o _vue/.env
            """,
                environ=env,
            )

        if "Omnibus-Build" not in skipped_stages:
            runner.golang(
                """
                WIKI_VUE_DIST=omnibus \
                WIKI_VUE_SERVE_FONTS=true \
                go run . \
                    tools config --template _vue/env.tpl -o _vue/.env.omnibus
            """,
                environ=env,
            )

        runner.node(
            """
            cd _vue
            if [ -z "$WIKI_NODE_MODULES" ]; then
                npm install
            elif [ ! -e "node_modules" ]; then
                ln -s "$WIKI_NODE_MODULES" node_modules
            fi
        """
        )

        if "K8s-Build" not in skipped_stages:
            runner.node(
                """
                cd _vue
                npm run build
            """
            )

        if "Omnibus-Build" not in skipped_stages:
            runner.node(
                """
                cd _vue
                npm run build-omnibus
            """
            )

        print("built node")
