import os

import utils.constants as constants
import utils.k8s as k8s
import utils.parse as parse
import utils.stage as stage


class Cleanup(stage.Stage):
    def name(self):
        return "Cleanup"

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("cleaning up")

        if parse.boolean(
            os.getenv(constants.ENV_LOCAL_CLEANUP, constants.DEFAULT_LOCAL_CLEANUP)
        ):
            runner.golang(
                """
                go clean -modcache
                rm -rf \
                    bin/ \
                    _cache/ \
                    _coverage/ \
                    *.out \
                    *.junit.xml \
                    test-images.t* \
                    omnibus-images.t* \
                    k8s-images.t* \
                    coverage.html* \
                    coverage.json \
                    coverage.t* \
                    kind.yaml \
                    _kustomize/wiki/.env \
                    _kustomize/persistence/.env \
                    kubeconfig.yaml \
                    prod.config.yaml \
                    prod.kubeconfig
            """
            )

            for image in constants.K8S_IMAGES:
                runner.k8s_cli(
                    """
                    cd _kustomize/test
                    kustomize edit set image wiki-{image}=wiki-{image}:latest
                    cd ../wiki
                    kustomize edit set image wiki-{image}=wiki-{image}:latest
                """.format(
                        image=image
                    )
                )

            if coverage_mode:
                runner.golang(
                    """
                    gocover clean -v .
                """,
                )

        if coverage_mode:
            try:
                container.delete_volume("volume-%s" % build_id)
            except Exception:
                pass

        if (
            container is not None
            and container.inspect_image("wiki-k8s-cli:%s" % build_id) is not None
        ):
            cluster_id = k8s.cluster_id(build_id)
            k8s.delete_cluster(runner, cluster_id)

        if container is not None:
            if parse.boolean(
                os.getenv(
                    constants.ENV_CLEANUP_IMAGES, constants.DEFAULT_CLEANUP_IMAGES
                )
            ):
                for image in (
                    constants.TEST_IMAGES
                    + constants.OMNIBUS_IMAGES
                    + constants.K8S_IMAGES
                ):
                    container.remove_image("wiki-%s:%s" % (image, build_id))
            if os.getenv(constants.ENV_BRANCH_NAME) == "main":
                registry = os.getenv(constants.ENV_DOCKER_REGISTRY)
                if registry is not None:
                    for image in constants.OMNIBUS_IMAGES + constants.K8S_IMAGES:
                        container.remove_image(
                            "%s/wiki-%s:%s" % (registry, image, build_id)
                        )
                    for image in constants.OMNIBUS_IMAGES:
                        container.remove_image(
                            "%s/wiki-%s:%s" % (registry, image, "latest")
                        )

        print("cleaned up")
