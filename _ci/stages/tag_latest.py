import utils.constants as constants
import utils.git as git
import utils.stage as stage


class TagLatest(stage.Stage):
    def name(self):
        return "Tag-Latest"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        if container is None:
            return (True, "no container runtime")
        return (not git.is_main(), "branch is not main")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("tagging latest")

        tag = "latest"
        if coverage_mode:
            tag += "-coverage"

        for image in [
            "wiki-" + i
            for i in constants.K8S_IMAGES
            + constants.OMNIBUS_IMAGES
            + constants.TEST_IMAGES
        ]:
            container.tag_image(image, build_id, tag)

        print("tagged latest")
