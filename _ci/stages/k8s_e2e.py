import os
import time

import utils.constants as constants
import utils.coverage as coverage
import utils.k8s as k8s
import utils.stage as stage


class K8sE2E(stage.Stage):
    def name(self):
        return "K8s-E2E"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("end-to-end testing k8s")

        env = {
            "WIKI_POSTGRES_PASSWORD": os.getenv(constants.ENV_POSTGRES_PASSWORD),
            "WIKI_POSTGRES_HOST": "wiki-db",
            "WIKI_POSTGRES_INSECURE": "true",
            "WIKI_REDIS_SECRET_STORE_HOST": "wiki-cache",
            "WIKI_CI_KIND_EXPOSE_PORT": "30080",
            "WIKI_CI_KIND_API_SERVER_ADDRESS": container.get_network_ip(),
        }
        registry_mirror = os.getenv(constants.ENV_REGISTRY_MIRROR)
        if registry_mirror is not None:
            env["WIKI_CI_KIND_DOCKER_MIRROR"] = registry_mirror
        if coverage_mode:
            env["WIKI_COVER_PROFILE"] = coverage.get_dir() + "/tools-*.out"
        templates = [
            "persistence",
            "wiki",
        ]
        for template in templates:
            runner.golang(
                """
                go run . tools config \
                    --template _kustomize/{template}/env.tpl \
                    --output _kustomize/{template}/.env
            """.format(
                    template=template
                ),
                environ=env,
            )
        runner.golang(
            """
            go run . tools config --template kind.tpl --output kind.yaml
        """,
            environ=env,
        )

        k8s.check_kubectl_version(runner)
        k8s.check_kind_version(runner)
        k8s.check_kustomize_version(runner)
        k8s.check_vault_version(runner)

        cluster_id = k8s.cluster_id(build_id)
        k8s.create_cluster(runner, cluster_id)

        runner.k8s_cli(
            """
            kubectl label nodes --all \
                wiki.persistence.redis=true \
                wiki.persistence.postgres=true
            kustomize build _kustomize/ingress-nginx | kubectl create -f -
        """,
            environ=k8s.env_with_kubeconfig(),
        )
        time.sleep(20.0)
        runner.k8s_cli(
            """
            kubectl \
                wait \
                pod \
                --namespace ingress-nginx \
                --for=condition=ready \
                --selector=app.kubernetes.io/component=controller \
                --timeout=4m40s
        """,
            environ=k8s.env_with_kubeconfig(),
        )
        k8s_images = []
        for image in constants.K8S_IMAGES:
            k8s_images.append("wiki-%s:%s" % (image, build_id))
        k8s.cluster_load_images(runner, k8s_images, cluster_id)
        k8s_images = []
        for image in constants.K8S_IMAGES:
            k8s_images.append("wiki-%s" % image)
        kustomize_tag(runner, k8s_images, build_id, "test")
        build = "test"
        if coverage_mode:
            build = "coverage"
        runner.k8s_cli(
            """
            kustomize build _kustomize/{build} | kubectl create -f -
        """.format(
                build=build
            ),
            environ=k8s.env_with_kubeconfig(),
        )
        kubectl_wait_for_resources(
            runner,
            {
                "api": "Ready",
                "ui": "Ready",
                "cache": "Ready",
                "db": "Ready",
            },
            constants.K8S_WAIT_TIMEOUT,
            "wiki",
            "pod",
        )
        kubectl_wait_for_resources(
            runner,
            {
                "migrate": "Complete",
            },
            constants.K8S_WAIT_TIMEOUT,
            "wiki",
            "job",
        )

        wiki_addr = container.get_ip("%s-control-plane" % cluster_id) + ":30080"

        sc = container.run(
            constants.SELENIUM_IMAGE,
            detached=True,
        )
        try:
            selenium_addr = container.get_ip(sc) + ":4444"
            env = {
                "WIKI_E2E_SELENIUM_ADDR": "http://%s/wd/hub" % selenium_addr,
                "WIKI_E2E_UI_ADDR": "http://%s" % wiki_addr,
                "WIKI_E2E_API_ADDR": "http://%s" % wiki_addr,
                "WIKI_E2E_BUILD": "%s-k8s" % build_id,
            }
            runner.python(
                """
                cd _e2e
                pytest -v --junitxml ../%s.junit.xml
            """
                % self.name(),
                environ=env,
            )
        finally:
            container.stop(sc)

        print("end-to-end tested k8s")


def kubectl_wait_for_resources(runner, component_conditions, timeout, namespace, type):
    for component in component_conditions:
        runner.k8s_cli(
            """
            kubectl \
                wait \
                {type} \
                --namespace {namespace} \
                --for condition={condition} \
                --selector app=wiki,component={component} \
                --timeout {timeout}
        """.format(
                component=component,
                condition=component_conditions[component],
                timeout=timeout,
                namespace=namespace,
                type=type,
            ),
            environ=k8s.env_with_kubeconfig(),
        )


def kustomize_tag(runner, images, tag, env, registry=None):
    for image in images:
        registry_tag = "%s:%s" % (image, tag)
        if registry is not None:
            registry_tag = "%s/%s" % (registry, registry_tag)
        runner.k8s_cli(
            """
            cd _kustomize/{env}
            kustomize edit set image {image}={registry_tag}
        """.format(
                env=env, image=image, registry_tag=registry_tag
            )
        )
