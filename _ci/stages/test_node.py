import utils.stage as stage


class TestNode(stage.Stage):
    def name(self):
        return "Test-Node"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (False, "")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("testing node")

        runner.node(
            """
            cd _vue
            if [ -z "$WIKI_NODE_MODULES" ]; then
                npm install
            elif [ ! -e "node_modules" ]; then
                ln -s "$WIKI_NODE_MODULES" node_modules
            fi
            npm run lint -- --output-file ../vue_lint.out --no-fix --max-warnings 0
        """
        )

        print("tested node")
