import os

import utils.stage as stage


class BuildGo(stage.Stage):
    def name(self):
        return "Build-Go"

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("building go")

        if coverage_mode:
            coverage_dir = os.getcwd() + "/_coverage"
            if not os.path.exists(coverage_dir):
                os.mkdir(coverage_dir)

        env = {}
        if coverage_mode:
            env["WIKI_COVER_PROFILE"] = "%s/tools-*.out" % coverage_dir
        build_flags = [
            "-v",
            "-ldflags",
            "'-s -w'",
        ]

        if "K8s-Build" not in skipped_stages:
            runner.golang(
                """
                go generate -v -tags compressdist ./internal/ui/vue/dist/
            """,
                environ=env,
            )

        if "Omnibus-Build" not in skipped_stages:
            tags = "omnibus"
            if coverage_mode:
                tags = "omnibus-compressed"
            runner.golang(
                """
                go generate -v -tags "$(go run . tools tags {tags})" \
                    ./internal/ui/vue/dist/
            """.format(
                    tags=tags,
                ),
                environ=env,
            )

        if coverage_mode:
            runner.golang(
                """
                gocover cover -v --cover-profile-env WIKI_COVER_PROFILE \
                    --no-exit-signals -m .
            """,
                environ=env,
            )

        if "K8s-Build" not in skipped_stages:
            if coverage_mode:
                # wiki-secrets does not handle exit signals
                runner.golang(
                    """
                    gocover cover -v -m --cover-profile-env WIKI_COVER_PROFILE .
                """
                )
            runner.golang(
                """
                go build {build_flags} -tags "$(go run . tools tags secrets)" \
                    -o bin/wiki-secrets .
            """.format(
                    build_flags=" ".join(build_flags),
                ),
                environ=env,
            )
            if coverage_mode:
                # other binaries do handle exit signals
                runner.golang(
                    """
                    gocover cover -v --cover-profile-env WIKI_COVER_PROFILE \
                        --no-exit-signals -m .
                """
                )
            runner.golang(
                """
                go build {build_flags} -tags "$(go run . tools tags api)" \
                    -o bin/wiki-api .
                go build {build_flags} -tags "$(go run . tools tags migrate)" \
                    -o bin/wiki-migrate .
                go build {build_flags} -tags "$(go run . tools tags ui)" \
                    -o bin/wiki-ui .
            """.format(
                    build_flags=" ".join(build_flags),
                ),
                environ=env,
            )

        if "Omnibus-Build" not in skipped_stages:
            tags = "omnibus"
            if coverage_mode:
                tags = "omnibus-compressed"
            runner.golang(
                """
                go build {build_flags} -tags "$(go run . tools tags {tags})" \
                    -o bin/wiki .
            """.format(
                    build_flags=" ".join(build_flags),
                    tags=tags,
                ),
                environ=env,
            )
            if not coverage_mode:
                runner.golang(
                    """
                    upx --lzma -q bin/wiki
                """,
                    environ=env,
                )

        print("built go")
