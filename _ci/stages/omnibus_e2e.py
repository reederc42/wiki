import utils.constants as constants
import utils.coverage as coverage
import utils.stage as stage


class OmnibusE2E(stage.Stage):
    def name(self):
        return "Omnibus-E2E"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (container is None, "no container runtime")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("end-to-end testing omnibus")

        omnibus_options = []
        if coverage_mode:
            coverage_dir = coverage.get_dir()
            coverage_vol = "volume-%s" % build_id
            container.create_volume(coverage_vol)
            omnibus_options = ["-v", "%s:%s" % (coverage_vol, "/coverage")]

        sc = container.run(
            constants.SELENIUM_IMAGE,
            detached=True,
        )
        try:
            oc = container.run(
                "wiki-omnibus:%s" % build_id,
                detached=True,
                options=omnibus_options,
            )
            try:
                selenium_addr = container.get_ip(sc) + ":%s" % constants.SELENIUM_PORT
                wiki_addr = container.get_ip(oc) + ":80"
                env = {
                    "WIKI_E2E_SELENIUM_ADDR": "http://%s/wd/hub" % selenium_addr,
                    "WIKI_E2E_UI_ADDR": "http://%s" % wiki_addr,
                    "WIKI_E2E_API_ADDR": "http://%s" % wiki_addr,
                    "WIKI_E2E_BUILD": "%s-omnibus" % build_id,
                }
                runner.python(
                    """
                    cd _e2e
                    pytest -v --junitxml ../%s.junit.xml
                """
                    % self.name(),
                    environ=env,
                )
            finally:
                container.stop(oc)
        finally:
            container.stop(sc)
            if coverage_mode:
                copy_volume(container, coverage_vol, coverage_dir)
                container.delete_volume(coverage_vol)

        print("end-to-end tested omnibus")


def copy_volume(container, src, dst):
    container.run(
        "alpine:3",
        script="""
        cp /src/* {dst}
    """.format(
            dst=dst
        ),
        options=["-v", "%s:%s" % (src, "/src")],
    )
