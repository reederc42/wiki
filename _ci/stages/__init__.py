import stages.archive as archive
import stages.build_go as build_go
import stages.build_node as build_node
import stages.cleanup as cleanup
import stages.collect_coverage as collect_coverage
import stages.k8s_build as k8s_build
import stages.k8s_deploy as k8s_deploy
import stages.k8s_e2e as k8s_e2e
import stages.omnibus_build as omnibus_build
import stages.omnibus_deploy as omnibus_deploy
import stages.omnibus_e2e as omnibus_e2e
import stages.tag_latest as tag_latest
import stages.test_build as test_build
import stages.test_go as test_go
import stages.test_node as test_node
import stages.test_python as test_python

map = {
    "archive": archive.Archive,
    "build_go": build_go.BuildGo,
    "build_node": build_node.BuildNode,
    "cleanup": cleanup.Cleanup,
    "collect_coverage": collect_coverage.CollectCoverage,
    "k8s_build": k8s_build.K8sBuild,
    "k8s_deploy": k8s_deploy.K8sDeploy,
    "k8s_e2e": k8s_e2e.K8sE2E,
    "omnibus_build": omnibus_build.OmnibusBuild,
    "omnibus_deploy": omnibus_deploy.OmnibusDeploy,
    "omnibus_e2e": omnibus_e2e.OmnibusE2E,
    "tag_latest": tag_latest.TagLatest,
    "test_build": test_build.TestBuild,
    "test_go": test_go.TestGo,
    "test_node": test_node.TestNode,
    "test_python": test_python.TestPython,
}
