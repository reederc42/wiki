import os

import utils.constants as constants
import utils.git as git
import utils.stage as stage


class OmnibusDeploy(stage.Stage):
    def name(self):
        return "Omnibus-Deploy"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        if not git.is_main():
            return (True, "branch is not main")
        if coverage_mode:
            return (True, "coverage mode is enabled")
        if os.getenv(constants.ENV_DOCKER_REGISTRY) is None:
            return (True, "no deployment registry defined")
        if container is None:
            return (True, "no container runtime")
        return (False, "")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("deploying omnibus")

        deploy_registry = os.getenv(constants.ENV_DOCKER_REGISTRY)
        omnibus_images = [
            container.tag_image(
                "wiki-" + i, build_id, "latest", new_registry=deploy_registry
            )
            for i in constants.OMNIBUS_IMAGES
        ]
        container.push_images(omnibus_images)

        print("deployed omnibus")
