import os

import utils.constants as constants
import utils.coverage as coverage
import utils.k8s as k8s
import utils.parse as parse
import utils.stage as stage


class CollectCoverage(stage.Stage):
    def name(self):
        return "Collect-Coverage"

    def skip(self, runner, container, coverage_mode, skipped_stages):
        return (not coverage_mode, "nothing to do!")

    def stage(self, runner, container, coverage_mode, build_id, skipped_stages):
        print("collecting coverage")

        coverage_dir = coverage.get_dir()
        try:
            if container is not None:
                runner.k8s_cli(
                    """
                    kubectl delete pod -n wiki -l app=wiki
                    kubectl cp -n wiki wiki-coverage:/coverage {coverage_dir}
                """.format(
                        coverage_dir=coverage_dir
                    ),
                    environ=k8s.env_with_kubeconfig(),
                )
        except Exception as e:
            print(e)

        try:
            runner.golang(
                """
                gocover clean -v .
                if [ -n "$(ls {coverage_dir})" ]; then
                    gocover merge -v {coverage_dir}/* > coverage.out
                fi
                go tool cover -html coverage.out -o coverage.html || true
            """.format(
                    coverage_dir=coverage_dir
                )
            )
        except Exception as e:
            print(e)

        for label in constants.COVERAGE_LABELS:
            label_profile(
                runner,
                label,
                constants.COVERAGE_LABELS[label],
                "coverage.json",
                coverage_dir,
            )

        if not parse.boolean(
            os.getenv(constants.ENV_LOCAL_ARCHIVE, constants.DEFAULT_LOCAL_ARCHIVE)
        ):
            runner.local(
                """
                tar -zcvf coverage.tgz {coverage_dir}
                gzip < coverage.html > coverage.html.gz
                gzip < coverage.out > coverage.out.gz
                rm -f coverage.html coverage.out
            """.format(
                    coverage_dir=coverage_dir
                )
            )

        print("collected coverage")


def label_profile(runner, label, profile, existing_labels, coverage_dir):
    if not os.path.exists(existing_labels):
        runner.local(
            """
            echo '{{"mode": "set", "blocks": null}}' > {existing_labels}
        """.format(
                existing_labels=existing_labels
            ),
            environ=k8s.env_with_kubeconfig(),
        )

    runner.golang(
        """
        if [ -n "$(ls {coverage_dir}/{profile})" ]; then
            gocover -v label --labels all,{label} --existing-labels {existing_labels} \
                -o {existing_labels} {coverage_dir}/{profile}
        fi
    """.format(
            label=label,
            existing_labels=existing_labels,
            profile=profile,
            coverage_dir=coverage_dir,
        )
    )
