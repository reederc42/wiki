from tokenize import String


class Runner:
    def golang(self, script: String, environ=None, capture_output=False):
        pass

    def node(self, script: String, environ=None, capture_output=False):
        pass

    def python(self, script: String, environ=None, capture_output=False):
        pass

    def k8s_cli(self, script: String, environ=None, capture_output=False):
        pass

    def local(self, script: String, environ=None, capture_output=False):
        pass
