import os
from tokenize import String

import runners.shell as shell
import utils.constants as constants
import utils.docker as docker


class Docker(shell.Shell):
    def __init__(self, build_id, docker_sock=constants.DEFAULT_DOCKER_SOCK):
        self.build_id = build_id
        self.docker_sock = docker_sock

    def golang(self, script: String, environ={}, capture_output=False):
        if constants.ENV_GO_CACHE not in environ:
            environ[constants.ENV_GO_CACHE] = os.getcwd() + "/_cache"
            environ["GOPATH"] = environ[constants.ENV_GO_CACHE]

        c = docker.run(
            "wiki-go-test",
            self.build_id,
            environ,
            script,
            capture_output=capture_output,
        )

        return c

    def node(self, script: String, environ=None, capture_output=False):
        c = docker.run(
            "wiki-node-test",
            self.build_id,
            environ,
            script,
            capture_output=capture_output,
        )

        return c

    def python(self, script: String, environ=None, capture_output=False):
        c = docker.run(
            "wiki-python-test",
            self.build_id,
            environ,
            script,
            capture_output=capture_output,
        )

        return c

    def k8s_cli(self, script: String, environ=None, capture_output=False):
        c = docker.run(
            "wiki-k8s-cli",
            self.build_id,
            environ,
            script,
            capture_output=capture_output,
            options=[
                "-v",
                "%s:%s" % (self.docker_sock, "/var/run/docker.sock"),
            ],
        )

        return c
