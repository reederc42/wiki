import os
import subprocess
from tokenize import String

import runners.base as base


class Shell(base.Runner):
    def golang(self, script: String, environ=None, capture_output=False):
        return self.local(script=script, environ=environ, capture_output=capture_output)

    def local(self, script: String, environ=None, capture_output=False):
        env = dict(os.environ)
        if environ is not None:
            env = {**env, **environ}

        p = subprocess.run(
            "set -x;" + script,
            shell=True,
            check=True,
            env=env,
            capture_output=capture_output,
        )

        if capture_output:
            return p.stdout.decode("utf-8")

    def node(self, script: String, environ=None, capture_output=False):
        return self.local(script=script, environ=environ, capture_output=capture_output)

    def python(self, script: String, environ=None, capture_output=False):
        return self.local(script=script, environ=environ, capture_output=capture_output)

    def k8s_cli(self, script: String, environ=None, capture_output=False):
        return self.local(script=script, environ=environ, capture_output=capture_output)
