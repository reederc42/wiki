package main

import (
	"os"

	"gitlab.com/reederc42/wiki/cmd"
)

func main() {
	if c := cmd.Execute(); c != 0 {
		os.Exit(c)
	}
}
