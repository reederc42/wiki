import urllib.parse
from time import sleep

import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium_harness import SeleniumHarness
from tenacity import retry, stop_after_delay


class TestSubject:
    def test_signed_out_new_show_error(self, harness: SeleniumHarness):
        harness.get("wiki/__new")

        @retry(stop=stop_after_delay(1))
        def check_signed_out():
            e1 = harness.find_element(By.XPATH, '//*[@id="app"]/div/div/div/div')
            assert "not signed in" in e1.text

        check_signed_out()

    def test_signed_in_can_save(self, harness: SeleniumHarness):
        harness.get()
        harness.signin()
        harness.get("wiki/__new")
        e1 = harness.find_element(By.XPATH, '//*[@id="editor___new"]/textarea')
        e1.send_keys("# subject 1")
        e2 = harness.find_element(
            By.XPATH, '//*[@id="app"]/div[1]/main/div/div/div/div[2]/div/div[2]/button'
        )
        assert not e2.get_attribute("disabled")

    def test_signed_in_create_and_edit(self, harness: SeleniumHarness):
        harness.get()
        harness.signin()
        title = harness.random_title()
        harness.get("wiki/__new?subject=%s" % title)
        e1 = harness.find_element(
            By.XPATH, '//*[@id="app"]/div[1]/main/div/div/div/div[2]/div/div[2]/button'
        )
        harness.click(e1)

        sleep(0.5)
        editTab = harness.find_element(
            By.XPATH, '//*[@id="app"]/div[1]/main/div/div/div/div[1]/div[2]/div/div[3]'
        )
        harness.click(editTab)

        editor = harness.find_element(By.XPATH, '//*[@id="editor_%s"]/textarea' % title)
        harness.send_keys(editor, "some awesome content")
        e2 = harness.find_element(
            By.XPATH, '//*[@id="app"]/div[1]/main/div/div/div/div[2]/div/div[2]/button'
        )
        harness.click(e2)

    def test_signed_out_cant_save(self, harness: SeleniumHarness):
        harness.driver.get(urllib.parse.urljoin(harness.wiki_ui_addr, "wiki/__new"))
        e1 = harness.find_element(By.XPATH, '//*[@id="editor___new"]/textarea')
        e1.send_keys("# subject 1")
        e2 = harness.find_element(
            By.XPATH, '//*[@id="app"]/div[1]/main/div/div/div/div[2]/div/div[2]/button'
        )
        assert e2.get_attribute("disabled")

    def test_signed_in_can_edit(self, harness: SeleniumHarness):
        harness.get()
        harness.signin()
        harness.get("wiki/__new")
        e1 = harness.find_element(By.XPATH, '//*[@id="editor___new"]/textarea')
        title = harness.random_title()
        e1.send_keys(Keys.BACKSPACE + title)
        e2 = harness.find_element(
            By.XPATH, '//*[@id="editor___new"]/div[3]/div/div[3]/div/span'
        )
        assert title == e2.text

    def test_signed_out_cant_edit(self, harness: SeleniumHarness):
        harness.get("wiki/__new")
        e1 = harness.find_element(By.XPATH, '//*[@id="editor___new"]/textarea')
        title = harness.random_title()
        e1.send_keys(Keys.BACKSPACE + title)
        with pytest.raises(NoSuchElementException):
            harness.driver.find_element(
                By.XPATH, '//*[@id="editor___new"]/div[3]/div/div[3]/div/span'
            )

    def test_subject_not_found(self, harness: SeleniumHarness):
        subject1 = self._new_subject_name(harness)
        harness.get("wiki/%s" % subject1)
        e1 = harness.find_element(By.XPATH, '//*[@id="app"]/div/main/div/div/div/h2')
        assert "Not Found" in e1.text
        e2 = harness.find_element(By.XPATH, '//*[@id="app"]/div/main/div/div/div/h3')
        assert "Sign in" in e2.text

        harness.signin()
        subject2 = self._new_subject_name(harness)
        harness.get("wiki/%s" % subject2)
        e3 = harness.find_element(By.XPATH, '//*[@id="app"]/div/main/div/div/div/h2')
        assert "Not Found" in e3.text
        e4 = harness.find_element(By.XPATH, '//*[@id="app"]/div/main/div/div/div/h3')
        assert "Create" in e4.text

    def _new_subject_name(self, harness):
        """Create new subject name"""

        new_subject = harness.random_title()
        while new_subject in harness.list_subjects():
            new_subject = harness.random_title()

        return new_subject
