import http.client
import json
import os
import random
import string
import time
import urllib.parse
from base64 import b64encode
from datetime import datetime, timedelta
from typing import Any

from selenium import webdriver
from selenium.common.exceptions import (
    ElementClickInterceptedException,
    ElementNotInteractableException,
    NoSuchElementException,
)
from selenium.webdriver.common.by import By
from tenacity import retry, retry_if_exception_type, stop_after_delay

_DEFAULT_WIKI_E2E_SELENIUM_DRIVER_NAME = "firefox"
_DEFAULT_WIKI_E2E_UI_ADDR = "http://localhost:8080"
_DEFAULT_WIKI_E2E_WINDOW_WIDTH = 1024
_DEFAULT_WIKI_E2E_WINDOW_HEIGHT = 768


class SeleniumHarness:
    """Set up Selenium driver

    Env:
        WIKI_E2E_SELENIUM_DRIVER_NAME: name of selenium driver; options: firefox,
            chrome; default: firefox

        WIKI_E2E_SELENIUM_ADDR: address of selenium instance; the full path,
            possibly including 'wd/hub'; if not set, uses local driver

        WIKI_E2E_UI_ADDR: address of UI

        WIKI_E2E_API_ADDR: address of API; optional: used to create users and list
            articles

        WIKI_E2E_SCREENSHOT_PATH: path for screenshot; see TestCase.save_screenshot

        WIKI_E2E_BUILD: build ID; when available, should be set: adds randomness and
            organizes screenshots

        WIKI_E2E_WINDOW_WIDTH: selenium window width in pixels; default: 1024

        WIKI_E2E_WINDOW_HEIGHT: selenium window height in pixels; default: 768"""

    def __init__(self):
        self._screenshot_id = 0
        self.window_width = int(
            os.getenv("WIKI_E2E_WINDOW_WIDTH", _DEFAULT_WIKI_E2E_WINDOW_WIDTH)
        )
        self.window_height = int(
            os.getenv("WIKI_E2E_WINDOW_HEIGHT", _DEFAULT_WIKI_E2E_WINDOW_HEIGHT)
        )
        self.driver = self._setup_selenium_driver(
            os.getenv(
                "WIKI_E2E_SELENIUM_DRIVER_NAME", _DEFAULT_WIKI_E2E_SELENIUM_DRIVER_NAME
            ),
            os.getenv("WIKI_E2E_SELENIUM_ADDR", ""),
        )
        self.wiki_ui_addr = os.getenv("WIKI_E2E_UI_ADDR", _DEFAULT_WIKI_E2E_UI_ADDR)
        self.screenshot_path = os.getenv("WIKI_E2E_SCREENSHOT_PATH", "")
        self.build = os.getenv("WIKI_E2E_BUILD", "")
        self.wiki_api_addr = os.getenv("WIKI_E2E_API_ADDR", "")
        if self.wiki_api_addr != "":
            self.test_user = self._create_test_user()
        else:
            self.test_user = "default"

    def cleanup(self):
        self.driver.quit()

    def save_screenshot(self):
        """Save a screenshot to the path:
        {WIKI_E2E_SCREENSHOT_PATH}/{WIKI_E2E_BUILD}/{test method}_{screenshot id}.png.
        For more control over screenshot location, use self.driver.save_screenshot"""

        filename = (
            os.path.join(
                self.screenshot_path,
                self.build,
                self._getTestMethodName() + "_" + self._get_screenshot_id(),
            )
            + ".png"
        )
        self.driver.save_screenshot(filename)

    def get(self, page="/"):
        """Get wiki UI at page"""

        self.driver.get(urllib.parse.urljoin(self.wiki_ui_addr, page))

    def list_subjects(self):
        """List subjects

        If api address is provided, get from api
        If api address is not provided, returns keys from
        ../../_vue/api/mock/subjects.json"""

        if self.wiki_api_addr == "":
            f = open(self._get_vue_dir() + "/subjects.json")
            subjects = json.load(f)
            f.close()
            return subjects

        u = urllib.parse.urlparse(self.wiki_api_addr)
        c = http.client.HTTPConnection(u.netloc)
        c.request("GET", "/api/v1/subject/")
        res = c.getresponse()
        if res.status != 200:
            raise Exception("could not list articles: %d %s" % (res.status, res.reason))
        subjects = json.loads(res.read().decode("utf-8"))
        c.close()
        return subjects

    def random_id(self):
        return self._random_string()

    def _random_string(self, length=10):
        """Get a random string of lowercase letters and numbers with length 'length'"""

        s = ""
        if self.build != "":
            s = self.build + "-"

        return s + "".join(
            random.choices(string.ascii_lowercase + string.digits, k=length)
        )

    def _get_screenshot_id(self):
        id = "{:04d}".format(self._screenshot_id)
        self._screenshot_id += 1
        return id

    def signin(self):
        """Signin as "test" user"""

        self.click(self.find_element(By.XPATH, '//*[@id="app"]/div/header/div/button'))
        self.click(
            self.find_element(
                By.CSS_SELECTOR,
                "#app > div.v-application--wrap > nav > \
                    div.v-navigation-drawer__content > div > div:nth-child(2)",
            )
        )
        self.find_element(By.ID, "input-username").send_keys(self.test_user)
        self.find_element(By.ID, "input-password").send_keys("pass")
        self.click(self.find_element(By.ID, "btn-signin"))
        time.sleep(0.5)

    @retry(
        retry=retry_if_exception_type(NoSuchElementException),
        stop=stop_after_delay(1 * 60),
    )
    def find_element(self, by=..., value: Any | None = ...):
        """Find element in page with retry"""

        return self.driver.find_element(by=by, value=value)

    @retry(
        retry=retry_if_exception_type(
            (
                ElementNotInteractableException,
                ElementClickInterceptedException,
            )
        ),
        stop=stop_after_delay(2),
    )
    def click(self, element):
        element.click()

    @retry(
        retry=retry_if_exception_type(ElementNotInteractableException),
        stop=stop_after_delay(2),
    )
    def send_keys(self, element, *value: Any):
        element.send_keys(value)

    def random_title(self):
        return "title-" + self._random_string()

    def _setup_selenium_driver(self, driver_name, addr, timeout=timedelta(minutes=1)):
        """Wait for selenium server until timeout"""

        statusUp = False
        lastException = None
        start = datetime.now()
        while datetime.now() - start <= timeout and not statusUp:
            try:
                driver = self._get_selenium_driver(driver_name, addr)
                statusUp = True
            except Exception as e:
                lastException = e
                if "unrecognized driver:" in str(e):
                    break

        if not statusUp:
            raise lastException

        driver.set_window_size(self.window_width, self.window_height)
        return driver

    def _get_selenium_driver(self, driver_name, addr):
        """Create driver from name and address"""

        if addr == "":
            if driver_name == "firefox":
                return webdriver.Firefox()
            elif driver_name == "chrome":
                return webdriver.Chrome()
        else:
            if driver_name == "firefox":
                return webdriver.Remote(
                    command_executor=addr,
                    options=webdriver.FirefoxOptions(),
                )
            elif driver_name == "chrome":
                return webdriver.Remote(
                    command_executor=addr,
                    options=webdriver.ChromeOptions(),
                )
        raise Exception("unrecognized driver: %s" % driver_name)

    def _create_test_user(self):
        """Create user with name 'test-{random id}' and password 'pass'"""

        name = "test-" + self.random_id()
        u = urllib.parse.urlparse(self.wiki_api_addr)
        h = {
            "Authorization": "Basic %s"
            % b64encode(("%s:pass" % name).encode("utf-8")).decode("utf-8")
        }
        c = http.client.HTTPConnection(u.netloc)
        c.request("POST", "/auth/v1/signup", headers=h)
        res = c.getresponse()
        # 401 is returned if user already exists, so ignore it
        if res.status != 200 and res.status != 401:
            c.close()
            raise Exception(
                "could not create test user: %d %s" % (res.status, res.reason)
            )

        c.close()
        return name

    def _getTestMethodName(self):
        return str(self).split()[0]

    def _get_vue_dir(self):
        """Get vue directory, depends on location of selenium_harness.py"""

        d = os.path.dirname(__file__)
        return os.path.abspath(d + "../../../_vue/src/api/mock")
