import pytest
from selenium_harness import SeleniumHarness


@pytest.fixture
def harness(request):
    h = SeleniumHarness()

    def f():
        h.cleanup()

    request.addfinalizer(f)
    return h
