import time

from selenium.webdriver.common.by import By
from selenium_harness import SeleniumHarness


class TestAuth:
    def test_signin_signout(self, harness: SeleniumHarness):
        harness.get()
        harness.signin()
        e1 = harness.find_element(
            By.CSS_SELECTOR,
            "#app > div.v-application--wrap > nav > div.v-navigation-drawer__content > \
                div > div:nth-child(2) > div > div.v-list-item__content",
        )
        assert harness.test_user == e1.text
        harness.click(
            harness.find_element(
                By.CSS_SELECTOR,
                "#app > div.v-application--wrap > nav > \
                    div.v-navigation-drawer__content > div > div:nth-child(2)",
            )
        )
        time.sleep(0.5)
        harness.click(harness.find_element(By.ID, "btn-signout"))
        e2 = harness.find_element(
            By.CSS_SELECTOR,
            "#app > div.v-application--wrap > nav > div.v-navigation-drawer__content > \
            div > div:nth-child(2) > div > div.v-list-item__content",
        )
        assert "" == e2.text

    def test_signup(self, harness: SeleniumHarness):
        harness.get()
        user = "user" + harness.random_id()
        harness.click(
            harness.find_element(By.XPATH, '//*[@id="app"]/div/header/div/button')
        )
        harness.click(
            harness.find_element(
                By.CSS_SELECTOR,
                "#app > div.v-application--wrap > nav > \
                    div.v-navigation-drawer__content > div > div:nth-child(2)",
            )
        )
        harness.find_element(By.ID, "input-username").send_keys(user)
        harness.find_element(By.ID, "input-password").send_keys("pass")
        harness.click(harness.find_element(By.ID, "btn-signup"))
        time.sleep(0.5)
        e2 = harness.find_element(
            By.CSS_SELECTOR,
            "#app > div.v-application--wrap > nav > div.v-navigation-drawer__content > \
            div > div:nth-child(2) > div > div.v-list-item__content",
        )
        assert user == e2.text
