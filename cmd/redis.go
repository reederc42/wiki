//go:build !no_redis

package cmd

import (
	"context"

	"gitlab.com/reederc42/wiki/internal/auth/redis"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var redisConfig = &redis.DefaultSecretStoreConfig

func init() {
	initializers.Push(initializeRedis, orderInitRedis)

	config.Add(config.Key{
		Path:    "redisSecretStore",
		Default: redisConfig,
		Doc: structdoc.Doc{
			Doc: "redis secret store; ignored if auth backend is postgres or " +
				"bolt",
			Order: orderCfgRedisSecretStore,
		},
	})
}

func initializeRedis(ctx context.Context) (context.Context, error) {
	if cfg.AuthBackend != "redis" {
		return ctx, nil
	}
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("connecting to redis")
	as, err := redis.NewSecretStore(*redisConfig, "authorization")
	if err != nil {
		return ctx, err
	}
	ss, err := redis.NewSecretStore(*redisConfig, "service")
	if err != nil {
		return ctx, err
	}
	l.Info("connected to redis")
	return contextWithServiceSecretsBackend(
		contextWithAuthSecretsBackend(
			ctx,
			as,
		),
		ss,
	), nil
}
