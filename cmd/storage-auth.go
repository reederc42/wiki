//go:build !no_storage_auth

package cmd

import (
	"context"

	storageauth "gitlab.com/reederc42/wiki/internal/auth/model"
)

func init() {
	initializers.Push(modelAuthBackendInitializer, orderInitStorageAuth)
}

func modelAuthBackendInitializer(ctx context.Context) (context.Context, error) {
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("initializing storage auth")
	if cfg.AuthBackend != "storage" {
		return ctx, nil
	}
	m, err := storageBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	as := storageauth.NewSecretStore(
		m.AuthSecret,
		authConfig.Authorization.SecretLifespan,
		authConfig.Authorization.TokenLifespan,
		authConfig.Wiggle,
	)
	ss := storageauth.NewSecretStore(
		m.AuthSecret,
		authConfig.Service.SecretLifespan,
		authConfig.Service.TokenLifespan,
		authConfig.Wiggle,
	)
	l.Info("initialized storage auth")
	return contextWithServiceSecretsBackend(
		contextWithAuthSecretsBackend(
			ctx,
			as,
		),
		ss,
	), nil
}
