//go:build !no_server
// Code generated by structdoc. DO NOT EDIT.
package doc

import (
	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/internal/httpserver"
)

func init() {
	structdoc.Set(
		httpserver.TLSConfig{},
		structdoc.Doc{Doc:"", Order:0, Items:map[string]structdoc.Doc{"CertFile":structdoc.Doc{Doc:"TLS certificate file", Order:0, Items:map[string]structdoc.Doc(nil)}, "Enable":structdoc.Doc{Doc:"use TLS", Order:0, Items:map[string]structdoc.Doc(nil)}, "KeyFile":structdoc.Doc{Doc:"TLS private key file", Order:0, Items:map[string]structdoc.Doc(nil)}}},
	)
}
