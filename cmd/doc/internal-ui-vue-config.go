//go:build !no_tools
// Code generated by structdoc. DO NOT EDIT.
package doc

import (
	"gitlab.com/reederc42/wiki/pkg/structdoc"
	"gitlab.com/reederc42/wiki/internal/ui/vue"
)

func init() {
	structdoc.Set(
		vue.Config{},
		structdoc.Doc{Doc:"", Order:0, Items:map[string]structdoc.Doc{"APIClient":structdoc.Doc{Doc:"vue app api client (\"mock\", \"server\")", Order:0, Items:map[string]structdoc.Doc(nil)}, "APIClientHost":structdoc.Doc{Doc:"vue app api client host, not valid if api client is \"mock\"", Order:0, Items:map[string]structdoc.Doc(nil)}, "APIClientPath":structdoc.Doc{Doc:"vue app api client path, not valid if api client is \"mock\"", Order:0, Items:map[string]structdoc.Doc(nil)}, "APIListRefresh":structdoc.Doc{Doc:"subject list refresh period", Order:0, Items:map[string]structdoc.Doc(nil)}, "AuthClient":structdoc.Doc{Doc:"vue app auth client (\"mock\", \"server\")", Order:0, Items:map[string]structdoc.Doc(nil)}, "AuthClientHost":structdoc.Doc{Doc:"vue app auth client host, not valid if auth client is \"mock\"", Order:0, Items:map[string]structdoc.Doc(nil)}, "AuthClientPath":structdoc.Doc{Doc:"vue app auth client path, not valid if auth client is \"mock\"", Order:0, Items:map[string]structdoc.Doc(nil)}, "Dist":structdoc.Doc{Doc:"dist name", Order:0, Items:map[string]structdoc.Doc(nil)}, "NodeEnv":structdoc.Doc{Doc:"node environment (\"development\", \"production\") (default \"production\")", Order:0, Items:map[string]structdoc.Doc(nil)}, "ServeFonts":structdoc.Doc{Doc:"if true, fonts are bundled into app", Order:0, Items:map[string]structdoc.Doc(nil)}}},
	)
}
