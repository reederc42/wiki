//go:build !no_config_yaml

package cmd

import (
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/config/yaml"
)

func init() {
	config.SetEncoder(&yaml.ConfigEncoder{})
}
