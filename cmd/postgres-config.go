//go:build !no_postgres_config

package cmd

import (
	"gitlab.com/reederc42/wiki/internal/postgres"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var postgresConfig = &postgres.DefaultConnection

func init() {
	config.Add(config.Key{
		Path:    "postgres",
		Default: postgresConfig,
		Doc: structdoc.Doc{
			Doc:   "postgres db; ignored if storage backend is bolt",
			Order: orderCfgPostgres,
		},
	})
}
