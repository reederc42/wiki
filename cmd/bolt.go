//go:build !no_bolt

package cmd

import (
	"context"

	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/bolt"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var boltConfig = &bolt.DefaultBoltConfig

func init() {
	initializers.Push(boltStorageBackendInitializer, orderInitBolt)

	config.Add(config.Key{
		Path:    "bolt",
		Default: boltConfig,
		Doc: structdoc.Doc{
			Doc:   "bolt db; ignored if storage backend is postgres",
			Order: orderCfgBolt,
		},
	})
}

func boltStorageBackendInitializer(ctx context.Context) (context.Context,
	error,
) {
	if cfg.StorageBackend != "bolt" {
		return ctx, nil
	}
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("connecting to boltdb", zap.String("file", boltConfig.Filename))
	b, close, err := bolt.Open(boltConfig.Filename)
	if err != nil {
		return ctx, err
	}
	finalizers = append(finalizers, func() {
		close()
	})
	l.Info("connected to boltdb", zap.String("file", boltConfig.Filename))
	return contextWithStorageBackend(ctx, b), nil
}
