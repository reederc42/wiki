package cmd

import (
	"bytes"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"

	"gitlab.com/reederc42/wiki/pkg/config"
)

func TestConfig(t *testing.T) {
	cmd := newTestCmd(t)
	err := collectConfig(cmd, nil)
	assert.NoError(t, err)

	var b bytes.Buffer
	err = config.Write(&b)
	assert.NoError(t, err)
}

func newTestCmd(t *testing.T) *cobra.Command {
	t.Helper()
	cmd := &cobra.Command{}
	cmd.PersistentFlags().Bool("silent", false, "")
	cmd.PersistentFlags().String("encoding", "", "")
	err := cmd.ParseFlags(nil)
	assert.NoError(t, err)
	return cmd
}
