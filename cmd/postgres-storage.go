//go:build !no_postgres_storage

package cmd

import (
	"context"

	"gitlab.com/reederc42/wiki/internal/postgres"
)

func init() {
	initializers.Push(postgresStorageBackendInitializer, orderInitPostgres)
}

func postgresStorageBackendInitializer(ctx context.Context) (context.Context,
	error,
) {
	if cfg.StorageBackend != "postgres" {
		return ctx, nil
	}
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("connecting to postgres")
	pg, close, err := postgres.New(*postgresConfig)
	if err != nil {
		return ctx, err
	}
	finalizers = append(finalizers, func() {
		close()
	})
	l.Info("connected to postgres")
	return contextWithStorageBackend(ctx, pg), nil
}
