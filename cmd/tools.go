//go:build !no_tools

package cmd

import (
	"gitlab.com/reederc42/wiki/internal/tools"
	"gitlab.com/reederc42/wiki/internal/ui/vue"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

func init() {
	rootCmd.AddCommand(tools.GetCommand())

	config.Add(config.Key{
		Path:    "vue",
		Default: &vue.DefaultConfig,
		Doc: structdoc.Doc{
			Doc:   "vue build configuration",
			Order: orderCfgVue,
		},
	})
	config.Add(config.Key{
		Path:    "ci",
		Default: &tools.DefaultCIConfig,
		Doc: structdoc.Doc{
			Doc:   "ci configuration",
			Order: orderCfgCI,
		},
	})
}
