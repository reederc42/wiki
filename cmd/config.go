package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	_ "gitlab.com/reederc42/wiki/cmd/doc"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/config/env"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

type Config struct {
	// do not display usage on error
	Silent bool `yaml:"silent"`
	// logger encoding: values: "json", "console"; default "json"
	Encoding string `yaml:"encoding"`
	// storage backend: values: "postgres", "bolt"; default "postgres"
	StorageBackend string `yaml:"storageBackend"`
	// authorization backend: values: "storage", "redis"; default "storage"
	AuthBackend string `yaml:"authBackend"`
}

var (
	cfg = Config{
		Silent:         false,
		Encoding:       "json",
		StorageBackend: "postgres",
		AuthBackend:    "storage",
	}
	logger      *zap.Logger
	configFiles []string
	export      bool
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "prints configuration",
	RunE: func(_ *cobra.Command, _ []string) error {
		if export {
			config.SetEncoder(env.DefaultEncoder)
		}
		return config.Write(os.Stdout)
	},
}

func init() {
	rootCmd.AddCommand(configCmd)
	configCmd.Flags().BoolVarP(&export, "export", "e", false,
		"exports configuration as environment variables")
}

func collectConfig(cmd *cobra.Command, _ []string) error {
	config.SetEnvPrefix("WIKI")
	config.SetFiles(configFiles)
	if err := config.Collect(); err != nil {
		return err
	}
	if c, v, err := getFlagBool(cmd, "silent"); err == nil && c {
		cfg.Silent = v
	} else if err != nil {
		return err
	}
	if c, v, err := getFlagString(cmd, "encoding"); err == nil && c {
		cfg.Encoding = v
	} else if err != nil {
		return err
	}
	cmd.SilenceErrors = cfg.Silent
	cmd.SilenceUsage = cfg.Silent
	logger = newZapLogger(cfg.Encoding)
	return nil
}

// newZapLogger exits program on error
func newZapLogger(encoding string) *zap.Logger {
	config := zap.NewProductionConfig()
	config.Encoding = encoding
	config.DisableStacktrace = true
	config.EncoderConfig.EncodeTime = zapcore.RFC3339NanoTimeEncoder
	config.OutputPaths = []string{"stdout"}
	logger, err := config.Build()
	if err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
	return logger
}

func getFlagBool(cmd *cobra.Command, flag string) (bool, bool, error) {
	f := cmd.Flags().Lookup(flag)
	if f == nil {
		return false, false, fmt.Errorf("flag not found: %s", flag)
	}
	v, err := cmd.Flags().GetBool(flag)
	return f.Changed, v, err
}

func getFlagString(cmd *cobra.Command, flag string) (bool, string, error) {
	f := cmd.Flags().Lookup(flag)
	if f == nil {
		return false, "", fmt.Errorf("flag not found: %s", flag)
	}
	v, err := cmd.Flags().GetString(flag)
	return f.Changed, v, err
}

func init() {
	configKeys := []config.Key{
		{
			Path:    "silent",
			Default: &cfg.Silent,
			Doc: structdoc.Doc{
				Doc:   "do not display usage on error",
				Order: orderCfgSilent,
			},
		},
		{
			Path:    "encoding",
			Default: &cfg.Encoding,
			Doc: structdoc.Doc{
				Doc: "logger encoding (\"json\", \"console\") default " +
					"\"json\"",
				Order: orderCfgEncoding,
			},
		},
		{
			Path:    "storageBackend",
			Default: &cfg.StorageBackend,
			Doc: structdoc.Doc{
				Doc: "storage backend (\"postgres\", \"bolt\") default " +
					"\"postgres\"",
				Order: orderCfgStorageBackend,
			},
		},
		{
			Path:    "authBackend",
			Default: &cfg.AuthBackend,
			Doc: structdoc.Doc{
				Doc: "authorization backend (\"storage\", \"redis\") default " +
					"\"storage\"",
				Order: orderCfgAuthBackend,
			},
		},
	}
	for _, k := range configKeys {
		config.Add(k)
	}
}
