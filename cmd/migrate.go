//go:build !no_migrate

package cmd

import (
	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/postgres"
	"gitlab.com/reederc42/wiki/internal/postgres/migration"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "runs wiki migrations against database",
	RunE: func(_ *cobra.Command, _ []string) error {
		{
			logger.Info("ensuring database exists")
			logger.Info("connecting to postgres")
			db, err := connectWithoutDatabase(*postgresConfig)
			if err != nil {
				return err
			}
			logger.Info("connected to postgres")
			if err := migration.EnsureDatabase(db,
				postgresConfig.Database); err != nil {
				return err
			}
			db.Close()
			logger.Info("database exists")
		}
		{
			logger.Info("migrating database")
			logger.Info("connecting to postgres")
			db, err := sqlx.Connect("postgres",
				postgres.MakeConnectionString(postgresConfig))
			if err != nil {
				return err
			}
			logger.Info("connected to postgres")
			if err := migration.MigrateDatabase(zap.NewStdLog(logger), db,
				migration.FS, migration.Dir); err != nil {
				return err
			}
			logger.Info("database migrated")
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(migrateCmd)
}

func connectWithoutDatabase(conn postgres.Connection) (*sqlx.DB, error) {
	conn.Database = ""
	return sqlx.Connect("postgres", postgres.MakeConnectionString(&conn))
}
