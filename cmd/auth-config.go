//go:build !no_auth_config

package cmd

import (
	"context"
	"fmt"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var authConfig = &auth.DefaultConfig

func init() {
	config.Add(config.Key{
		Path:    "auth",
		Default: authConfig,
		Doc: structdoc.Doc{
			Doc:   "authorization configuration",
			Order: orderCfgAuth,
		},
	})
}

type (
	authSecretsBackendKey    struct{}
	serviceSecretsBackendKey struct{}
)

func authSecretsBackendFromContext(ctx context.Context) (auth.SecretStore,
	error,
) {
	v := ctx.Value(authSecretsBackendKey{})
	if v == nil {
		return nil, fmt.Errorf("no auth secrets backend")
	}
	return v.(auth.SecretStore), nil
}

func contextWithAuthSecretsBackend(ctx context.Context,
	as auth.SecretStore,
) context.Context {
	return context.WithValue(ctx, authSecretsBackendKey{}, as)
}

func serviceSecretsBackendFromContext(ctx context.Context) (auth.SecretStore,
	error,
) {
	v := ctx.Value(serviceSecretsBackendKey{})
	if v == nil {
		return nil, fmt.Errorf("no service secrets backend")
	}
	return v.(auth.SecretStore), nil
}

func contextWithServiceSecretsBackend(ctx context.Context,
	ss auth.SecretStore,
) context.Context {
	return context.WithValue(ctx, serviceSecretsBackendKey{}, ss)
}
