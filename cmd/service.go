//go:build !no_service

package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/pkg/initialize"
)

// global initializer order
const (
	orderInitBolt int = iota
	orderInitPostgres
	orderInitRedis
	orderInitStorageAuth
	orderInitAuthReconciler
	orderInitRouter
	orderInitAPI
	orderInitUI
	orderInitServer
)

const initializerCap = orderInitServer + 1

var (
	// initializers initialize components
	initializers initialize.Queue = initialize.NewQueueWithCap(initializerCap)
	// components are started as goroutines
	components []func(chan error)
	// Finalizers are always run after at least 1 component has written to the
	// error channel
	finalizers []func()
)

var serviceCmd = &cobra.Command{
	Use:   "service",
	Short: "starts wiki server",
	RunE: func(_ *cobra.Command, _ []string) error {
		rootLogger := logger.With(zap.String("component", "root"))
		rootLogger.Info("initializing service")
		// execute initializers
		if _, err := initializers.Execute(
			contextWithRootLogger(
				contextWithGlobalLogger(
					context.Background(),
					logger,
				),
				rootLogger,
			)); err != nil {
			return err
		}
		rootLogger.Info("initialized service")
		// start components
		errCh := make(chan error)
		for _, c := range components {
			go c(errCh)
		}
		err := <-errCh
		// execute finalizers
		for _, f := range finalizers {
			f()
		}
		return err
	},
}

func init() {
	rootCmd.AddCommand(serviceCmd)
}

type rootLoggerKey struct{}

func rootLoggerFromContext(ctx context.Context) (*zap.Logger, error) {
	v := ctx.Value(rootLoggerKey{})
	if v == nil {
		return nil, fmt.Errorf("no root logger")
	}
	return v.(*zap.Logger), nil
}

func contextWithRootLogger(ctx context.Context, l *zap.Logger) context.Context {
	return context.WithValue(ctx, rootLoggerKey{}, l)
}

type globalLoggerKey struct{}

func globalLoggerFromContext(ctx context.Context) (*zap.Logger, error) {
	v := ctx.Value(globalLoggerKey{})
	if v == nil {
		return nil, fmt.Errorf("no global logger")
	}
	return v.(*zap.Logger), nil
}

func contextWithGlobalLogger(ctx context.Context,
	l *zap.Logger,
) context.Context {
	return context.WithValue(ctx, globalLoggerKey{}, l)
}
