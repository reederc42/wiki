//go:build !no_ui

package cmd

import (
	"context"

	"gitlab.com/reederc42/wiki/internal/ui/vue"
)

func init() {
	initializers.Push(uiInitializer, orderInitUI)
}

func uiInitializer(ctx context.Context) (context.Context,
	error,
) {
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("initializing ui")
	router, err := routerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	logger, err := globalLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	r, err := vue.Handle(router, logger)
	if err != nil {
		return ctx, err
	}
	l.Info("initialized ui")
	return contextWithRouter(ctx, r), nil
}
