//go:build !no_server

package cmd

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/NYTimes/gziphandler"
	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/httpserver"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var httpConfig = &httpserver.DefaultHTTPConfig

func init() {
	initializers.Push(func(ctx context.Context) (context.Context, error) {
		return contextWithRouter(ctx, mux.NewRouter()), nil
	}, orderInitRouter)
	initializers.Push(serverInitializer, orderInitServer)

	config.Add(config.Key{
		Path:    "http",
		Default: httpConfig,
		Doc: structdoc.Doc{
			Doc:   "HTTP server configuration",
			Order: orderCfgHTTP,
		},
	})
}

func serverInitializer(ctx context.Context) (context.Context, error) {
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("initializing server")
	router, err := routerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	router.PathPrefix("/").
		Methods(http.MethodOptions).
		HandlerFunc(func(_ http.ResponseWriter, _ *http.Request) {})
	var h http.Handler = router
	if httpConfig.DisableCORS {
		h = httpserver.HandlerDisableCORS(router)
	}
	h = gziphandler.GzipHandler(h)
	var tc *tls.Config
	if httpConfig.TLS.Enable {
		cert, err := tls.LoadX509KeyPair(httpConfig.TLS.CertFile,
			httpConfig.TLS.KeyFile)
		if err != nil {
			return ctx, err
		}
		tc = &tls.Config{Certificates: []tls.Certificate{cert}}
	}
	srv := httpserver.GracefulLoggedTLS{
		Addr:                httpConfig.Address,
		Handler:             h,
		Logger:              logger.With(zap.String("component", "http")),
		TLS:                 tc,
		ShutdownGracePeriod: httpConfig.ShutdownTime,
		GracefulShutdown:    make(chan struct{}),
	}
	components = append(components, []func(chan error){
		// graceful shutdown on received signals
		func(_ chan error) {
			sig := make(chan os.Signal, 1)
			signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
			<-sig
			srv.GracefulShutdown <- struct{}{}
		},
		// start server
		func(c chan error) {
			c <- srv.Serve()
		},
	}...)
	l.Info("initialized server")
	return ctx, nil
}

type routerKey struct{}

func routerFromContext(ctx context.Context) (*mux.Router, error) {
	v := ctx.Value(routerKey{})
	if v == nil {
		return nil, fmt.Errorf("no router")
	}
	return v.(*mux.Router), nil
}

func contextWithRouter(ctx context.Context, r *mux.Router) context.Context {
	return context.WithValue(ctx, routerKey{}, r)
}
