package cmd

import (
	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

// global config order
const (
	orderCfgSilent int = iota
	orderCfgEncoding
	orderCfgHTTP
	orderCfgStorageBackend
	orderCfgAuthBackend
	orderCfgAuthEnableReconciler
	orderCfgAuth
	orderCfgRedisSecretStore
	orderCfgPostgres
	orderCfgBolt
	orderCfgVue
	orderCfgCI
)

var rootCmd = &cobra.Command{
	Use:               "wiki",
	PersistentPreRunE: collectConfig,
}

func init() {
	rootCmd.CompletionOptions.DisableDefaultCmd = true
	rootCmd.PersistentFlags().StringSliceVarP(&configFiles, "config",
		"c", nil, "list of configuration files; merged in order")
	rootCmd.PersistentFlags().BoolP("silent", "s", false,
		"do not display usage on error")
	rootCmd.PersistentFlags().StringP("encoding", "l", "json",
		"logger encoding (\"json\", \"console\")")
}

func Execute() int {
	if err := rootCmd.Execute(); err != nil {
		if cfg.Silent && logger != nil {
			logger.Error("error in root", zap.String("component", "main"),
				zap.Error(err))
		}
		return 1
	}
	return 0
}
