//go:build !no_auth_reconciler

package cmd

import (
	"context"
	"time"

	"go.uber.org/zap"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/pkg/config"
	"gitlab.com/reederc42/wiki/pkg/structdoc"
)

var authEnableReconciler = true

func init() {
	initializers.Push(authReconcilerInitializer, orderInitAuthReconciler)

	config.Add(config.Key{
		Path:    "authEnableReconciler",
		Default: &authEnableReconciler,
		Doc: structdoc.Doc{
			Doc: "enable authorization reconciler, should only be enabled on " +
				"a single instance",
			Order: orderCfgAuthEnableReconciler,
		},
	})
}

func authReconcilerInitializer(ctx context.Context) (context.Context, error) {
	if !authEnableReconciler {
		return ctx, nil
	}
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("initializing auth reconciler")
	logger, err := globalLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	as, err := authSecretsBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	components = append(components, func(c chan error) {
		c <- startSecretReconciler(
			context.Background(),
			as,
			logger.With(zap.String("component", "secrets"),
				zap.String("secrets", "auth")),
			authConfig.Authorization,
			authConfig.Wiggle,
			authConfig.ErrorRequeue,
		)
	})
	ss, err := serviceSecretsBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	components = append(components, func(c chan error) {
		c <- startSecretReconciler(
			context.Background(),
			ss,
			logger.With(zap.String("component", "secrets"),
				zap.String("secrets", "service")),
			authConfig.Service,
			authConfig.Wiggle,
			authConfig.ErrorRequeue,
		)
	})
	l.Info("initialized auth reconciler")
	return ctx, nil
}

func startSecretReconciler(ctx context.Context, ss auth.SecretStore,
	logger *zap.Logger, ac auth.SecretConfig, wiggle,
	errorRequeue time.Duration,
) error {
	logger.Info("starting reconciler")
	for {
		t, err := auth.Reconcile(ctx, ss, logger, ac, wiggle, errorRequeue)
		if err != nil {
			return err
		}
		time.Sleep(t)
	}
}
