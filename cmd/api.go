//go:build !no_api

package cmd

import (
	"context"
	"fmt"

	"gitlab.com/reederc42/wiki/internal/auth"
	"gitlab.com/reederc42/wiki/internal/model"
	"gitlab.com/reederc42/wiki/internal/wiki/mdlwiki"
	"gitlab.com/reederc42/wiki/internal/wiki/rest"
)

func init() {
	initializers.Push(initializeAPI, orderInitAPI)
}

func initializeAPI(ctx context.Context) (context.Context, error) {
	l, err := rootLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	l.Info("initializing api")
	m, err := storageBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	a := &auth.Service{
		Model:  m,
		Config: *authConfig,
	}
	as, err := authSecretsBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	ss, err := serviceSecretsBackendFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	a.AuthorizationSecrets = as
	a.ServiceSecrets = ss
	s := &mdlwiki.Service{
		Model:     m,
		GetUserID: auth.MakeAuthorizeServiceFunc(a),
	}
	router, err := routerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	logger, err := globalLoggerFromContext(ctx)
	if err != nil {
		return ctx, err
	}
	rr := rest.Handle(router, s, logger)
	ar := auth.Handle(rr, a, logger)
	l.Info("initialized api")
	return contextWithRouter(ctx, ar), nil
}

type storageBackendKey struct{}

func storageBackendFromContext(ctx context.Context) (model.Model, error) {
	v := ctx.Value(storageBackendKey{})
	if v == nil {
		return model.Model{}, fmt.Errorf("no storage backend")
	}
	return v.(model.Model), nil
}

func contextWithStorageBackend(ctx context.Context,
	m model.Model,
) context.Context {
	return context.WithValue(ctx, storageBackendKey{}, m)
}
