GO_MODULE = gitlab.com/reederc42/wiki
GOHOSTOS = "$(shell go env GOHOSTOS)"
VERSION ?= $(shell git rev-parse --short HEAD)
DOCKER_SOCK ?= /var/run/docker.sock
ifeq (${GOHOSTOS},"windows")
	DOCKER_SOCK = //var/run/docker.sock
endif
WD ?= $(shell pwd)
ifeq (${GOHOSTOS},"windows")
	WD = $(shell cd)
endif

ifdef JENKINS_WIKI_PROD_CONFIG_FILE
	PROD_DB_PASSWORD="$(shell yq '.postgres.password' ${JENKINS_WIKI_PROD_CONFIG_FILE})"
endif
ifdef JENKINS_WIKI_PROD_KUBECONFIG
	PROD_KUBECONFIG_BYTES="$(shell base64 -w 0 ${JENKINS_WIKI_PROD_KUBECONFIG})"
endif

.PHONY: ui clean init-dev-pki omnibus-build jenkins

install-ui:
	cd _vue && \
		npm install

build-ui:
	go run -v . tools config -t _vue/env.tpl -o _vue/.env
ifeq (${GOHOSTOS},"windows")
	set WIKI_VUE_DIST=omnibus\
	&& \
	set WIKI_VUE_SERVE_FONTS=true\
	&& \
	go run -v . tools config -t _vue/env.tpl -o _vue/.env.omnibus
else
	WIKI_VUE_DIST=omnibus WIKI_VUE_SERVE_FONTS=true go run -v . tools config -t _vue/env.tpl -o _vue/.env.omnibus
endif
	cd _vue && \
		npm run build && \
		npm run build-omnibus

init-dev-pki:
	cd _pki && \
		openssl req -x509 -newkey rsa:2048 -nodes \
			-keyout key.pem -out cert.pem \
			-subj "/C=US/ST=Washington/L=Seattle/CN=wiki-dev" \
			-addext "subjectAltName=DNS:localhost"

generate:
	go generate -v -tags 'compressdist omnibusdist' ./...
	go generate -v -tags 'compressdist' ./...
	go generate -v -tags 'omnibusdist' ./...
	go generate -v ./...

ui: install-ui build-ui generate

clean-api:
ifeq (${GOHOSTOS},"windows")
	go clean -x -testcache -cache -modcache && \
		del /f /q internal\ui\vue\dist\zz_generated.* > NUL
else
	go clean -x -testcache -cache -modcache && \
		rm -rf internal/ui/vue/zz_generated.*
endif

clean-ui:
ifeq (${GOHOSTOS},"windows")
	cd _vue && \
		del /f /q *.local && \
		del /f /q /s node_modules > NUL && \
		rmdir /q /s node_modules > NUL
	cd _vue && \
		del /f /q /s dist > NUL && \
		rmdir /q /s dist > NUL && \
		del /f /q /s dist-omnibus > NUL && \
		rmdir /q /s dist-omnibus > NUL
else
	cd _vue && \
		rm -rf *.local && \
		rm -rf node_modules && \
		rm -rf dist && \
		rm -rf dist-omnibus
endif

clean-pki:
ifeq (${GOHOSTOS},"windows")
	cd _pki && \
		del /f /q *.pem > NUL
else
	cd _pki && \
		rm -rf *.pem
endif

clean-bolt:
ifeq (${GOHOSTOS},"windows")
	del /f /q wiki.boltdb
else
	rm -rf wiki.boltdb
endif

clean: clean-api clean-ui clean-pki clean-bolt

omnibus-build:
	docker build -t wiki-omnibus:${VERSION} -f _docker/omnibus/Dockerfile .

jenkins-build:
ifeq (${GOHOSTOS},"windows")
	exit 1
else
	@JENKINS_PROD_KUBECONFIG_BYTES=${PROD_KUBECONFIG_BYTES} \
	JENKINS_PROD_DB_PASSWORD="${PROD_DB_PASSWORD}" \
	JENKINS_REPOSITORY_SOURCE=file:///home/src/${GO_MODULE} \
	JENKINS_CONTAINER_WORKSPACE_MOUNT=wiki-jenkins-home:/var/jenkins_home \
		envsubst < _docker/jenkins/jenkins.yaml > _docker/jenkins/ci.jenkins.yaml
	docker build -t jenkins:wiki -f _docker/jenkins/Dockerfile _docker/jenkins
	rm -f _docker/jenkins/ci.jenkins.yaml
endif

jenkins-deploy:
	docker run \
		--rm -d -u root --name wiki-jenkins-pre-deploy \
		-v ${DOCKER_SOCK}:/var/run/docker.sock \
		jenkins:wiki \
		sh -c 'chown 1000:1000 /var/run/docker.sock'
	docker run \
		--rm -d --name wiki-jenkins \
		-p 8080:8080 \
		-p 50000:50000 \
		-v wiki-jenkins-home:/var/jenkins_home \
		-v ${DOCKER_SOCK}:/var/run/docker.sock \
		-v ${WD}:/home/src/${GO_MODULE} \
		jenkins:wiki

jenkins: jenkins-build jenkins-deploy
